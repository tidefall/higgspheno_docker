import tools
import os
import numpy as np
from numpy import testing as tst
from scripts import Data
import shutil
from ipdb import set_trace as st


def test_AttributesDict():
    contents = {'a': 1, 'b': 2}
    unnamed_dict = Data.AttributesDict(contents)
    assert unnamed_dict.name == 'this'
    named_dict = Data.AttributesDict(contents, "name")
    assert named_dict.name == 'name'
    assert named_dict.a == 1
    assert unnamed_dict.b == 2
    # should raise an attribute error
    try:
        named_dict.c
    except AttributeError as e:
        assert str(e) == "No c in name dict"
    else:
        raise AssertionError("Failed to raise attribute error for non existant member")
    assert set(contents.keys()).issubset(set(dir(named_dict)))


def test_Run():
    with tools.TempTestDir("tmp_test") as dir_name:
        empty_dir = os.path.join(dir_name, "empty_run")
        empty = Data.Run(empty_dir)
        assert empty.dir_name == empty_dir
        assert len(empty.datasets) == 0
        assert len(empty.infos) == 0
        ds1 = np.random.rand(3,5)
        cols1 = [f"a{i}" for i in range(5)]
        ds2 = np.ones((10, 1))
        cols2 = ['s(i)ngle']
        ds3 = np.zeros((1, 5))
        cols3 = [f"x{i}" for i in range(5)]
        ds4 = np.array([])
        cols4 = []
        inf1 = {'part': 'stoop', 'dtype': float, 'arry': '[2,3,6]', 'plop': 'doof gibble', 'columns': cols1}
        inf2 = {'dtype': np.int, 'columns': cols2}
        inf3 = {'columns': cols3}
        inf4 = {'columns': cols4}
        infos = {'a': inf1, 'b': inf2, 'c': inf3, 'd': inf4}
        datasets = {'a': ds1, 'b': ds2, 'c': ds3, 'd': ds4}
        full_dir = os.path.join(dir_name, "full_run")
        full = Data.Run(full_dir, datasets=datasets, infos=infos)
        tst.assert_allclose(full.a.a0, ds1[:, 0])
        tst.assert_allclose(full.a.a4, ds1[:, 4])
        tst.assert_allclose(full.b.single, ds2[:, 0])
        tst.assert_allclose(full.c.x4, ds3[:, 4])
        assert len(full.datasets['d']) == 0
        assert full.n_b.single == cols2[0]
        full.write()
        # make a pre existing run
        from_file_dir = os.path.join(dir_name, "from_file")
        os.mkdir(from_file_dir)
        shutil.copyfile(os.path.join(full_dir, 'a.csv'), os.path.join(from_file_dir, 'a.csv'))
        shutil.copyfile(os.path.join(full_dir, 'a.info'), os.path.join(from_file_dir, 'a.info'))
        shutil.copyfile(os.path.join(full_dir, 'a.csv'), os.path.join(from_file_dir, 'validinputs.csv'))
        shutil.copyfile(os.path.join(full_dir, 'a.info'), os.path.join(from_file_dir, 'validinputs.info'))
        existing = Data.Run(from_file_dir)
        tst.assert_allclose(existing.a.a0, ds1[:, 0])
        tst.assert_allclose(existing.validinputs.a4, ds1[:, 4])
        # combine with another run
        append_ds1 = np.random.rand(2, 5)
        append_ds2 = np.random.rand(2, 5)
        check_ds = np.random.rand(7, 3)
        check_info = {'part': 'checks', 'columns':['very', 'good', 'dog']}
        append_dir = os.path.join(dir_name, "append")
        datasets = {'a': append_ds1, 'validinputs': append_ds2, 'checks': check_ds}
        infos = {'a': inf1, 'validinputs': inf1, 'checks': check_info}
        append = Data.Run(append_dir, datasets=datasets, infos=infos)
        existing.merge_with(append)
        tst.assert_allclose(existing.a.a0, np.vstack((ds1, append_ds1))[:, 0])
        tst.assert_allclose(existing.validinputs.a4, np.vstack((ds1, append_ds2))[:, 4])
        tst.assert_allclose(existing.checks.dog, check_ds[:, 2])

                          


        





