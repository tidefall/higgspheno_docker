import numpy as np
from matplotlib import pyplot as plt
import InputTools, Data


def a_pm(lambda1, lambda2, lambda3, lambda4, lambda5):
    radicand = (lambda1 - lambda2)**2 + (4/9)*(2*lambda3 + lambda4)**2
    term1 = (3/2)*(lambda1 + lambda2)
    term2 = (3/2)*np.sqrt(radicand)
    return term1 + term2, term1 - term2

def b_pm(lambda1, lambda2, lambda3, lambda4, lambda5):
    radicand = (lambda1 - lambda2)**2 + 4*lambda4**2
    term1 = (1/2)*(lambda1 + lambda2)
    term2 = (1/2)*np.sqrt(radicand)
    return term1 + term2, term1 - term2

def c_pm(lambda1, lambda2, lambda3, lambda4, lambda5):
    radicand = (lambda1 - lambda2)**2 + 4*lambda5**2
    term1 = (1/2)*(lambda1 + lambda2)
    term2 = (1/2)*np.sqrt(radicand)
    return term1 + term2, term1 - term2

def e_12(lambda1, lambda2, lambda3, lambda4, lambda5):
    e_1 = lambda3 + 2*lambda4 - 3*lambda5
    e_2 = lambda3 - lambda5
    return e_1, e_2

def f_pm(lambda1, lambda2, lambda3, lambda4, lambda5):
    f_p = lambda3 + 2*lambda4 + 3*lambda5
    f_m = lambda3 + lambda5
    return f_p, f_m

def g_12(lambda1, lambda2, lambda3, lambda4, lambda5):
    g = lambda3 + lambda4
    return g, g


def unitarity(run):
    lambdas = run.datasets['lambda']
    lambdas = lambdas.T[:5]
    names = ["a_p", "a_m", "b_p", "b_m", "c_p", "c_m",
             "e_1", "e_2", "f_p", "f_m", "g_1", "g_2"]
    functions = [a_pm, b_pm, c_pm, e_12, f_pm, g_12]
    terms = []
    for func in functions:
        print('.', end='', flush=True)
        terms += list(func(*lambdas))
    terms = np.vstack(terms)
    passing = np.abs(terms) < 16 * np.pi
    return np.all(passing, axis=0), names, terms


def compare_unitarity():
    run_path = InputTools.get_dir_name("Please give the run to check; ")
    run1 = Data.Run(run_path)
    run_path = InputTools.get_dir_name("Please give the other run to check; ")
    run2 = Data.Run(run_path)
    thdmc_uni = np.concatenate((run1.uniquechecks.unitarity, run2.uniquechecks.unitarity))
    calc_uni1, names, terms1 = unitarity(run1)
    calc_uni2, names, terms2 = unitarity(run2)
    calc_uni = np.concatenate((calc_uni1, calc_uni2))
    matches = calc_uni == thdmc_uni
    if np.all(matches):
        print("Perfect match")
    else:
        print(f"{100*np.sum(matches)/len(matches)}% matches")
    n_plots = len(names) + 2
    rows = int(np.floor(np.sqrt(n_plots)))
    cols = int(np.ceil(n_plots/rows))
    terms = np.hstack((terms1, terms2))
    for tanb in set(run1.inputs.tanbeta):
        tb_mask1 = np.abs(run1.uniqueinputs.tanbeta - tanb) < 0.5
        tb_mask2 = np.abs(run2.uniqueinputs.tanbeta - tanb) < 0.5
        tb_mask = np.concatenate((tb_mask1, tb_mask2))
        fig, ax_array = plt.subplots(rows, cols, sharex=True, sharey=True)
        ax_array = ax_array.flatten()
        mH = np.concatenate((run1.uniqueinputs.mH, run2.uniqueinputs.mH))[tb_mask]
        mA = np.concatenate((run1.uniqueinputs.mA, run2.uniqueinputs.mA))[tb_mask]
        for ax, name, term in zip(ax_array, names, terms):
            term = term[tb_mask]
            positive = np.abs(term) < 16 *np.pi
            negative = ~positive
            disagrees = np.logical_and(negative, thdmc_uni[tb_mask])
            neg_agree = np.logical_and(negative, ~disagrees)
            ax.scatter(mH[positive], mA[positive], c='green', label='term $< 16\\pi$', marker=',')
            ax.scatter(mH[neg_agree], mA[neg_agree], c='purple', label='term $> 16\\pi$ and 2HDMC says not unitary', marker=',')
            ax.scatter(mH[disagrees], mA[disagrees], c='red', label='term $> 16\\pi$ and 2HDMC says unitary (disagreement)', marker=',')
            ax.set_xlabel("$m_H$")
            ax.set_ylabel(f"${name}$ ~ $m_A$")
        # give thdmc itself
        ax = ax_array[-1]
        ax.scatter(mH[thdmc_uni[tb_mask]], mA[thdmc_uni[tb_mask]], c='yellow', label='2HDMC says unitary', marker=',')
        ax.scatter(mH[~thdmc_uni[tb_mask]], mA[~thdmc_uni[tb_mask]], c='blue', label='2HDMC says not unitary', marker=',')
        ax.set_xlabel("$m_H$")
        ax.set_ylabel(f"2HDMC ~ $m_A$")
        ax = ax_array[-2]
        ax.scatter([], [], c='yellow', label='2HDMC says unitary (bottom right plot)', marker=',')
        ax.scatter([], [], c='blue', label='2HDMC says not unitary (bottom right plot)', marker=',')
        ax.scatter([], [], c='green', label='term $< 16\\pi$', marker=',')
        ax.scatter([], [], c='purple', label='term $> 16\\pi$ and 2HDMC says not unitary', marker=',')
        ax.scatter([], [], c='red', label='term $> 16\\pi$ and 2HDMC says unitary (disagreement)', marker=',')
        ax.axis('off')
        ax.legend()
        ax = ax_array[-3]
        ax.axis('off')
        fig.suptitle(f"Type {run1.infos['inputs']['yukawa']}, $tan(\\beta) = {tanb}$")
        plt.show()
        input("Press enter to continue")
    return calc_uni, terms

if __name__ == '__main__':
    compare_unitarity()
    


