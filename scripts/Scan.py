""" the foil to MonteCarlo.py - calculates a predefined range of points """
import subprocess
import time
import sys
import fcntl
import os
import numpy as np
import InputTools
import MonteCarlo
import Data
import fit_m122
import debug
from ipdb import set_trace as st
AtoZH='A->ZH'
HtoZA='H->ZA'

def define_scope(scope={}):
    """
    Create or check the scope for a run

    Parameters
    ----------
    scope : dict
         (Default value = {})
         If checking scope or adding to existing scope 
         this should be a dictionary of scope

    Returns
    -------
    scope : dict
        generated and checked scope

    """
    process = InputTools.list_complete("Which process? ", [AtoZH, HtoZA]).strip()
    print("process = {}".format(process))
    yukawa_map = {'TypeI': 1, 'TypeII': 2,
                  'TypeIII': 3, 'TypeIV': 4}
    if 'yukawa' not in scope:
        yukawa = InputTools.list_complete("Chose yukawa type; ", yukawa_map.keys())
        yukawa = yukawa_map.get(yukawa, int(yukawa))
        scope['yukawa'] = yukawa
        if yukawa in [1, 2, 3]:
            tanb = np.array([1., 5., 10., 20.])
        elif yukawa == 4:
            tanb = np.array([1., 2., 3.])
        else:
            raise NotImplementedError
    print("yukawa type = {}".format(scope['yukawa']))
    column_names = ['mH', 'mA']
    defaults = {'tan(beta)': (tanb, None)}
    singles = {'sin(beta-alpha)': 1.,
               'step': 10.}
    if process == AtoZH:
        defaults = {**defaults,
                    'mins': (np.array([130., 230.]), column_names),
                    'maxes': (np.array([700., 800.]), column_names)}
        singles = {**singles,
                    'min(mA-mH)': 100.}
    elif process == HtoZA:
        defaults = {**defaults,
                    'mins': (np.array([230., 130.]), column_names),
                    'maxes': (np.array([800., 700.]), column_names)}
        singles = {**singles,
                    'min(mH-mA)': 100.}
    else:
        raise NotImplementedError
    for name, (default, cols) in defaults.items():
        if name not in scope:
            scope[name] = InputTools.select_values(name, cols, default)
    for name, default in singles.items():
        if name not in scope:
            scope[name] = InputTools.select_value(name, default)
    default_mC_pair = 'mA' if process == AtoZH else 'mH'
    if InputTools.yesNo_question("Fix mC={}? ".format(default_mC_pair)):
        scope['mC'] = default_mC_pair
    else:
        scope['mC'] = InputTools.get_literal("Enter a value in GeV for mC; ", float)
    #curves = InputTools.get_dir_name("Give the directory for curves for m122 in this run (blank for None)? ")
    #if curves:
    #    scope['m12_2_curves'] = curves.strip()
    if InputTools.yesNo_question("Allow m12_2 to vary until the points are valid? "):
        scope['m12_2'] = 'less than mA**2*tan(beta)/(1+tan**2(beta))'
        try:
            num_tries = int(input("How many do you want to try? ").strip())
            if num_tries < 0:
                raise ValueError
        except ValueError:
            print("Not a valid response, trying only 1 point")
            num_tries = 1
        scope['num_tries'] = num_tries
        print("checking up to {} points".format(num_tries))
        if InputTools.yesNo_question("Fix number of sucesses? "):
            try:
                collection_limit = int(input("How many do you want to obtain? ").strip())
                if collection_limit < 0:
                    raise ValueError
            except ValueError:
                print("Not a valid response, collecting only 1 valid point")
                collection_limit = 1
            scope['collection_limit'] = collection_limit
            print("collecting up to {} valid points".format(collection_limit))
        if InputTools.yesNo_question("Add a minimum m122 ceiling? "):
            scope["min_m122_ceil"] = InputTools.select_value("min_m122_ceil", 5000)
    else:
        scope['m12_2'] = 'mA**2*tan(beta)/(1+tan**2(beta))'
    return scope


def generate_points(scope, batch_size, last_values=None):
    # interpret the scope
    if scope['mC'] == 'mA':
        mC_eq_mA = True
        mC_eq_mH = False
    elif scope['mC'] == 'mH':
        mC_eq_mA = False
        mC_eq_mH = True
    else:
        mC_eq_mA = False
        mC_eq_mH = False
        mC = float(scope['mC'])
    yukawa = int(scope['yukawa'])
    # value that we should not try m122 above
    min_ceil = scope.get("min_m122_ceil", 0.)
    m122_ceil = lambda tanb, mass : np.max(min_ceil, MonteCarlo.gen_m12_2(mass, tb, random=False))
    # chose the m122 generation method
    if 'm12_2_curves' in scope:
        curves = fit_m122.CoreCurves(scope['m12_2_curves'])
        m122_func = lambda tanb, mH, mA, mC: curves.m122(yukawa, mH, mA, mC, tanb)
    else:
        if mC_eq_mA:
            m122_func = lambda tanb, mH, mA, mC: MonteCarlo.gen_m12_2(mA, tanb, random=False)
        else:
            m122_func = lambda tanb, mH, mA, mC: MonteCarlo.gen_m12_2(mH, tanb, random=False)
    if scope['m12_2'] == 'mA**2*tan(beta)/(1+tan**2(beta))':
        m12_adjust = False
    elif scope['m12_2'] == 'less than mA**2*tan(beta)/(1+tan**2(beta))':
        m12_adjust = True
        max_tries = scope['num_tries']
        if 'collection_limit' in scope:
            keep_all = False
            collection_limit = scope['collection_limit']
        else:
            keep_all = True
            collection_limit = None
        num_found = 0
    else:
        raise NotImplementedError
    num_cols = 6
    batch_points = np.empty((batch_size, num_cols), dtype=float)
    index_reached = 0
    step = scope['step']
    min_mH, min_mA = scope['mins']
    max_mH, max_mA = scope['maxes']
    mH_steps = int(np.ceil((max_mH-min_mH)/step))
    mA_steps = int(np.ceil((max_mA-min_mA)/step))
    bounds = MonteCarlo.Bounds(scope)
    if 'min(mA-mH)' in scope:
        gap = scope['min(mA-mH)']
        # 700.......
        #    ......
        # mA ....
        #    ...
        #    ..
        # 100.
        #    100 mH 700
        min_x, max_x = min_mH, max_mH
        min_y, max_y = min_mA, max_mA
        def mHmAorder(x, y):
            return x, y
    elif 'min(mH-mA)' in scope:
        gap = scope['min(mH-mA)']
        # 700.......
        #    ......
        # mH ....
        #    ...
        #    ..
        # 100.
        #    100 mA 700
        min_x, max_x = min_mA, max_mA
        min_y, max_y = min_mH, max_mH
        def mHmAorder(x, y):
            return y, x
    sba = scope['sin(beta-alpha)']
    if last_values is None:  #start from the begingin
        tb_range = scope['tan(beta)']
        x, y = mHmAorder(min_mH, min_mA)
    else:
        # tan beta is the outerloop, so crop it to incomplete values only
        tb_range = tb_range[tb_range>= last_values[4]]
        x, y = mHmAorder(last_values[0]+step, last_values[1]+step)  # move to the next mH, mA
    # set up for quickly calling bounds
    command = np.array(['mH', 'mA', 'mC', str(sba), 'tb', 'm12_2'],
                        dtype=str)
    for tb in tb_range:
        print("tan(beta) = {}".format(tb), flush=True)
        command[4] = str(tb)
        while y <= max_y:
            x_cap = min(max_x, y-gap)
            while x <= x_cap:
                if m12_adjust:
                    print("\ty = {}, x = {}".format(y, x), end='\r', flush=True)
                mH, mA = mHmAorder(x, y)
                if mC_eq_mA:
                    mC = mA
                elif mC_eq_mH:
                    mC = mH
                command[0] = str(mH)
                command[1] = str(mA)
                command[2] = str(mC)
                try:
                    m12_2 = m122_func(tb, mH, mA, mC)
                except FileNotFoundError:
                    m12_2 = m122_ceil(tb, y)
                command[5] = str(m12_2)
                if m12_adjust:  # if we are allowed to keep reducing m12^2 until the checks pass
                    ceil_here = m122_ceil(tb, mA)
                    for _ in range(max_tries):
                        # cheching negative values too
                        try_m12_2 = ceil_here*np.random.rand()
                        command[5] = str(try_m12_2)
                        checks = bounds.check(command)
                        if np.all(checks) or keep_all:
                            num_found += 1
                            batch_points[index_reached] = [mH, mA, mC, sba, tb, try_m12_2]
                            # increment the index, and yeald if we reached the batch size
                            index_reached += 1
                            if index_reached == batch_size:
                                index_reached = 0
                                yield batch_points.copy()
                            if not keep_all and num_found == collection_limit:
                                break  # stop trying even if we haven't hit max tries
                    if num_found == 0 and not keep_all:
                        batch_points[index_reached] = [mH, mA, mC, sba, tb, try_m12_2]
                        # increment the index, and yeald if we reached the batch size
                        index_reached += 1
                        if index_reached == batch_size:
                            index_reached = 0
                            yield batch_points.copy()
                    num_found = 0
                else: # not ajusting m122
                    batch_points[index_reached] = [mH, mA, mC, sba, tb, m12_2]
                    # increment the index, and yeald if we reached the batch size
                    index_reached += 1
                    if index_reached == batch_size:
                        index_reached = 0
                        yield batch_points.copy()
                x += step
            # reset x
            x = min_x
            y += step
        # reset y
        y = min_y
    # yield one last time at the end
    yield(batch_points[:index_reached])


def systematic(scope, batch_size):
    if scope['mC'] == 'mA':
        mC_eq_mA = True
        mC_eq_mH = False
    elif scope['mC'] == 'mH':
        mC_eq_mA = False
        mC_eq_mH = True
    else:
        mC_eq_mA = False
        mC_eq_mH = False
        mC = float(scope['mC'])
    yukawa = int(scope['yukawa'])
    # value that we should not try m122 above
    m122_ceil = lambda tanb, mass : MonteCarlo.gen_m12_2(mass, tb, random=False) + 5000
    max_tries = scope['num_tries']
    num_cols = 6
    batch_points = np.empty((batch_size, num_cols), dtype=float)
    index_reached = 0
    step = scope['step']
    min_mH, min_mA = scope['mins']
    max_mH, max_mA = scope['maxes']
    mH_steps = int(np.ceil((max_mH-min_mH)/step)) + 1
    mA_steps = int(np.ceil((max_mA-min_mA)/step)) + 1
    bounds = MonteCarlo.Bounds(scope)
    if 'min(mA-mH)' in scope:
        gap = scope['min(mA-mH)']
        # 700.......
        #    ......
        # mA ....
        #    ...
        #    ..
        # 100.
        #    100 mH 700
        min_x, max_x = min_mH, max_mH
        min_y, max_y = min_mA, max_mA
        x_steps, y_steps = mH_steps, mA_steps
        def mHmAorder(x, y):
            return x, y
    elif 'min(mH-mA)' in scope:
        gap = scope['min(mH-mA)']
        # 700.......
        #    ......
        # mH ....
        #    ...
        #    ..
        # 100.
        #    100 mA 700
        min_x, max_x = min_mA, max_mA
        min_y, max_y = min_mH, max_mH
        x_steps, y_steps = mA_steps, mH_steps
        def mHmAorder(x, y):
            return y, x
    sba = scope['sin(beta-alpha)']
    tb_range = scope['tan(beta)']
    x, y = mHmAorder(min_mH, min_mA)
    # make grids of unitarity, perturbativity and stability values that worked
    unit = [[[] for _ in range(x_steps)] for _ in range(y_steps)]
    pert = [[[] for _ in range(x_steps)] for _ in range(y_steps)]
    stab = [[[] for _ in range(x_steps)] for _ in range(y_steps)]
    passes = [stab, unit, pert]
    n_passes = len(passes)
    # set up for quickly calling bounds
    command = np.array(['mH', 'mA', 'mC', str(sba), 'tb', 'm12_2'],
                        dtype=str)
    for tb in tb_range:
        row = col = 0
        print("tan(beta) = {}".format(tb), flush=True)
        command[4] = str(tb)
        while y <= max_y:
            x_cap = min(max_x, y-gap)
            while x <= x_cap:
                print("\ty = {}, x = {}".format(y, x), end='\r', flush=True)
                mH, mA = mHmAorder(x, y)
                if mC_eq_mA:
                    mC = mA
                elif mC_eq_mH:
                    mC = mH
                command[0] = str(mH)
                command[1] = str(mA)
                command[2] = str(mC)
                neighbours = [[] for _ in range(n_passes)]
                if row > 0:
                    for i in range(n_passes):
                        neighbours[i] += passes[i][row-1][col]
                if row < y_steps-1:
                    for i in range(n_passes):
                        neighbours[i] += passes[i][row+1][col]
                if col > 0:
                    for i in range(n_passes):
                        neighbours[i] += passes[i][row][col-1]
                if col < x_steps-1:
                    for i in range(n_passes):
                        neighbours[i] += passes[i][row][col+1]
                m122, found = m122_finder(bounds, command, neighbours,
                                          max_tries=max_tries)
                # record the indervidual findings
                for i in range(n_passes):
                    passes[i][row][col] += found[i]
                # whatever the result add it to the batch
                batch_points[index_reached] = [mH, mA, mC, sba, tb, m122]
                # increment the index, and yeald if we reached the batch size
                index_reached += 1
                if index_reached == batch_size:
                    index_reached = 0
                    yield batch_points.copy()
                x += step
                col += 1
            # reset x
            x = min_x
            col = 0
            y += step
            row += 1
        # reset y
        y = min_y
        row = 0


def m122_finder(bounds, command, neighbours, default_value=70000, max_tries=50000):
    m122_idx = 5
    found = [[] for _ in neighbours]
    n_checks = len(neighbours)
    # decide on start points from neighbours
    mins = np.fromiter((np.min(n, initial=default_value) for n in neighbours), dtype=float)
    maxes = np.fromiter((np.max(n, initial=default_value) for n in neighbours), dtype=float)
    # find the midpoint 
    global_mid = 0.5*(np.max(mins) + np.min(maxes))
    global_range = np.max(maxes) - np.min(mins)
    starts = np.full(n_checks, global_mid)
    starts[maxes < global_mid] = maxes[maxes < global_mid]
    starts[mins > global_mid] = mins[mins > global_mid]
    # try to find valid points here
    passes = np.full(n_checks, np.nan)
    for i, start in enumerate(starts):
        step_size = 2*max(start, global_range)/max_tries
        if not np.isnan(passes[i]):
            continue
        # search with an expanding range
        for t in range(max_tries):
            m122 = start + t*step_size*(np.random.rand()-0.5)
            command[m122_idx] = str(m122)
            pset, *checks = bounds.check(command, require_bounds=False)
            if not pset:  # some failure
                continue
            for c in np.where(checks)[0]:
                found[c].append(m122)
            if np.all(checks):  # all checks at once is a perfect win
                passes[:] = m122
                break  # the continue clause will deal with the outer loop
            else:  # could optimise this futher, but probably not worth it
                mask = np.logical_and(checks, np.isnan(passes))
                passes[mask] = m122
    # we no longer ascribe the passes to indervidual checks
    last_pass = list(set(passes[~np.isnan(passes)]))
    # if there are 1 or zero values found return now
    if not last_pass:
        return global_mid, found
    elif len(last_pass) == 1:
        return last_pass[0], found
    # else try to move these values together
    # we must never move so that a value that was passing starts failing
    which_pass = [passes==l for l in last_pass]
    last_mid = np.mean(last_pass)
    # should the measures progress up or down
    seek_lower = np.full_like(last_pass, True, dtype=bool)
    # if the last_passing value is greater than the last min we need to drive down
    # make bounds, the boun is in the dirsction we are seeking
    upper_bounds = np.full_like(last_pass, np.max(last_pass))
    lower_bounds = np.full_like(last_pass, np.min(last_pass))
    i = 0
    min_movement = 0.1
    max_attempts = 1000000
    for step in range(max_attempts):  # keep trying until all points pass together
        i = (i+1)%len(last_pass)
        from_last = 0.3*last_pass[i]
        m122 = from_last + 0.7*last_mid  # ideally move towards the mid
        direction = m122 - last_pass[i]
        if abs(direction) < min_movement:
            # then we are all super close
            return last_mid, found
        if direction > 0:
            cap = from_last + 0.7*upper_bounds[i]
            m122 = min(cap, m122)  # don't go more than 70% to the upper bound
        else:
            base = from_last + 0.7*lower_bounds[i]
            m122 = max(base, m122)  # don't go more than 70% to the lower bound
        command[m122_idx] = str(m122)
        pset, *checks = bounds.check(command, require_bounds=False)
        checks = np.array(checks)
        if not pset:
            # default fail, consider this a bound
            if direction > 0:
                upper_bounds[i] = m122
            else:
                lower_bounds[i] = m122
        else:
            for c in np.where(checks)[0]:
                found[c].append(m122)
            if np.all(checks):
                return m122, found
            if not np.all(checks[which_pass[i]]):
                # checks that passed before need to stay passing
                if direction > 0:
                    upper_bounds[i] = m122
                else:
                    lower_bounds[i] = m122
            else:
                # if we reach here at least as many points pass as before
                which_pass[i] = checks
                last_pass[i] = m122
                last_mid = np.mean(last_pass)
        # if any upper bounds are lower than any lower bounds the system is unsolvable
        if np.min(upper_bounds) < np.max(lower_bounds):
            # just return the last pass value wiht the most checks passing
            best = np.argmax([np.sum(p) for p in which_pass])
            return last_pass[best], found
    # we we get here we reached max attempts
    st()
    best = np.argmax([np.sum(p) for p in which_pass])
    chosen = last_pass[best]
    return chosen, found


def main():
    """ Run the monty carlo generation of points."""

    if os.path.exists('continue'):
        run_condition = lambda: os.path.exists('continue')
    else:
        if InputTools.yesNo_question("Would you like to do a time based run? "):
            run_time = InputTools.get_time("How long should it run?")
            stop_time = time.time() + run_time
            run_condition = lambda: time.time() < stop_time
        elif InputTools.yesNo_question("Would you like to create a continue file?"
                                       +" (be sure you can delete it while the"
                                       +" program is running!) "):
            open('continue', 'w').close()
            run_condition = lambda: os.path.exists('continue')
        else:
            return 
    dir_name = InputTools.get_dir_name("Name the save directory: ")
    inputs_columns = ["$m_H$", "$m_A$", "$m_C$", "$sin(\\beta - \\alpha)$",
                      "$tan(\\beta)$", "$m_{12}^2$"]
    scope = define_scope()
    #InputTools.last_selections.write("scan_type1_inps.dat")
    batch_size = 500
    m12_2_step = 0.05
    last_inputs = None
    if os.path.exists(dir_name):
        run = Data.Run(dir_name)
        inputs = run.datasets.get('inputs', None)
        if inputs is not None:
            print("Continuing existing run")
            last_inputs = inputs[-1]
            print("Working on line number {}".format(len(inputs)))
    run = Data.Run(dir_name)
    batch_generator = systematic(scope, batch_size)
    inputs_info = scope
    inputs_info['m12_2_step'] = m12_2_step
    inputs_info['columns'] = inputs_columns
    inputs_info['dtype'] = float
    inputs_info['part'] = 'input'
    checks_info = {'dtype': bool,
                   'part': 'checks',
                   'columns': ["parameters are valid",
                               "check stability",
                               "check unitarity",
                               "check perturbativity",
                               "HiggsBounds",
                               "flavour constraints"]}
    valid_path = False
    while run_condition():
        try:
            # chose the inputs
            inputs = next(batch_generator)
        except StopIteration:
            break
        run.add_ds('inputs', inputs_info, inputs, mode='append')
        #check validity
        checks = MonteCarlo.check_bounds(inputs, inputs_info)
        run.add_ds('checks', checks_info, checks, mode='append')
        # make a valid-only dataset
        mask = np.all(checks[:, 1:], axis=1)
        mask[:] = True
        if not np.any(mask):
            run.write()
            continue
        valid_inputs = inputs[mask]
        run.add_ds('validinputs', inputs_info, valid_inputs, mode='append')
        run.write()  # the branching ratios wil read the valid ds from file
    unique_path = run.add_unique()
    # generate the branching ratios
    infos = MonteCarlo.generate_brs_lambdas(unique_path, inputs_info, dir_name)
    run.refresh_ds_from_file(infos)
    run.write()
    print("Done. Now run SusHiScan.py to add the cross Sections.")

if __name__ == '__main__':
    # InputTools.pre_selections = InputTools.PreSelections("scan_type1_inps.dat")
    main()

