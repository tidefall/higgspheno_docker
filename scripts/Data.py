""" Module to read or write a run, cross compatible with python 2 or 3 
A run consists of a directory with some info files (extention = .info)
                               and some dataset files (extention = .csv)
Every dataset must have an info file,
   but there can be info files for datasets that don't exist yet

In memory the info file is a dictionary.
On disk the format of an info file is;
key1|value1
key2|value2
...
every info file should have a dtype and a columns key;
dtype|<type 'float'>
columns|["column name 1", "column name 2"]
...
Attempts will be made to parse the value into a sensible variable,
   if all else fails it will be read in as a string.

In memory the dataset is a numpy array.
On disk the dataset has the format;
#column name 1|column name 2|column name 3
x11|x12|x13
x21|x22|x23
...
Excluding some special cases (see diferent_length list)
  it is assumed that each row of the dataset is one point of data.


"""
from __future__ import print_function
import os
import ast
import numpy as np
import shutil
import pickle
import debug
#from ipdb import set_trace as st
import InputTools

def soft_generic_equality(a, b):
    """
    

    Parameters
    ----------
    a :
        
    b :
        

    Returns
    -------

    """
    try:
        # floats, ints and arrays of these should work here
        return np.allclose(a, b, equal_nan=True)
    except TypeError:
        try:  # strings and bools should work here
            return a == b
        except Exception:
            # for whatever reason this is not possible
            if len(a) == len(b): 
                # by this point it's not a string
                # maybe a tuple of objects, try each one seperatly
                sections = [soft_generic_equality(aa, bb) for aa, bb in zip(a, b)]
                return np.all(sections)
            return False  # diferent lengths


def possible_run(dir_name):
    '''
    Check if the specifed directory has the format required to be a run

    Parameters
    ----------
    dir_name : str
       name of directory to check
        

    Returns
    -------
    : bool
       True for a possible run, false otherwise

    '''
    file_names = os.listdir(dir_name)
    info_names = [f for f in file_names if f.endswith(Run.info_suffix)]
    ds_names = [f for f in file_names if f.endswith(Run.ds_suffix)]
    if len(ds_names) > 0 and len(ds_names) <= len(info_names):
        return True
    return False


def get_possible_runs(path_start):
    '''
    Return list of subdirectories that could be runs

    Parameters
    ----------
    path_start : str
       super directory to look in
        

    Returns
    -------
    possibles : list of str
        full paths of subdirectories that could be runs 

    '''
    folder_names = os.listdir(path_start)
    possibles = []
    for name in folder_names:
        path = os.path.join(path_start, name)
        try:
            if possible_run(path):
                possibles.append(path)
            else:
                print("{} does not seem to be a run".format(path))
                print(os.listdir(path))
        except OSError:
            print("{} does not seem to be a run".format(path))
    return possibles


def merge_superdir(super_dir, possible_runs=None):
    '''
    Try to merge all runs found in the directory.
    Safe - will only delete folders that have been sucessfully merged

    Parameters
    ----------
    super_dir : str
        path to directory containing runs
        

    Returns
    -------
    first_run : str
        path to merged run

    '''
    if possible_runs is None:
        possible_runs = get_possible_runs(super_dir)
    first_run = None
    first_index = 0
    while first_run is None:
        try:
            first_run = Run(possible_runs[first_index])
        except:
            print("{} not a run".format(possible_runs[first_index]))
            first_index +=1
        if first_index == len(possible_runs):
            print("No runs in dir")
            return 1
    for name in possible_runs[first_index+1:]:
        try:
            run_here = Run(name)
        except:
            print("{} not a run".format(name))
            continue
        first_run.merge_with(run_here)
        shutil.rmtree(run_here.dir_name)
    first_run.write()
    return first_run


class Run:
    """ Structured data about parameter points in 2HDM

    Parameters
    ----------
    dir_name : str
        path to the run directory (new or existing)

    A run consists of a directory with some info files (extention = .info)
                                   and some dataset files (extention = .csv)
    Every dataset must have an info file,
       but there can be info files for datasets that don't exist yet
    
    In memory the info file is a dictionary.
    On disk the format of an info file is;
    key1|value1
    key2|value2
    ...
    every info file should have a dtype and a columns key;
    dtype|<type 'float'>
    columns|["column name 1", "column name 2"]
    ...
    Attempts will be made to parse the value into a sensible variable,
       if all else fails it will be read in as a string.
    
    In memory the dataset is a numpy array.
    On disk the dataset has the format;
    #column name 1|column name 2|column name 3
    x11|x12|x13
    x21|x22|x23
    ...
    Excluding some special cases (see diferent_length list)
      it is assumed that each row of the dataset is one point of data.
  """
    info_suffix = '.info'
    ds_suffix = '.csv'
    sep = '|'
    diferent_length = ['inputs', 'checks', 'validinputs']
    dtype_dict = {'float': float, 'int': int, 'bool': bool}
    def __init__(self, dir_name, datasets=None, infos=None):
        self.dir_name = dir_name
        try:
            os.mkdir(dir_name)
        except OSError:
            # if the directory already exists then datasets and infos
            # should not be passed
            assert datasets is None
            assert infos is None
            print("Not new.")
        if datasets is None:  # then read anything on the disk
            file_names = os.listdir(dir_name)
            # look for info files
            info_names = [f for f in file_names if f.endswith(Run.info_suffix)]
            self.infos = self._read_infos(info_names)
            # look for datasets
            self.datasets = {}
            self.refresh_ds_from_file()
        else:  # write the inputs to the disk
            self.infos = infos
            self.datasets = datasets  # hold datasets as np arrays
            for key in self.datasets:
                self.datasets[key] = np.array(self.datasets[key])
            self._write_infos()
            self._write_dsets()
        self._make_attr_dicts()

    def refresh_ds_from_file(self, new_infos={}):
        '''
        Read or reread the datasets from the run directory.
        Useful after cauculating brs becuase they are written directly to disk
        hopefully in the run directory.
        

        Parameters
        ----------
        new_infos : dict of dict
             (Default value = {})
             If this read is going to pick up new datasets,
             supply their info dicts.
             each key should eb the basename of a dataset (datasets being dir_name/basename.csv)
             each value should be the info dictionary, with at least the dtype and columns

        '''
        # write out current status
        self.write()
        # now clear datasets
        self.datasets = {}
        # if new infos have been provided, add them
        for name, info in new_infos.items():
            self.infos[name] = info
        file_names = os.listdir(self.dir_name)
        # look for datasets
        ds_names = [f for f in file_names if f.endswith(Run.ds_suffix)]
        basenames = self.basenames()
        # each of the datasets should have a unique assocated info
        # but we may have infor for databases we have yet to create
        assert len(ds_names) <= len(basenames)
        for ds_name in ds_names:
            base = ds_name[:-len(Run.ds_suffix)]
            path = os.path.join(self.dir_name, ds_name)
            assert base in basenames
            dtype = self.infos[base]['dtype']
            # add this dataset
            if dtype == bool:
                # bools read in as ints
                read_in = np.genfromtxt(path, dtype=int, delimiter=Run.sep)
                dset = read_in.astype(dtype)
            else:
                dset = np.genfromtxt(path,
                                     dtype=dtype,
                                     delimiter=Run.sep)
            if len(dset.shape) == 1:
                dset = dset.reshape((1, -1))
            self.datasets[base] = dset
        # where needed read the columns from the headder of the dataset
        for name in self.datasets:
            if 'columns' not in self.infos[name]:
                path = os.path.join(self.dir_name, name+Run.ds_suffix)
                with open(path, 'r') as ds_file:
                    header = ds_file.readline()[1:-1]
                columns = [c for c in header.split(Run.sep) if len(c) > 0]
                self.infos[name]['columns'] = columns
            else:
                dataset = self.datasets[name]
                if len(self.datasets[name]) > 0:
                    n_columns = len(self.infos[name]['columns'])
                    if n_columns == 1 and dataset.shape[1] != 1 and dataset.shape[0] == 1:
                        # this seems to probelm with reading in one column
                        self.datasets[name] = dataset.T
                    else:
                        assert n_columns == dataset.shape[1],\
                               "problem with {}, has {}".format(name, n_columns) + \
                               "names in columns, but dataset is len {}".format(dataset.shape)
        # legacy compatability
        # previous runs used 'validinputs' (inaccuralty at times) in place of 'uniqueinputs'
        changed_names = [('validinputs', 'uniqueinputs'),
                         ('validchecks', 'uniquechecks')]
        for new_name, old_name in changed_names:
            if old_name in self.infos and new_name not in self.infos:
                self.infos[new_name] = self.infos[old_name]
                if old_name in self.datasets:
                    self.datasets[new_name] = self.datasets[old_name]

    @classmethod
    def strip_column(cls, column_name):
        ''' Simplify a coulmn name to it can be an attribute '''
        substitute = [("check", ""), ("rightarrow", "TO"), ('H+', 'Hplus'), ('H-', 'Hminus')]
        stripped_name = column_name
        # substite first then strip
        # otherwise H- and H+ lose these signs
        for subs in substitute:
            stripped_name = stripped_name.replace(*subs)
        stripped_name = ''.join(s for s in stripped_name if s.isalpha() or s.isdigit())
        if stripped_name[0].isdigit():
            stripped_name = 'x' + stripped_name
        return stripped_name

    def _make_attr_dicts(self):
        for name in self.datasets:
            this_dict = {self.strip_column(col): self.datasets[name][:, i]
                         for i, col in enumerate(self.infos[name]['columns'])}
            attr_dict = AttributesDict(this_dict, name)
            setattr(self, name, attr_dict)
            name_dict = {self.strip_column(col): col
                         for i, col in enumerate(self.infos[name]['columns'])}
            attr_name_dict = AttributesDict(name_dict, 'n_' + name)
            setattr(self, 'n_' + name, attr_name_dict)

    def _read_infos(self, info_names):
        '''
        Read the info from file.        
        Attempts to parse the values as variables where possible
        e.g. '1'->1

        Parameters
        ----------
        info_names : str
            the names of the info files on the disk
            e.g. basename.info

        '''
        infos = {}
        for name in info_names:
            path = os.path.join(self.dir_name, name)
            with open(path, 'r') as ifile:
                lines = ifile.readlines()
            attributes = {}
            for line in lines:
                if line.isspace():
                    continue
                # the first word is the name of the attribute
                attr_name, attr_string = line.split(Run.sep, 1)
                attr_string = attr_string[:-1]
                try:
                    attr = ast.literal_eval(attr_string)
                except (ValueError, SyntaxError) as e:
                    type_dict = {'float':float, 'int':int, 'bool': bool}
                    # if litral eval fails, do some guessing
                    if attr_string in info_names or attr_name=='part':
                        # it's the part name
                        attr = attr_string
                    elif (attr_string.startswith("<type") or
                        attr_string.startswith("<class")):
                        # it's an oddly formed type
                        if 'int' in attr_string: attr = int
                        elif 'float' in attr_string: attr = float
                        elif 'bool' in attr_string: attr = bool
                    elif attr_string in type_dict:
                        # its a normal type
                        attr = type_dict[attr_string]
                    elif attr_string.startswith('[') and attr_string.endswith(']'):
                        # probably a poorly formated numpy array
                        split_string = attr_string[1:-1].replace(',',' ').split()
                        attr = np.array(split_string).astype(float)
                    else:
                        print("Couldn't guess what {} was in {}".format(attr_string, path))
                        attr = attr_string
                attributes[attr_name] = attr
            infos[name[:-len(Run.info_suffix)]] = attributes
        return infos

    def _write_infos(self):
        ''' Write the info files to the disk'''
        for name in self.infos:
            # check the dtype and add if possible
            if 'dtype' not in self.infos[name] and name in self.datasets:
                dtype = self.datasets[name].dtype
                self.infos[name]['dtype'] = str(dtype)
            elif isinstance(self.infos[name]['dtype'], str):
                for key in self.dtype_dict:
                    if key in self.infos[name]['dtype']:
                        self.infos[name]['dtype'] = self.dtype_dict[key]
                        break
            file_name = name + Run.info_suffix
            path = os.path.join(self.dir_name, file_name)
            with open(path, 'w') as ifile:
                attributes = self.infos[name]
                for attr_name, attr in attributes.items():
                    # force numpy arrays to lists
                    if isinstance(attr, np.ndarray):
                        attr = attr.tolist()
                    ifile.write(attr_name + Run.sep + 
                                str(attr) + "\n")

    def _write_dsets(self):
        ''' Write the datasets to the disk '''
        for base_name, data in self.datasets.items():
            dtype = self.infos[base_name]['dtype']
            if isinstance(dtype, str):
                for key in self.dtype_dict.keys():
                    if key in dtype:
                        dtype = self.dtype_dict[key]
                        break
            dtype_fmt = {int: "%d", bool:"%d", float:"%.18e"}
            ds_name = os.path.join(self.dir_name, base_name + Run.ds_suffix)
            columns = Run.sep.join(self.infos[base_name]['columns'])
            np.savetxt(ds_name, data, header=columns, delimiter=Run.sep,
                       fmt=dtype_fmt[dtype])

    def write(self):
        ''' write everything to the disk '''
        self._write_infos()
        self._write_dsets()

    def basenames(self):
        '''Get all the bsenames of the files
        
        Returns
        -------
        : list of str
           list of basenames
        '''
        return list(self.infos.keys())

    def merge_with(self, run2):
        '''
        Add the contents of another run to this run.
        Both the other run and this run must be complete,
        in other words, no half filled datasets.

        Parameters
        ----------
        run2 : Run
          run to be added to this run

        '''
        # check they contain the same filenames
        basenames2 = run2.basenames()
        for name in self.basenames():
            assert name in basenames2 or name in self.diferent_length
        if 'uniqueinputs' in self.datasets:
            length = len(self.datasets['uniqueinputs'])
            length2 = len(run2.datasets['uniqueinputs'])
            # check each part has the same number of lines present
            for name in basenames2:
                if name in self.diferent_length: continue
                assert len(self.datasets[name]) == length, name
                assert len(run2.datasets[name]) == length2, name
        # now stick them together
        for name in basenames2:
            if name not in run2.datasets:
                continue
            if run2.datasets[name].shape == (0,):
                continue
            if name in self.diferent_length and name not in self.datasets:  # the diferent length datasets can be in both segments.....
                self.datasets[name] = run2.datasets[name]
                self.infos[name] = run2.infos[name]
            else:
                combined_part = np.vstack((self.datasets[name],
                                           run2.datasets[name]))
                # put it in the first dataset
                self.datasets[name] = combined_part
        self._make_attr_dicts()
 
    def split(self, n_fragments, new_dir):
        try:
            os.mkdir(new_dir)
        except FileExistsError:
            pass
        length = set([len(ds) for name, ds in self.datasets.items() if name not in self.diferent_length])
        assert len(length) == 1,  "All datasets should have the same length"
        length = list(length)[0]
        segment_length = int(length/n_fragments)
        split_points = [segment_length*i for i in range(n_fragments)] + [length]
        sub_dir_names = []
        for i in range(n_fragments):
            start = split_points[i]
            end = split_points[i+1]
            sub_dir_name = os.path.join(new_dir, "part_" + str(i))
            sub_dir_names.append(sub_dir_name)
            ds_here = {k: v[start:end] for k, v in self.datasets.items()
                       if k not in self.diferent_length}
            # put the odd lengthed ds in section 0
            if i == 0:
                for name in self.diferent_length:
                    if name in self.datasets:
                        ds_here[name] = self.datasets[name]
            Run(sub_dir_name, ds_here, self.infos)

    def add_ds(self, name, info, dataset, mode='overwrite'):
        '''
        Add a dataset to this run.
        If the dataset is alread written to disk dont use this,
        use refresh_ds_from_file.

        Parameters
        ----------
        name : str
           basename fo this dataset
            
        info : dict
           info dict for this dataset
            
        dataset : numpy array
           data
            

        Returns
        -------
        dataset_path : str
           Location the dataset will be on the disk.
           (not there until you call write)

        '''
        if 'columns' in info:
            assert len(info['columns']) == dataset.shape[1]
        if mode == 'overwrite':
            self.infos[name] = info
            self.datasets[name] = np.array(dataset)
        elif mode == 'append':
            if name in self.infos:
                for key in info:
                    assert soft_generic_equality(info[key], self.infos[name][key]), "{} not equal to {}".format(info[key], self.infos[name][key])
                if isinstance(dataset, list):
                    dataset = np.array(dataset)
                self.datasets[name] = np.vstack((self.datasets[name] , dataset))
            else:
                self.add_ds(name, info, dataset, mode='overwrite')
        dataset_path = os.path.join(self.dir_name, name+Run.ds_suffix)
        return dataset_path

    def add_unique(self, unique_requirements=None):
        self._make_attr_dicts()
        if unique_requirements is None:
            unique_requirements = {'inputs': ['mH', 'mA', 'tanbeta']}
        # identify the rows to keep
        unique_cols = np.vstack((getattr(getattr(self, dset), col)
                                for dset in unique_requirements
                                for col in unique_requirements[dset])).T
        invalid = {}  # format {inputs: idx}
        best_invalid = {}  # format {inputs: most_passes}
        valid = {}   # format {inputs: idx}
        for idx, row in enumerate(unique_cols):
            inputs_key = tuple(row)
            if inputs_key not in valid:
                # work out if this is valid
                checks_row = self.datasets['checks'][idx]
                if all(checks_row):
                    # if an invalid version was previously recorded delete it
                    invalid.pop(inputs_key, None)
                    valid[inputs_key] = idx
                elif inputs_key not in invalid or best_invalid[inputs_key] < sum(checks_row):
                    invalid[inputs_key] = idx
                    best_invalid[inputs_key] = sum(checks_row)
        # anything left in invalid at this point does not have a valid version
        unique_idxs = np.array(list(valid.values()) + list(invalid.values()))
        # make the inputs dataset
        info = {k:v for k, v in self.infos['inputs'].items()}
        info['part'] = 'uniqueinputs'
        dataset = self.datasets['inputs'][unique_idxs]
        unique_path = self.add_ds(info['part'], info, dataset)
        # make the checks dataset
        info = {k:v for k, v in self.infos['checks'].items()}
        info['part'] = 'uniquechecks'
        dataset = self.datasets['checks'][unique_idxs]
        self.add_ds(info['part'], info, dataset)
        # write the dataset
        self.write()
        return unique_path

    def make_pp(self, order="nnlo"):
        '''
        Combine the quark to higgs and gluon to higgs
        to make a rough proton to higgs cross section
        adds this as a new dataset

        Parameters
        ----------
        order : str
             (Default value = "nnlo")
             order at which to make to combination.

        '''
        self.remove_pp()
        cross_sec_name = 'crossSections'
        cross_sec_columns = self.infos[cross_sec_name]['columns']
        this_order = [name for name in cross_sec_columns
                      if  name.lower().strip().startswith(order.lower())]
        products = [self.strip_column(name).split("TO",1)[1].strip()[0]
                    for name in this_order]
        higges = "hHA"
        cross_secs = self.datasets[cross_sec_name]
        for higgs in higges:
            generates_this_higgs = [name for name, product in zip(this_order, products)
                                    if product==higgs]
            if len(generates_this_higgs) == 0: continue
            locations = [cross_sec_columns.index(name) for name in generates_this_higgs]
            column = np.sum([self.datasets[cross_sec_name][:, [l]] for l in locations], axis=0)
            column_name = "{} $\\sigma(p p \\rightarrow {})$ (pb)".format(order, higgs)
            cross_secs = np.hstack((cross_secs, column))
            cross_sec_columns.append(column_name)
        self.datasets[cross_sec_name] = cross_secs
        self.infos[cross_sec_name]['columns'] = cross_sec_columns
        # reset the attributes
        self._make_attr_dicts()

    def remove_pp(self):
        cross_sec_name = 'crossSections'
        cross_sec_columns = self.infos[cross_sec_name]['columns']
        for name in cross_sec_columns:
            if 'sigma' in name and 'p p' in name:
                self.remove_col(cross_sec_name, name)

    def remove_col(self, dataset_name, col_name):
        col_idx = self.infos[dataset_name]['columns'].index(col_name)
        del self.infos[dataset_name]["columns"][col_idx]
        ds = self.datasets[dataset_name]
        new_ds = np.empty((ds.shape[0], ds.shape[1]-1))
        new_ds[:, :col_idx] = ds[:, :col_idx]
        new_ds[:, col_idx:] = ds[:, col_idx+1:]
        self.datasets[dataset_name] = new_ds
        # need to recalculate locaitons
        self._make_attr_dicts()


class AttributesDict:
    def __init__(self, content_dict, dict_name="this"):
        self.content = content_dict
        self.name = dict_name

    def __getattr__(self, attr_name):
        # this way the dict updates properly
        if attr_name in self.content:
            return self.content[attr_name]
        raise AttributeError("No {} in {} dict".format(attr_name, self.name))

    def __dir__(self):
        return self.content.keys()


if __name__ == '__main__':
    run_dir = InputTools.get_dir_name("What run to use? ")
    if run_dir != '':
        run = Run(run_dir)
