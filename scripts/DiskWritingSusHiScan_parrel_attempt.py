""" 
This pyhton scrip will only run when sitting inside the SusHi directory

No cpp program required

Reads and writes to disk
"""
# make fstrings work in older python versions
# -*- coding: future_fstrings -*-
#from ipdb set_trace as st
import InputTools
import subprocess
import os
import sys
import numpy as np
import Data
import time
import multiprocessing


def parameter_position(input_name, block_name, param_number):
    ''' Get the position of a paramter in the input file.
    Will fail on blocks that don't have parameter numbers.
    

    Parameters
    ----------
    input_name : str
        name of the input file to search
        
    block_name : str
        name of the block to seek
        
    param_number : int
        number of the parameter to seek
        

    Returns
    -------
    line_number : int
        number of the line containing the parameter
        zero indexed

    charicter_number: int
        number of the first charcter of the parameter
        zero indexed

    '''
    with open(input_name, 'r') as sushi_file:
        # finding th eline number
        line_number = 0
        # get to the with block line
        block_line = "Block {}".format(block_name)
        block_len = len(block_line)
        while next(sushi_file)[:block_len] != block_line:
            line_number += 1
        # now get to the param number
        line = next(sushi_file)
        line_number += 1
        str_num = str(param_number)
        while line.split()[0] != str_num:
            line = next(sushi_file)
            line_number += 1
        # now get the charicter number
        charicter_number = line.index(str_num) + len(str_num)  # must be after the parameter number
        char = line[charicter_number]
        line_len = len(line)
        while (char == ' ' and charicter_number < line_len):
            char = line[charicter_number]
            charicter_number += 1
        charicter_number -= 1
    return line_number, charicter_number


def replace_parameter(input_name, line_number, charicter_number, new_value):
    ''' Function to replace one of the parameters in the input file.
    

    Parameters
    ----------
    input_name : str
        name of the file to replace in
        
    line_number : int
        number of the line containing the parameter
        zero indexed

    charicter_number: int
        number of the first charcter of the parameter
        zero indexed

    new_value : float
        new value the parameter should take

    '''
    with open(input_name, 'r+') as sushi_file:
        contents = sushi_file.readlines()
        change_line = contents[line_number]
        old_entry_len = len(change_line[charicter_number:].split()[0].split('#')[0])
        new_line = change_line[:charicter_number] + str(new_value) + change_line[charicter_number + old_entry_len:]
        contents[line_number] = new_line
        sushi_file.seek(0)
        sushi_file.truncate(0)
        sushi_file.writelines(contents)


def replace_parameters(input_name, line_numbers, charicter_numbers, new_values):
    ''' Function to replace any number of the parameters in the input file.
    

    Parameters
    ----------
    input_name : str
        name of the file to replace in
        
    line_numbers : list of int
        numbers of the line containing the parameter
        zero indexed

    charicter_numbers: list of int
        numbers of the first charcter of the parameter
        zero indexed

    new_values : list of float
        new values the parameters should take

    '''
    with open(input_name, 'r+') as sushi_file:
        contents = sushi_file.readlines()
        for line_n, char_n, val in zip(line_numbers, charicter_numbers, new_values):
            if line_n is None:
                continue
            change_line = contents[line_n]
            old_entry_len = len(change_line[char_n:].split()[0].split('#')[0])
            new_line = change_line[:char_n] + str(val) + change_line[char_n + old_entry_len:]
            contents[line_n] = new_line
        sushi_file.seek(0)
        sushi_file.truncate(0)
        sushi_file.writelines(contents)


def fetch_result(temp_name, block_names, parameter_numbers):
    ''' Read the output file for any required paramters
    also deletes the file after reading it. 

    Parameters
    ----------
    temp_name : str
        name of the file containing the output
        
    block_names : list of str
        names of the blocks containing the results
        
    parameter_numbers : list of int
        names of the parameter numbers that are the results
        

    Returns
    -------
    results : list of float
        all numbers found

    '''
    # also deletes the file once done
    with open(temp_name, 'r') as sushi_file:
        # an array of dummy values is faster
        results = [[None for _ in numbers] for numbers in parameter_numbers]
        in_block = False
        block_finished = np.full(len(block_names), False)
        block_number = -1
        # get to the with block line
        for line in sushi_file:
            if np.all(block_finished):
                break
            split_line = line.split()
            if len(split_line) < 2 or '#' in split_line[0]:
                continue
            elif split_line[0] == 'Block':
                # new block
                in_block = False
                # is this a block to read from?
                if split_line[1] in block_names:
                    in_block = True
                    block_number = block_names.index(split_line[1])
                    block_parameters = parameter_numbers[block_number]
            elif in_block:
                if int(split_line[0]) in block_parameters:
                    para_number = block_parameters.index(int(split_line[0]))
                    results[block_number][para_number] = float(split_line[1])
                    if not None in results[block_number]:
                        block_finished[block_number] = True
    os.remove(temp_name)
    return results


def run_scan(input_name, run, scan_param, low, high, steps):
    ''' Calculate cross sections for a range of values.
    

    Parameters
    ----------
    input_name : str
        name of the skeleton input file 2HDMC_physicalbasis.in
        
    run : Run
        collection of datasets to save output
        
    scan_param : str
        name of the parameter to scan in
        
    low : float
        start of the scan
        
    high : float
        end of the scan (inclusive)
        
    steps :int
        number of steps for the scan
        

    '''
    program = "./bin/sushi.2HDMC"
    block_name = "2HDMC"
    scan_dict = { "type": 2  , 
                  "tanbeta": 3,
                  "m12": 4,
                  "mh": 21,
                  "mH": 22,
                  "mA": 23,
                  "mC": 24,
                  "sinbetaalpha": 25,
                  "lambda6": 26,
                  "lambda7": 27}
    param_number = scan_dict[scan_param.lower()]
    line_number, charicter_number = parameter_position(input_name, block_name, param_number)
    temp_name = "/home/henry/tmp/scan_tmp.out"
    column_names = np.array([scan_param, "ggh cross section in pb",
                    "bbh cross section in pb"], dtype='S50')
    block_names = ["SUSHIggh", "SUSHIbbh"]
    parameter_numbers = [[1], [1]]
    values = np.linspace(low, high, steps)
    findings = np.full((steps, len(column_names)), np.nan)
    for row, entry in enumerate(values):
        # set the parameter
        replace_parameter(input_name, line_number, charicter_number, entry)
        # run the input
        subprocess.run([program, input_name, temp_name])
        # get the return
        if os.path.exists(temp_name):
            out = fetch_result(temp_name, block_names, parameter_numbers)
            findings[row, 1] = entry
            findings[row, 1] = out[0][0]
            findings[row, 2] = out[1][0]
    output_name = "scan"
    info = {'dtype': float,
            'part': 'scan',
            'columns': column_names}
    run.add_ds(output_name, info, findings)
            
# get the cross sections for a list of parameters in an h5 file
# save in the same h5 file
def batch_crossSections(input_name, run, batch_size=10):
    '''Add the cross sections for a batch in the file
    

    Parameters
    ----------
    input_name : str
        name of the skeleton input file 2HDMC_physicalbasis.in
        
    run : Run
        collection of datasets to save output
        
    batch_size : int
        number of items to process


    '''
    crossSec_ds_name = "crossSections"
    # read a batch
    all_input = run.datasets['validinputs']
    columns = run.infos['validinputs']['columns']
    CS_ds_exists = crossSec_ds_name in run.datasets
    # there are 3 versions gg to h and bb to h for each of h in (h,H,A)
    CS_column_format = ["LO $\\sigma(g g \\rightarrow {})$ (pb)",
                        "NLO $\\sigma(g g \\rightarrow {})$ (pb)",
                        "NNLO $\\sigma(g g \\rightarrow {})$ (pb)",
                        "LO $\\sigma(b b \\rightarrow {})$ (pb)",
                        "NLO $\\sigma(b b \\rightarrow {})$ (pb)",
                        "NNLO $\\sigma(b b \\rightarrow {})$ (pb)"]
    # this dic mapt the higgs typ to the value needed by the input file
    higgs_types = {'h': 11, 'H' : 12, 'A': 21}
    if CS_ds_exists:
        point_reached = len(run.datasets[crossSec_ds_name])
        CS_columns = run.infos[crossSec_ds_name]['columns']
    else:
        point_reached = 0
        CS_columns = []
        for symbol in higgs_types:
            CS_columns += [col.format(symbol) for col in CS_column_format]
        info = {'part': 'CrossSections',
                'dtype': float,
                'columns': CS_columns}
    total_length = len(all_input)
    if total_length == point_reached:
        last_batch = True
        return last_batch
    if total_length <= batch_size + point_reached -1:
        last_batch = True
        points = all_input[point_reached:]
    else:
        last_batch = False
        points = all_input[point_reached: point_reached+batch_size]
    # make array for this batch
    crossSecs = np.full((len(points), len(CS_columns)), np.nan)
    # set up to run script
    program = "./bin/sushi.2HDMC"
    temp_name = "./scan_tmp.out"
    block_name = "2HDMC"
    # python2 and 3 have some divergance in the way they treat strings, so acount for all possibles
    param_numbers = {b'$m_H$': 22 , b'$m_A$': 23, b'$m_C$' :24 , b'$sin(\\beta - \\alpha)$': 25,
                     '$m_H$': 22 , '$m_A$': 23, '$m_C$' :24 , '$sin(\\beta - \\alpha)$': 25,
                     b'$tan(\\beta)$': 3, b'$m_{12}^2$': 4,
                     '$tan(\\beta)$': 3, '$m_{12}^2$': 4}
    line_numbers = []; charicter_numbers = []
    for name in columns:
        line_num, char_num = parameter_position(input_name, block_name, param_numbers[name])
        line_numbers.append(line_num)
        charicter_numbers.append(char_num)
    higgs_block_name = "SUSHI"
    higgs_param_num = 2
    higgs_line_num, higgs_char_num = parameter_position(input_name, higgs_block_name, higgs_param_num)
    results_block_names = ["SUSHIggh", "SUSHIbbh", "XSGGH", "XSBBH"]
    results_parameter_numbers = [[1], [1], [1, 2], [1, 2]]
    # run script
    for row, params in enumerate(points):
        # set the parameters
        replace_parameters(input_name, line_numbers, charicter_numbers, params)
        for h_i, symbol in enumerate(higgs_types):
            # set the higgs type
            replace_parameter(input_name, higgs_line_num, higgs_char_num, higgs_types[symbol])
            # run the input
            subprocess.call([program, input_name, temp_name])
            # get the return
            if os.path.exists(temp_name):
                higgs_start = h_i * len(CS_column_format)
                out = fetch_result(temp_name, results_block_names, results_parameter_numbers)
                crossSecs[row, higgs_start + 0] = out[2][0]
                crossSecs[row, higgs_start + 1] = out[2][1]
                crossSecs[row, higgs_start + 2] = out[0][0]
                crossSecs[row, higgs_start + 3] = out[3][0]
                crossSecs[row, higgs_start + 4] = out[3][1]
                crossSecs[row, higgs_start + 5] = out[1][0]
    # if there are already cross sections append to them
    if CS_ds_exists:
        current = run.datasets[crossSec_ds_name]
        new = np.vstack((current, crossSecs))
        run.datasets[crossSec_ds_name] = new
    else:
        run.add_ds(crossSec_ds_name, info, crossSecs)
    run.write()
    return last_batch


# define a worker object, one will go in each thread
def worker(input_name, run, run_condition, batch_size):
    batch_n = 0
    if run_condition == 'continue':
        while os.path.exists(run_condition):
            print("Starting batch {}".format(batch_n))
            last_batch = batch_crossSections(input_name, run, batch_size)
            if last_batch:
                break
            batch_n += 1
    else:
        while time.time() < run_condition:
            print("Starting batch {}".format(batch_n))
            last_batch = batch_crossSections(input_name, run, batch_size)
            if last_batch:
                break
            batch_n += 1
    return last_batch, batch_n

def add_crossSections(input_name, run_dir, multi_thread=True):
    '''Add the cross sections for a batch in the file
    works while the file continue exists 
    

    Parameters
    ----------
    input_name : str
        name of the skeleton input file 2HDMC_physicalbasis.in
        
    run_dir : str
        name of the directory containing segments to be run
    '''
    run = Data.Run(run_dir)
    last_batch = False
    # decide on a stop condition
    if os.path.exists('continue'):
        run_condition = 'continue'
    else:
        if InputTools.yesNo_question("Would you like to do a time based run? "):
            run_time = InputTools.get_time("How long should it run?")
            stop_time = time.time() + run_time
            run_condition = stop_time
        elif InputTools.yesNo_question("Would you like to create a continue file?"
                                       +" (be sure you can delete it while the"
                                       +" program is running!) "):
            open('continue', 'w').close()
            run_condition = 'continue'
        else:
            return 
    profile_start_time = time.time()
    batch_size = 20
    # work out how many threads
    if multi_thread:
        n_threads = multiprocessing.cpu_count()
        print("Running on {} threads".format(n_threads))
        # work out how many data components
        segment_names = [os.path.join(run_dir, name) for name in next(os.walk(run_dir))[1]]
    else:
        segment_names = [run_dir]
    # each componetn needs it's pwn input file to avoid race conditions
    # check that mh = 125
    block_name = "2HDMC"
    param_number = 21
    line_number, charicter_number = parameter_position(input_name, block_name, param_number)
    replace_parameter(input_name, line_number, charicter_number, 125.)
    with open(input_name, 'r') as input_file:
        input_lines = input_file.readlines()
    input_names = [os.path.join(run_dir, "tmp{}.in".format(i))
                   for i, _ in enumerate(segment_names)]
    for name in input_names:
        with open(name, 'w') as tmp_file:
            tmp_file.writelines(input_lines)
    # make a run for each thread
    runs = [Data.Run(name) for name in segment_names]
    if multi_thread:
        pool = multiprocessing.Pool(n_threads)
        # now each segment makes a worker
        args = []
        for name, run in zip(input_names, runs):
            args.append((name, run, run_condition, batch_size))
        results = [pool.apply(worker, args=a) for a in args]
        pool.close()
        finished, num_batches = zip(*results)
        batches_finished = sum(num_batches)
    else:
        finished, batches_finished = worker(input_names[0], runs[0], run_condition, batch_size)
        finished = [finished]
    profile_run_time = time.time() - profile_start_time
    items_ran = batch_size * batches_finished
    profile_message = "Ran {} items in {} seconds\n".format(items_ran, profile_run_time)
    if items_ran != 0:
        profile_message += "{} seconds per item".format(profile_run_time/items_ran)
    print(profile_message)
    with open("speed_PipeSusHi.txt", 'w') as f:
        f.write(profile_message)
    if np.all(finished):
        print("Finished last batch.")
    else:
        print("stopped before last batch")
        incomplete = np.where(~np.array(finished, dtype=bool))[0]
        print("Incomplete sections; ")
        print(segment_names[incomplete])


def main():
    """ add cross sections """
    run_dir = InputTools.get_dir_name("Name the data file (existing): ")
    if run_dir == '':
        run_dir = "/home/henry/Programs/docker/2HDM_live/test_sushi/dayrun1/"
    if not os.path.exists(run_dir):
        print("Error, this directory dosn't exist")
        return
    input_name = InputTools.get_file_name("Give the SusHi input file to start from; ", '.in')
    if input_name == '':
        input_name = "example/2HDMC_physicalbasis.in"
    add_crossSections(input_name, run_dir)


if __name__ == '__main__':
    main()

