""" Module to run the altered version of SusHi that
communicated via the stdin/stdout (the pipe) instead of reading and writing to disk.
"""
import subprocess
import time
import os
import InputTools
#from ipdb import set_trace as st
SUSHI_ROOT = '../PipeSusHi/'

# sushi thinks it needs an input file, and its easier to give it a
# junk file than fix that
def setup():
    ''' Makes a junk file, because SusHi looks for
    an input file even though it dosn't read one anymore
    (and I'm too lazy to disable that)'''
    legacy_junk_name = "legacy_junk.txt"
    sushi_name = os.path.join(SUSHI_ROOT, "bin/sushi")
    if not os.path.exists(legacy_junk_name):
        try:
            open(legacy_junk_name, 'w').close()
        except IOError:
            message = "Couldn't create legacy_junk.txt, name alternative location; (e.g. /some/path/legacy_junk.txt) "
            legacy_junk_name = InputTools.get_dir_name(message)
            open(legacy_junk_name, 'w').close()
            # also assume the sushi bin may be somewhre awkward
            message = "name the location of the sushi program; (e.g. /some/path/bin/sushi) "
            sushi_name = InputTools.get_dir_name(message)
    locations = [sushi_name, legacy_junk_name]
    return locations


def get_skeleton(real_input_name = "../PipeSushi/example/2HDMC_physicalbasis.in"):
    '''
    Read in a file and return the lines as byte arrays
    Intended to be used to read in a SusHi input file
    (that is then held in the ram for repeated use)
    to feed to SusHi via stdin.
    

    Parameters
    ----------
    real_input_name : str
         (Default value = "example/2HDMC_physicalbasis.in")
         path to the input file

    Returns
    -------
    lines: list of byte array
         contents of the file as byte arrays

    '''
    with open(real_input_name, 'rb') as inputf:
        lines = inputf.readlines()
    return lines


def xrun_SusHi(input_lines, locations, tries=0):
    '''
    Run SusHi using the provided input lines in place of an input file
    

    Parameters
    ----------
    input_lines: list of byte array
         contents of the file as byte arrays

    Returns
    -------
    output_lines: list of byte array
         the data that SusHi would write to disk as an output file
         (minus some inessential parts)

    '''
    process = subprocess.Popen([locations[0], locations[1], locations[1][:-4] + '_out.txt'],
                               stdout=subprocess.PIPE,
                               stdin=subprocess.PIPE)
    i = 0
    while process.poll() is None:
        output_lines = []
        process_output = process.stdout.readline()
        #print(process_output)
        if process_output[:2] == b' *': # all system prompts start with *
            # note that SusHi reads the input file several times
            if b'**send input file to stdin' in process_output:
                i += 1
                #print("started")
                for line in input_lines:
                    process.stdin.write(line)
                    process.stdin.flush()
                process.stdin.write(b'END\n')
                process.stdin.flush()
                #print("sent message {}".format(i))
            elif b'**output file starts here' in process_output:
                output_lines += process.stdout.readlines()
            elif i > 17:  # we only expect to send 17 messages
                # let the program run undisturbed for a bit
                #print("sleeping")
                time.sleep(1)
    # add any futher output
    output_lines += process.stdout.readlines()
    if output_lines is []:  # we really didn't get anything
        print("Error! No output, retrying that input")
        tries += 1
        if tries > 5:
            print("Tried this 5 times... already")
        # recursive call
        output_lines = run_SusHi(input_lines, locations, tries)
    return output_lines[:-1]


def run_SusHi(input_lines, locations, tries=0):
    '''
    Run SusHi using the provided input lines in place of an input file
    

    Parameters
    ----------
    input_lines: list of byte array
         contents of the file as byte arrays

    Returns
    -------
    output_lines: list of byte array
         the data that SusHi would write to disk as an output file
         (minus some inessential parts)

    '''
    process = subprocess.Popen([locations[0], locations[1], locations[1][:-4] + '_out.txt'],
                               stdout=subprocess.PIPE,
                               stdin=subprocess.PIPE)
    i = 0
    while process.poll() is None:
        output_lines = None
        process_output = process.stdout.readline()
        #print(process_output)
        if process_output[:2] == b' *': # all system prompts start with *
            # note that SusHi reads the input file several times
            if b'**send input file to stdin' in process_output:
                i += 1
                #print("started")
                for line in input_lines:
                    process.stdin.write(line)
                    process.stdin.flush()
                process.stdin.write(b'END\n')
                process.stdin.flush()
                #print("sent message {}".format(i))
            elif b'**output file starts here' in process_output:
                #output_lines += process.stdout.readlines()
                output_lines, errors = process.communicate()
    # add any futher output
    #output_lines += process.stdout.readlines()
    #process.stdout.flush()
    if output_lines is None or len(output_lines) == 0:  # we really didn't get anything
        print("Error! No output, retrying that input")
        tries += 1
        if tries > 5:
            print("Tried this 5 times... already")
        # recursive call
        output_lines = run_SusHi(input_lines, locations, tries)
    return output_lines[:-1]

if __name__ == '__main__':
    locations = setup()
    lines = get_skeleton()
    output = run_SusHi(lines, locations)
    print(output)

