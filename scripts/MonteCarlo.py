""" Functions fo generating points, checking their vaildity and calculating their brancing ratios
required compiled Bounds.cpp and FromFile.cpp"""
import subprocess
import sys
import fcntl
import os
import time
#from ipdb import set_trace as st
import numpy as np
import InputTools
import scipy.interpolate
import Data

THDMC_dir_name = '../2HDMC-1.7.0'
PATH_TO_FLAVOUR_CSVS = '../flavour_constraints'
AtoZH='A->ZH'
HtoZA='H->ZA'

# python2/3 compatability
try: input = raw_input
except NameError: pass

def define_conditions(column_names, conditions={}):
    """
    Create or check a set of conditions for a run

    Parameters
    ----------
    conditions : dict
         (Default value = {})
         If checking conditions or adding to existing conditions 
         this should be a dictionary of conditions

    Returns
    -------
    conditions : dict
        generated and checked condions

    """
    process = InputTools.list_complete("Which process? ", [AtoZH, HtoZA]).strip()
    print("process = {}".format(process))
    yukawa_map = {'TypeI': 1, 'TypeII': 2,
                  'TypeIII': 3, 'TypeIV': 4}
    if 'yukawa' not in conditions:
        yukawa = InputTools.list_complete("Chose yukawa type; ", yukawa_map.keys())
        yukawa = yukawa_map.get(yukawa, int(yukawa))
        conditions['yukawa'] = yukawa
    print("yukawa type = {}".format(conditions['yukawa']))
    remove_chars = '\\$_(){}^ -'
    column_names_stripped = [''.join(c for c in name if c not in remove_chars)
                             for name in column_names]
    assert column_names_stripped[-1] == 'm122', "m122 should be the last column because it will be appeneded later based on more complex formalism"
    local_order = ['mH', 'mA', 'mC', 'sba', 'tb']
    column_names_stripped = column_names_stripped[:-1]
    min_dict = {'mH':125., 'mA': 125., 'mC':125., 'sinbetaalpha': 0.8, 'tanbeta': 0.}
    max_dict = {'mH':1000., 'mA': 1000., 'mC':1000., 'sinbetaalpha': 1., 'tanbeta': 25.}
    defaults = {'mins': np.vectorize(min_dict.get)(column_names_stripped),
                'maxes': np.vectorize(max_dict.get)(column_names_stripped),
                'fixed': np.full(len(column_names_stripped), np.nan)}
    for name, default in defaults.items():
        if name not in conditions:
            conditions[name] = InputTools.select_values(name, column_names_stripped, default)
    if process == AtoZH:
        n_min_mAmH, min_mHmA = 'min(mA-mH)', 100.
        if InputTools.yesNo_question("Restrict {}? ".format(n_min_mAmH)):
            conditions[n_min_mAmH] = InputTools.select_value(n_min_mAmH, min_mHmA)
        else:
            conditions[n_min_mAmH] = False
    if process == HtoZA:
        n_min_mHmA, min_mAmH = 'min(mH-mA)', 100.
        if InputTools.yesNo_question("Restrict {}? ".format(n_min_mHmA)):
            conditions[n_min_mHmA] = InputTools.select_value(n_min_mHmA, min_mAmH)
        else:
            conditions[n_min_mHmA] = False
    if InputTools.yesNo_question("Add a minimum m122 ceiling? "):
        conditions["min_m122_ceil"] = InputTools.select_value("min_m122_ceil", 5000)
    if InputTools.yesNo_question("Add a m122 ceiling multiplier? "):
        conditions["m122_ceil_multi"] = InputTools.select_value("m122_ceil_multi", 3)
    if 'linked' not in conditions:
        linked = []
        if InputTools.yesNo_question("Has linked parameters? (y/n) "):
            print("Enter the first two indices the be linked: ")
            print("Format is {}".format(column_names_stripped))
            while True:
                linked_inp = input("linked = ")
                split_inp = linked_inp.replace(',', ' ').split()
                linked.append(tuple(int(i) for i in split_inp[:2]))
                if not InputTools.yesNo_question("Link another pair? "):
                    break
        conditions['linked'] = linked
    print("linked = {}".format(linked))
    return conditions


def define_restricted(column_names, conditions={}):
    """
    Create or check a set of conditions for a run

    Parameters
    ----------
    conditions : dict
         (Default value = {})
         If checking conditions or adding to existing conditions 
         this should be a dictionary of conditions

    Returns
    -------
    conditions : dict
        generated and checked condions

    """
    process = InputTools.list_complete("Which process? ", [AtoZH, HtoZA]).strip()
    print("process = {}".format(process))
    yukawa_map = {'TypeI': 1, 'TypeII': 2,
                  'TypeIII': 3, 'TypeIV': 4}
    if 'yukawa' not in conditions:
        yukawa = InputTools.list_complete("Chose yukawa type; ", yukawa_map.keys())
        yukawa = yukawa_map.get(yukawa, int(yukawa))
        conditions['yukawa'] = yukawa
    print("yukawa type = {}".format(conditions['yukawa']))
    remove_chars = '\\$_(){}^ -'
    column_names_stripped = [''.join(c for c in name if c not in remove_chars)
                             for name in column_names]
    assert column_names_stripped[-1] == 'm122', "m122 should be the last column because it will be appeneded later based on more complex formalism"
    local_order = ['mH', 'mA', 'mC', 'sba', 'tb']
    column_names_stripped = column_names_stripped[:-1]
    min_dict = {'mH':125., 'mA': 125., 'mC':125., 'sinbetaalpha': 0.8, 'tanbeta': 0.}
    max_dict = {'mH':1000., 'mA': 1000., 'mC':1000., 'sinbetaalpha': 1., 'tanbeta': 25.}
    conditions['mins']  = np.vectorize(min_dict.get)(column_names_stripped)
    conditions['maxes'] = np.vectorize(max_dict.get)(column_names_stripped)
    tanbeta = InputTools.get_literal("Chose a value of tanbeta; ", float)
    conditions['fixed'] = np.array([np.nan, np.nan, np.nan, 1., tanbeta])
    if process == AtoZH:
        n_min_mAmH, min_mHmA = 'min(mA-mH)', 100.
        conditions[n_min_mAmH] = min_mHmA
    if process == HtoZA:
        n_min_mHmA, min_mAmH = 'min(mH-mA)', 100.
        conditions[n_min_mHmA] = min_mAmH
    if InputTools.yesNo_question("Add a minimum m122 ceiling? "):
        conditions["min_m122_ceil"] = InputTools.select_value("min_m122_ceil", 5000)
    if InputTools.yesNo_question("Add a m122 ceiling multiplier? "):
        conditions["m122_ceil_multi"] = InputTools.select_value("m122_ceil_multi", 3)
    if 'linked' not in conditions:
        linked = [(0,2)]
        conditions['linked'] = linked
    print("linked = {}".format(linked))
    return conditions


def generate_points(conditions, n_points):
    """ Run the monty carlo generation of points.

    Parameters
    ----------
    conditions : dictionary
       the conditions defining the placement of points

    n_points : int
       number of poitns to be generated
    Returns
    -------
    inputs : numpy array of floats
        the random positions

    """
    mins = np.array(conditions['mins'])
    maxes = np.array(conditions['maxes'])
    fixed = np.array(conditions['fixed'])
    linked = np.array(conditions['linked'])
    num_dimensions = len(mins)
    assert num_dimensions == 5
    assert len(maxes) == num_dimensions
    assert len(fixed) == num_dimensions
    # now generate
    ranges = maxes-mins
    varies = np.isnan(fixed)
    for pair in linked:
        # consider the second of each pair not to vary
        varies[pair[1]] = False
    # now generate points
    inputs = np.full((n_points, num_dimensions), fixed, dtype=float)  # the unfixed columns will be nan
    for i, var in enumerate(varies):
        if var:
            random_vals = np.random.random(n_points)
            rescaled_random = random_vals * ranges[i] + mins[i]
            inputs[:, i] = rescaled_random
    for pair in linked:
        inputs[:, pair[1]] = inputs[:, pair[0]]
    return inputs


def gen_m12_2(mass, tb, random=True, min_ceil=0, ceil_multiplier=1.):
    max_m12_2 = np.clip(mass**2*tb/(1+tb**2), min_ceil, None)
    if random:
        min_m12_2 = np.zeros_like(mass)
        m12_2 = min_m12_2 + np.random.random(len(mass))*ceil_multiplier*max_m12_2
    else:
        m12_2 = max_m12_2
    return m12_2


class Bounds:
    _input_request = b" **send input to stdin"
    _expected_columns = ["$m_H$", "$m_A$", "$m_C$", "$sin(\\beta - \\alpha)$", "$tan(\\beta)$",
                         "$m_{12}^2$"]
    def __init__(self, info, verbose=False):
        yukawa = info['yukawa']
        for found, expected in zip(info['columns'], self._expected_columns):
            assert found == expected, "Expected ({}) column order differs from order in data; {}".format(self._expected_columns, info['columns'])
        self._yukawa_str = ' {yukawa} '.encode()  # needs a space on the end
        self._need_str = b'HiggsBounds '
        self._leave_str = b'No '
        if verbose:
            self._program = os.path.join(THDMC_dir_name, "Bounds_verbose")
        else:
            self._program = os.path.join(THDMC_dir_name, "Bounds")
        self.n_outs = 5
        self.n_outs += verbose*7
        self.out_type = float if verbose else int
        self.higgsbounds = {}
        self._start_process()
        
    def _start_process(self):
        self._process = subprocess.Popen([self._program], stdin=subprocess.PIPE, stdout=subprocess.PIPE)

    def check(self, points, recurse_count=0, require_bounds=True):
        if require_bounds:
            bounds = self._need_str
        else:
            bounds = self._leave_str
        while self._process.poll() is None:
            output = self._process.stdout.readline()
            if output.startswith(self._input_request):
                line = bounds + ' '.join(points).encode() + self._yukawa_str
                self._process.stdin.write(line)
                self._process.stdin.flush()
            elif len(output):
                try:
                    output = [self.out_type(c) for c in output.split()]
                except ValueError:  # try again
                    if recurse_count > 10:
                        message = "Problem checking {}, output is {}".format(points, output)
                        raise RuntimeError(message)  # TODO consider changing this to returning fails?
                    return self.check(points, recurse_count+1)
                return np.array(output)
        else:  # if the process stops polling and we didn't return or break
            try:
                self.terminate()
            except Exception:
                # accept anything, it's dificult to know why this didn't work
                # but I really don't want to stop here
                pass
            self._start_process()
            return self.check(points=points, recurse_count=recurse_count,
                              require_bounds=require_bounds)

    def check_many(self, points):
        bounds = self._need_str
        n_checks = len(points)
        n_complete = 0
        points = points.astype(str)
        results = np.full((n_checks, self.n_outs), True, dtype=self.out_type)
        while self._process.poll() is None:
            output = self._process.stdout.readline()
            if output.startswith(self._input_request):
                line = bounds + ' '.join(points[n_complete]).encode() + self._yukawa_str
                self._process.stdin.write(line)
                self._process.stdin.flush()
            elif len(output):
                try:
                    results[n_complete] = [self.out_type(c) for c in output.split()]
                except ValueError: # try this line again
                    results[n_complete] = self.check(points)
                n_complete += 1
                if n_complete == n_checks:
                    return results
        else:  # if the process stops polling and we didn't return or break
            try:
                self.terminate()
            except Exception:
                # accept anything, it's dificult to know why this didn't work
                # but I really don't want to stop here
                pass
            self._start_process()
            return self.check_many(points=points)

    def terminate(self):
        try:
            self._process.terminate()
        except AttributeError:
            pass

    def __del__(self):
        self.terminate()


def check_bounds(points, info):
    """ Checks given points for validity
    

    Parameters
    ----------
    points : array of floats
        points to check order mH, mA, sba, tb
        
    info : dict
        the info dictionary for the points, inclusding the columns and the yukawa type
        
    Returns
    -------
    checks: numpy array of bools
        validity checks for each point

    """
    bounds = Bounds(info)
    checks = bounds.check_many(points)
    bounds.terminate()
    flavour = check_flavour_constraints(points, info)
    checks = np.hstack((checks, flavour.reshape((-1, 1))))
    return checks


flavour_interpolations = []
def make_flavour_interpolators(path_to_csvs=PATH_TO_FLAVOUR_CSVS):
    name_structure = 'Scan2D_Type{}_tanBeta_vs_MHc_Combined{}.csv'
    for yukawa in range(1, 5):
        if yukawa == 1:
            file_names = [name_structure.format('I', '')]
        elif yukawa == 2:
            file_names = [name_structure.format('II', '_pt1'),
                          name_structure.format('II', '_pt2')]
        elif yukawa == 3:
            file_names = [name_structure.format('Flipped', '_pt1'),
                          name_structure.format('Flipped', '_pt2')]
        elif yukawa == 4:
            file_names = [name_structure.format('LS', '')]
        interpolators = []
        for name in file_names:
            name = os.path.join(path_to_csvs, name)
            mC, tanbeta = np.genfromtxt(name, dtype=float, skip_header=1, delimiter=', ').T
            interpolater = scipy.interpolate.interp1d(tanbeta, mC, fill_value='extrapolate')
            interpolators.append(interpolater)
        flavour_interpolations.append(interpolators)


def check_flavour_constraints(points, info, path_to_csvs=PATH_TO_FLAVOUR_CSVS):
    columns = list(info['columns'])
    tanbeta_col = columns.index("$tan(\\beta)$")
    mC_col = columns.index("$m_C$")
    if not flavour_interpolations:  # create them
        make_flavour_interpolators(path_to_csvs)
    yukawa = int(info['yukawa'])
    tanbetas = points[:, tanbeta_col]
    possibles = np.vstack([inter(tanbetas) for inter in flavour_interpolations[yukawa-1]])
    min_mC = np.max(possibles, axis=0)
    return points[:, mC_col] > min_mC


def add_flavour_constraints(run):
    yukawa = run.infos['inputs']['yukawa']
    if 'inputs' in run.infos:
        mA = run.inputs.mA
        tanbeta = run.inputs.tanbeta
        all_points = np.vstack((tanbeta, mA)).T
        all_flavour = check_flavour_constraints(all_points, run.infos['inputs'])
        run.infos['checks']['columns'].append("flavour constraints")
        new_checks = np.hstack((run.datasets['checks'], all_flavour.reshape((-1, 1))))
        run.datasets['checks'] = new_checks
    mA = run.uniqueinputs.mA
    tanbeta = run.uniqueinputs.tanbeta
    mask_points = np.vstack((tanbeta, mA)).T
    mask_flavour = check_flavour_constraints(mask_points, run.infos['inputs'])
    for name in run.datasets:
        if name in run.diferent_length:
            continue
        run.datasets[name] = run.datasets[name][mask_flavour]
    run.write()


def generate_brs_lambdas(unique_inputs_filename, inputs_info, dir_name, new=True):
    """
    Calculates branching ratios

    Parameters
    ----------
    unique_inputs_filename : str
       path to a file containing points that we wish to calculate brs for
       should have the format generated by using generate_points function
       to create a dataset and then saving the dataset in a Run object
        
    inputs_info : dict
       info dict for the inputs, as found in a Run object
        
    dir_name : str
       path to output the brs

    """
    program = os.path.join(THDMC_dir_name, "FromFile")
    yukawa = str(inputs_info['yukawa'])
    br_out_prefix = os.path.join(dir_name, "brs")
    needs_headers = 'headers' if new else 'no'
    lambda_out_prefix = os.path.join(dir_name, "lambda")
    subprocess.call([program, yukawa, unique_inputs_filename, br_out_prefix, needs_headers, lambda_out_prefix])
    info_names = [("brs_h" + h) for h in "hHA"]
    info_names.append("lambda")
    infos = {}
    for name in info_names:
        infos[name] = {'dtype': float.__name__,
                       'part': 'br'}
    return infos


def main(use_time = True):
    """ Run the monty carlo generation of points."""
    if os.path.exists('continue') and not use_time:
        run_condition = lambda: os.path.exists('continue')
    else:
        if InputTools.yesNo_question("Would you like to do a time based run? "):
            run_time = InputTools.get_time("How long should it run?")
            stop_time = time.time() + run_time
            run_condition = lambda: time.time() < stop_time
        elif InputTools.yesNo_question("Would you like to create a continue file?"
                                       +" (be sure you can delete it while the"
                                       +" program is running!) "):
            open('continue', 'w').close()
            run_condition = lambda: os.path.exists('continue')
        else:
            return 
    dir_name = InputTools.get_dir_name("Name the save directory: ")
    run = Data.Run(dir_name)
    if 'inputs' in run.infos:
        inputs_info = run.infos['inputs']
        conditions = inputs_info
        input_columns = inputs_info['columns']
    else:
        input_columns = ["$m_H$", "$m_A$", "$m_C$", "$sin(\\beta - \\alpha)$",
                          "$tan(\\beta)$", "$m_{12}^2$"]
        conditions = define_conditions(input_columns)
        inputs_info = conditions
        inputs_info['columns'] = input_columns
        inputs_info['dtype'] = float
        inputs_info['part'] = 'input'
    col_mH = input_columns.index('$m_H$')
    col_mA = input_columns.index('$m_A$')
    col_tb = input_columns.index('$tan(\\beta)$')
    batch_size = 500
    while run_condition():
        print('.', end='', flush=True)
        # chose the inputs
        inputs = generate_points(inputs_info, batch_size)
        if 'min(mA-mH)' in conditions:
            mass_col = col_mA
        else:
            mass_col = col_mH
        min_ceil = inputs_info.get("min_m122_ceil", 0.)
        ceil_multi = inputs_info.get("m122_ceil_multi", 1.)
        inputs = np.hstack((inputs, gen_m12_2(inputs[:, mass_col], inputs[:, col_tb], random=True, min_ceil=min_ceil, ceil_multiplier=ceil_multi).reshape((-1, 1))))
        # remove anything that dosn't satify the min(mA-mH)
        minmAmH = inputs_info.get('min(mA-mH)', False)
        if minmAmH:
            mask = inputs[:, col_mA] -  inputs[:, col_mH] > minmAmH
            inputs = inputs[mask]
        # remove anything that dosn't satify the min(mH-mA)
        minmHmA = inputs_info.get('min(mH-mA)', False)
        if minmHmA:
            mask = inputs[:, col_mH] -  inputs[:, col_mA] > minmHmA
            inputs = inputs[mask]
        #check validity
        try:
            checks = check_bounds(inputs, inputs_info)
        except ValueError:
            continue
        checks_info = {'dtype': bool,
                       'part': 'checks',
                       'columns': ["parameters are valid",
                                   "check stability",
                                   "check unitarity",
                                   "check perturbativity",
                                   "HiggsBounds",
                                   "flavour constraints"]}
        if len(checks) != len(inputs):
            print("Something not right...")
            st()
        run.add_ds('inputs', inputs_info, inputs, mode='append')
        run.add_ds('checks', checks_info, checks, mode='append')
        run.write()
    print("Done. Now run SusHiScript.py to add the cross Sections.")

if __name__ == '__main__':
    main()

