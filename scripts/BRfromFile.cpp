/*******************************************************************************
  Adaptation of the Demo program to generate only Branching ratios.

  Put me in 2HDMC/src and then do
  make BRonly

  The python script AddBRs.py requires me.
 *******************************************************************************/
#include "THDM.h"
#include "SM.h"
#include "HBHS.h"
#include "Constraints.h"
#include "DecayTable.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <stdio.h>
#include <string.h>
#include <string>
#include <iomanip>
#include <vector>
#include <math.h>

using namespace std;

// to replace somthing in a string
std::string ReplaceAll(std::string str, const std::string& from, const std::string& to) {
    size_t start_pos = 0;
    while((start_pos = str.find(from, start_pos)) != std::string::npos) {
        str.replace(start_pos, from.length(), to);
        start_pos += to.length(); // Handles case where 'to' is a substring of 'from'
    }
    return str;
}

double br(double dG, double G) {

  double BR = 0.;

  if (G>0.) {
    BR = dG/G;
    //if (BR<THDM::EPS) BR = 0.;
  }
  
  return BR;
}


void column_heading(std::ostream &ss, char h_type, string sep, string name1, string name2=""){
    ss << "$BR(" << h_type << "\\rightarrow " << name1 << " " << name2 << ")$";
    ss << sep;
}

void column_headings(std::ostream &ss, int &n_columns, string sep, char h_type){
    //comment for numpy
    ss << "#" << sep;
    std::string names[8][6]; // 8 types, up to 5 generations
    // fill in with default
    for(int i = 0; i < 8; i++){
        for(int j = 0; j < 5; j++){
            names[i][j] = "none";
        }
    }
    // now will in the defined names
    // type 0 is d = down family quark; down strange bottom
    names[0][1] = "d";
    names[0][2] = "s";
    names[0][3] = "b";
    // type 1 is u = up family quark; up charm top
    names[1][1] = "u";
    names[1][2] = "c";
    names[1][3] = "t";
    // type 2 is l = lepton; electron muon tau
    names[2][1] = "e";
    names[2][2] = "\\mu";
    names[2][3] = "\\tau";
    // type 3 is n = neutrino; nu_e nu_m nu_t
    names[3][1] = "\\nu_e";
    names[3][2] = "\\nu_\\mu";
    names[3][3] = "\\nu_\\tau";
    // type 4 is h = higgs; h H A +H
    names[4][1] = "h";
    names[4][2] = "H";
    names[4][3] = "A";
    names[4][4] = "H+";
    // type 5 is v = vector boson; photon Z W
    names[5][1] = "\\gamma";
    names[5][2] = "Z";
    names[5][3] = "W";
    // type 6 is ga = pair of photons
    names[6][1] = "\\gamma";
    // type 7 is gg = pair of gluons; gluonPair
    names[7][1] = "gg";

    n_columns = 0;
    ss << "All" << sep;
    n_columns++;
    // Fermion decay modes
    for (int i=1;i<4;i++) {
        for (int j=1;j<4;j++) {
            // the gamma are the branching fraction for x to yz
            // get_gamma_xyz
            // the inputs are for the type of higgs
            // and the generation of the quark/lepton
            column_heading(ss, h_type, sep, names[0][i], names[0][j]);
            column_heading(ss, h_type, sep, names[1][i], names[1][j]);
            column_heading(ss, h_type, sep, names[0][i], names[1][j]);
            column_heading(ss, h_type, sep, names[2][i], names[2][j]);
            column_heading(ss, h_type, sep, names[3][i], names[2][j]);
            n_columns += 5;
        }
    }

    // Vector bosons
    for (int i=1;i<4;i++) {
        column_heading(ss, h_type, sep, names[5][i], names[5][i]);
        n_columns++;
        for (int j=1;j<5;j++) {
            column_heading(ss, h_type, sep, names[5][i], names[4][j]);
            n_columns++;
        }
    }

    // Z gamma
    column_heading(ss, h_type, sep, names[5][2], names[5][1]);
    n_columns++;

    // Gluons
    column_heading(ss, h_type, sep, names[7][1]);
    n_columns++;

    // to higgs
    for (int i=1;i<=4;i++) {
        column_heading(ss, h_type, sep, names[4][i]);
        n_columns++;
    }
    ss << endl;
}

void br_row(std::vector<double> &row, DecayTable &table, int h) {
    int n = 0; // column number
    // there are only higgs 1 to 4
    // we are intrested in higgs 2 and 3
    if((h<1) || (h>3)){
        printf("Error, h should be 1 (for light neutral higgs h), 2 (for heavist neutral higgs H) or 3 (for cp odd higgs A)\n");
        return;
    }
    double gtot = table.get_gammatot_h(h);
    row[n++] = gtot;

    // see colum names function
    double gdd[4][4];
    double guu[4][4];
    double gdu[4][4];
    double gll[4][4];
    double gln[4][4];
    double gvv[4];
    double gvh[4][5];
    double ghh[5];
    double ghZga;
    double ghgg;

    // Fermion decay modes
    for (int i=1;i<4;i++) {
        for (int j=1;j<4;j++) {
            // the gamma are the branching fraction for x to yz
            // get_gamma_xyz
            // the inputs are for the type of higgs
            // and the generation of the quark/lepton
            gdd[i][j]=table.get_gamma_hdd(h,i,j);
            guu[i][j]=table.get_gamma_huu(h,i,j);
            gdu[i][j]=table.get_gamma_hdu(h,i,j);
            gll[i][j]=table.get_gamma_hll(h,i,j);
            gln[i][j]=table.get_gamma_hln(h,i,j);
            row[n++] = br(gdd[i][j],gtot);
            row[n++] = br(guu[i][j],gtot);
            row[n++] = br(gdu[i][j],gtot);
            row[n++] = br(gll[i][j],gtot);
            row[n++] = br(gln[i][j],gtot);
        }
    }

    // Vector bosons
    for (int i=1;i<4;i++) {
        gvv[i]=table.get_gamma_hvv(h,i);
        row[n++] = br(gvv[i],gtot);
        for (int j=1;j<5;j++) {
            gvh[i][j]=table.get_gamma_hvh(h,i,j);
            //if((h==2) && (i==1) && (j==1)){
            //    std::cout << gvh[i][j] << endl;
            //}
            row[n++] = br(gvh[i][j],gtot);
        }
    }

    // Z gamma
    ghZga = table.get_gamma_hZga(h);
    row[n++] = br(ghZga,gtot);

    // Gluons
    ghgg = table.get_gamma_hgg(h);
    row[n++] = br(ghgg,gtot);

    for (int i=1;i<=4;i++) {
        ghh[i]=table.get_gamma_hhh(h,i,i);
        row[n++] = br(ghh[i],gtot);
    }
}


int main(int argc, char* argv[]) {
    int yt_in;  //yukawa coupling type
    // read in the model type from the commandline if given
    std::istringstream iss (argv[1]);
    iss >> yt_in;
	// get the file with the changing parameters to be used
	std::ifstream inFile(argv[2]);
	std::string outPrefix(argv[3]);
    bool verbose = false;
    // Reference SM Higgs mass for EW precision observables
    double mh_ref = 125.;

    // Create SM and set parameters
    SM sm;
    sm.set_qmass_pole(6, 172.5);		
    sm.set_qmass_pole(5, 4.75);		
    sm.set_qmass_pole(4, 1.42);	
    sm.set_lmass_pole(3, 1.77684);	
    sm.set_alpha(1./127.934);
    sm.set_alpha0(1./137.0359997);
    sm.set_alpha_s(0.119);
    sm.set_MZ(91.15349);
    sm.set_MW(80.36951);
    sm.set_gamma_Z(2.49581);
    sm.set_gamma_W(2.08856);
    sm.set_GF(1.16637E-5);

    // Create 2HDM and set SM parameters
    THDM model;
    model.set_SM(sm);

    // Variables for parameters of the 2HDM in the 'physical' basis
    // Some parameters stay the same
    double mh       = 125.;
    double mH ; // 100 1000
    double mA ;  //100 1000
    double mC ; //100 1000
    double sba ;  //0.9 1
    double lambda_6 = 0.;  // 
    double lambda_7 = 0.;
    double tb ;  //0 25
    double m12_2 ;

    std::string sep = "|";
    std::string hlist = "hHA";
    std::string line;
    for(int h = 1; h < 4; h++){ // loop over higgs, h, H and A
        // make a file name
        std::stringstream nameOut;
        nameOut << outPrefix << "_h" << hlist[h-1] << "_Type" << yt_in << ".csv";
        std::ofstream outFile(nameOut.str().c_str());
        int n_columns = 0;
        column_headings(outFile, n_columns, sep, hlist[h-1]);
        std::vector<double> row(n_columns);
        // return to the start of the file
        inFile.clear();
        inFile.seekg(0, ios::beg);
        //check the header line
        std::getline(inFile, line);
        /* headr check dosn't work.... just be trusting
        std::string expected_header("$m_H$ $m_A$ $m_C$ $sin(\\beta - \\alpha)$ $tan(\\beta)$");
        if(strcmp(line.c_str(), expected_header.c_str()) != 0){
            std::cout << "Header is Wrong!!!!\n";
            std::cout << "Expected; " << expected_header << endl;
            std:: cout << "Found; " << line << endl;
            return 1;
        }
        */
        int input_line = 0;
		while (std::getline(inFile, line))
		{
            input_line += 1;
            if(input_line % 10000 == 0){
                std::cout << "Reached line " << input_line << endl;
            }
            // if the have been using | as a delimiter, switch to spaces
            line = ReplaceAll(line, "|", " ");

			std::istringstream iss(line);
			if (!(iss >> mH
                      >> mA
                      >> mC
                      >> sba
                      >> tb
                      >> m12_2)) { 
                std::cout << "Some Error in file format" << endl;
                break; 
            }

            bool pset = model.set_param_phys(mh,mH,mA,mC,sba,lambda_6,lambda_7,m12_2,tb);

            // Set Yukawa couplings to model type
            model.set_yukawas_type(yt_in);

            // Prepare to calculate decay widths
            DecayTable table(model);
            br_row(row, table, h);
            
            for(int i=0; i<row.size()-1; ++i){
              outFile << std::setprecision(14) << std::fixed << row[i] << sep;
            }
            // last item needs no sep
            outFile << std::setprecision(14) << row[row.size()-1] << endl;
            
        }
        outFile.close();
    }
    return 0;
}

