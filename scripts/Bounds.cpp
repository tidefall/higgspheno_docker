/*******************************************************************************
  Adaptation of the Demo program to determine bounds

  Put me in 2HDMC/src and then do
  make Bounds

  The python script MC_Bounds.py requires me.
 *******************************************************************************/
#include "THDM.h"
#include "SM.h"
#include "HBHS.h"
#include "Constraints.h"
#include "DecayTable.h"
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <math.h>

using namespace std;
int main(int argc, char* argv[]) {

    // Create SM and set parameters
    SM sm;

    // Create 2HDM and set SM parameters
    THDM model;
    model.set_SM(sm);

    // Set parameters of the 2HDM in the 'physical' basis
    // Some parameters stay the same
    double mh       = 125.;
    double lambda_6 = 0.;  // 
    double lambda_7 = 0.;

    // will be filled in the while loop
    double mH, mA, mC, sba, tb, m12_2;
    int yt_in;

    // will contain output
    int hbres[6];
    double hbobs[6];
    int hbchan[6];
    int hbcomb[6]; 
    bool pset;
    
    // don't alwasy need to check bounds
    bool higgsbounds;

    std::string input;
    string sep = " ";

    while(true){ // the program can eb killed by sending 'END', or just way for pyhton to kill it
        // request input from python
        std::cout << " **send input to stdin\n";

        // Set parameters of the 2HDM in the 'physical' basis
        // These parameters vary
        std::cin >> input;
        // a line with the word END means we are done
        if(input.find("END") != std::string::npos){
            exit(0);
        }
        // if the first argument contains the word higgsbounds, we want to calculate that
        higgsbounds = input.find("HiggsBounds") != std::string::npos;

        std::cin >> mH; // 100 1000
        std::cin >> mA; //100 1000
        std::cin >> mC; //100 1000
        std::cin >> sba; //0.9 1
        std::cin >> tb; //0 25
        std::cin >> m12_2; //mA*mA*sin(atan(tb))*cos(atan(tb));
        std::cin >> yt_in; //Type

        pset = model.set_param_phys(mh,mH,mA,mC,sba,lambda_6,lambda_7,m12_2,tb);
        // Set Yukawa couplings to model type
        model.set_yukawas_type(yt_in);
        Constraints constr(model);
        // Prepare to calculate observables

        //double S,T,U,V,W,X;   
        //constr.oblique_param(mh_ref,S,T,U,V,W,X);

        bool stability = constr.check_stability();
        bool unitarity = constr.check_unitarity();
        bool perturbativity = constr.check_perturbativity();
        std::cout << pset << sep
            << stability << sep
            << unitarity << sep
            << perturbativity;

        if(higgsbounds){
            HB_init();
            HB_set_input_effC(model);
            HB_run_full(hbres, hbchan, hbobs, hbcomb);
            std::cout << sep << hbres[0];
        }
        std::cout << std::endl;

    }


}

