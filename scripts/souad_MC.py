#!/usr/bin/python

import numpy as np
import os
import commands
import subprocess
import pyslha2
import sys
import math
import random





def rang(start, stop, step):
  x = start
  while True:
    if x >= stop: return
    yield x
    x += step

for iN in rang(1.0, 1000000.0, 1.0):
####### input parameter ############################################################################
    mh=125.0
    typ=1
    mA=random.uniform(0, 1)*(780.0-240.0)+240.0
    mH=random.uniform(0, 1)*(mA-230.0)+130.0
    mC=mA
    tanb=1.0
    sba=random.uniform(0, 1)*(1.0-0.85)+0.85
    m12_2=random.uniform(0, 1)*mA*mA*tanb/(1.0+tanb*tanb)
    l6= 0.0
    l7= 0.0
    w_higgs = 21  # 11 = h, 12 = H, 21 = A
    E_GeV= 13000.0 # center-of-mass energy in GeV
    order_ggh= 2                # order ggh: -1 = off, 0 = LO, 1 = NLO, 2 = NNLO, 3 = N3LO
    order_bbh= 2                # order bbh: -1 = off, 0 = LO, 1 = NLO, 2 = NNLO
    data_file="data_pp_tb1.dat"
#######  run 2HDMC & HiggsBounds & HiggsSignals ####################################################
    os.chdir('2HDMC-1.7.0/')
    subprocess.call(['./ParameterScan_Hybrid_MultiDim', str(mh), str(mH), str(mA), str(tanb), str(mC), str(sba), str(l6), str(l7), str(m12_2), str(typ), str(data_file)])
    data = np.genfromtxt(data_file, delimiter=" ")
    subprocess.call(['rm', str(data_file)])
    os.chdir('../')
######  test vs experimental and theoretical constraints ###########################################
#    if data[7]<=1.0 and data[10]==1.0 and data[11]==1.0 and data[12]==1.0  and data[9]>=0.0027 :
######  run Sushi  #################################################################################
    LesHouches, decay= pyslha2.readSLHAFile('SusHi-1.7.0/bin/2HDMC_physicalbasis.in') 
    THDMC=LesHouches['2HDMC']  
    THDMC.entries[2]= typ
    THDMC.entries[3]= tanb 
    THDMC.entries[4]= m12_2 
    THDMC.entries[21]= mh
    THDMC.entries[22]= mH 
    THDMC.entries[23]= mA
    THDMC.entries[24]= mC
    THDMC.entries[25]= sba
    THDMC.entries[26]= l6
    THDMC.entries[27]= l7
    SUSHI1=LesHouches['SUSHI']
    SUSHI1.entries[2]=w_higgs
    SUSHI1.entries[4]=E_GeV
    SUSHI1.entries[5]=order_ggh
    SUSHI1.entries[6]=order_bbh
    LesHouches2={'SUSHI':LesHouches['SUSHI'],'2HDMC':LesHouches['2HDMC'],'SMINPUTS':LesHouches['SMINPUTS'],'DISTRIB':LesHouches['DISTRIB'],'SCALES':LesHouches['SCALES'],'RENORMBOT':LesHouches['RENORMBOT'],'PDFSPEC':LesHouches['PDFSPEC'],'VEGAS':LesHouches['VEGAS'],'FACTORS':LesHouches['FACTORS']}
    os.chdir('SusHi-1.7.0/bin/')
    pyslha2.writeSLHAFile('2HDMC_physicalbasis2.in',LesHouches2,decay)
    subprocess.call(['./sushi.2HDMC', '2HDMC_physicalbasis2.in', '2HDM.out'])
    with open('2HDM.out', 'a') as file:
            file.write('Block MASS')
    LesHouches3, decay= pyslha2.readSLHAFile('2HDM.out')

    ggh_NNLO = LesHouches3['SUSHIGGH'].entries[1]
    ggh_NLO = LesHouches3['XSGGH'].entries[2]
    ggh_LO = LesHouches3['XSGGH'].entries[1]
    bbh_NNLO = LesHouches3['XSBBH'].entries[3]
    bbh_NLO = LesHouches3['XSBBH'].entries[2]
    bbh_LO = LesHouches3['XSBBH'].entries[1]
    subprocess.call(['rm', '2HDM.out', '2HDM.out_murdep', '2HDMC.out', '2HDMC_physicalbasis2.in'])
    os.chdir('../../')
    data2 = open('pp_tb1.dat', 'a')
# les datas sont enregistrees sous l'ordre suivant:

#  1    2   3   4    5     6     7      8      9     10     11   12   13    14    15      16        17     18     19     20    21      22      23     24        25      26      27     28    29 
#  mh  mH  mA  mH+  sba  tanb  m12_2  ratio  chi2  pvalue  sta  uni  per  Brhww  Brhzz  Brhtatau  Brhss  Brhbb  Brhuu  Brhcc  Brhgg  Brhgaga  BrHww  BrHzz  BrHtautau  BrHss  BrHbb  BrHuu  BrHcc
#    
#   30       31       32    33      34     35       36      37      38      39     40     41      42     43      44      45     46     47      48      49      50      51      52       53      54      55       56
#  BrHtt   BrHH+H-  BrHhh  BrHAA  BrHgg  BrHgaga  BrHZA   GtotH  BrAtautau  BrAss  BrAbb  BrAuu  BrAcc  BrAtt  BrAgg  BrAgaga  BrAZh  BrAZH  GtotA   BrH+tb  BrH+cs  BrH+cb  BrH+wh  BrH+WA  BrH+WH  BrH+munu  BrH+taunu
#
# 57  58  59       60     61       62         63        64          65
# S   T   U    ggh_LO   bbh_LO   ggh_NLO   bbh_NLO   ggh_NNLO    bbh_NNLO    
    print >> data2, data[0], data[1], data[2], data[3], data[4], data[5], data[6], data[7], data[8], data[9], data[10], data[11], data[12], data[13], data[14], data[15], data[16], data[17], data[18], data[19], data[20], data[21], data[22], data[23], data[24], data[25], data[26], data[27], data[28], data[29], data[30], data[31], data[32], data[33], data[34], data[35], data[36], data[37], data[38], data[39], data[40], data[41], data[42], data[43], data[44], data[45], data[46], data[47], data[48], data[49], data[50], data[51], data[52], data[53], data[54], data[55], data[56], data[57], data[58], ggh_LO, bbh_LO, ggh_NLO, bbh_NLO, ggh_NNLO, bbh_NNLO   
    data2.close()
       
    


