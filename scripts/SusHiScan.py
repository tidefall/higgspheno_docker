""" 
This pyhton scrip will only run when sitting inside the SusHi directory

No cpp program required

Works via the pipe
"""
from ipdb import set_trace as st
import InputTools
import subprocess
import os
import shutil
import sys
import numpy as np
import RunSusHi
import Data
import time
import multiprocessing


def parameter_position(input_lines, block_name, param_number):
    ''' Get the position of a paramter in the input file.
    Will fail on blocks that don't have parameter numbers.
    

    Parameters
    ----------
    input_lines : list of byte array
        content of the input file to search
        
    block_name : byte array
        name of the block to seek
        
    param_number : int
        number of the parameter to seek
        

    Returns
    -------
    line_number : int
        number of the line containing the parameter
        zero indexed

    '''
    # finding the line number
    line_number = 0
    # get to the with block line
    block_line = b"Block " +block_name
    block_len = len(block_line)
    while input_lines[line_number][:block_len] != block_line:
        line_number += 1
    # now get to the param number
    line_number += 1
    str_num = str(param_number).encode()
    while input_lines[line_number].split()[0] != str_num:
        line_number += 1
    # now get the charicter number
    line = input_lines[line_number]
    charicter_number = line.index(str_num) + len(str_num) + 1  # must be after the parameter number
    char = line[charicter_number]
    line_len = len(line)
    blank_byte = char # the charicter currently selected should be blank
    while (char == blank_byte and charicter_number < line_len):
        char = line[charicter_number]
        charicter_number += 1
    charicter_number -= 1
    return line_number, charicter_number


def replace_parameter(input_lines, line_number, charicter_number, new_value):
    ''' Function to replace one of the parameters in the input file.
    

    Parameters
    ----------
    input_lines : list of byte array
        content of the input file to alter
        
    line_number : int
        number of the line containing the parameter
        zero indexed

    charicter_number: int
        number of the first charcter of the parameter
        zero indexed

    new_value : float
        new value the parameter should take

    '''
    change_line = input_lines[line_number]
    old_entry_len = len(change_line[charicter_number:].split()[0].split(b'#', 1)[0])
    new_line = change_line[:charicter_number] + str(new_value).encode() + change_line[charicter_number + old_entry_len:]
    input_lines[line_number] = new_line
    return input_lines


def replace_parameters(input_lines, line_numbers, charicter_numbers, new_values):
    ''' Function to replace any number of the parameters in the input file.
    

    Parameters
    ----------
    input_lines : list of byte array
        content of the input file to alter
        
    line_numbers : list of int
        numbers of the line containing the parameter
        zero indexed

    charicter_numbers: list of int
        numbers of the first charcter of the parameter
        zero indexed

    new_values : list of float
        new values the parameters should take

    '''
    for line_n, char_n, val in zip(line_numbers, charicter_numbers, new_values):
        if line_n is None:
            continue
        change_line = input_lines[line_n]
        old_entry_len = len(change_line[char_n:].split()[0].split(b'#')[0])
        new_line = change_line[:char_n] + str(val).encode() + change_line[char_n + old_entry_len:]
        input_lines[line_n] = new_line
    return input_lines


def fetch_result(out_lines, block_names, parameter_numbers):
    ''' Read the output file for any required paramters
    also deletes the file after reading it. 

    Parameters
    ----------
    out_lines : list of byte array
        the string output by SusHi
        
    block_names : list of byte array
        names of the blocks containing the results
        
    parameter_numbers : list of int
        names of the parameter numbers that are the results
        

    Returns
    -------
    results : list of float
        all numbers found

    '''
    if isinstance(out_lines, bytes):
        out_lines = out_lines.split(b'\n')
    # an array of dummy values is faster
    results = [[None for _ in numbers] for numbers in parameter_numbers]
    in_block = False
    block_finished = np.full(len(block_names), False)
    block_number = -1
    # get to the with block line
    for line in out_lines:
        if np.all(block_finished):
            break
        split_line = line.split()
        if len(split_line) < 2 or b'#' in split_line[0]:
            continue
        elif split_line[0] == b'Block':
            # new block
            in_block = False
            # is this a block to read from?
            if split_line[1] in block_names:
                in_block = True
                block_number = block_names.index(split_line[1])
                block_parameters = parameter_numbers[block_number]
        elif in_block:
            if int(split_line[0]) in block_parameters:
                para_number = block_parameters.index(int(split_line[0]))
                results[block_number][para_number] = float(split_line[1])
                if not None in results[block_number]:
                    block_finished[block_number] = True
    return results


def run_scan(input_lines, run, scan_param, low, high, steps, locations):
    ''' Calculate cross sections for a range of values.
    

    Parameters
    ----------
    input_lines : list of byte array
        the skeleton input file 2HDMC_physicalbasis.in
        
    run : Run
        collection of datasets to save output
        
    scan_param : str
        name of the parameter to scan in
        
    low : float
        start of the scan
        
    high : float
        end of the scan (inclusive)
        
    steps :int
        number of steps for the scan
        

    '''
    program = "./bin/sushi.2HDMC"
    block_name = b"2HDMC"
    scan_dict = { "type": 2  , 
                  "tanbeta": 3,
                  "m12": 4,
                  "mh": 21,
                  "mH": 22,
                  "mA": 23,
                  "mC": 24,
                  "sinbetaalpha": 25,
                  "lambda6": 26,
                  "lambda7": 27}
    param_number = scan_dict[scan_param.lower()]
    line_number, charicter_number = parameter_position(input_lines, block_name, param_number)
    temp_name = "/home/henry/tmp/scan_tmp.out"
    column_names = np.array([scan_param, "ggh cross section in pb",
                    "bbh cross section in pb"], dtype='S50')
    block_names = [b"SUSHIggh", b"SUSHIbbh"]
    parameter_numbers = [[1], [1]]
    values = np.linspace(low, high, steps)
    findings = np.full((steps, len(column_names)), np.nan)
    # set yukawa type
    yukawa_block_name = b"2HDMC"
    yukawa_param_num = 2
    yukawa_line_num, yukawa_char_num = parameter_position(input_lines, yukawa_block_name, yukawa_param_num)
    input_lines = replace_parameter(input_lines, yukawa_line_num, yukawa_char_num, run.infos['uniqueinputs']['yukawa'])
    for row, entry in enumerate(values):
        # set the parameter
        input_lines = replace_parameter(input_lines, line_number, charicter_number, entry)
        # run the input
        output_lines = RunSusHi.run_SusHi(input_lines, locations)
        # get the return
        out = fetch_result(output_lines, block_names, parameter_numbers)
        findings[row, 1] = entry
        findings[row, 1] = out[0][0]
        findings[row, 2] = out[1][0]
    output_name = "scan"
    info = {'dtype': float,
            'part': 'scan',
            'columns': column_names}
    run.add_ds(output_name, info, findings)
            
# get the cross sections for a list of parameters in an h5 file
# save in the same h5 file
def batch_crossSections(input_lines, run, locations, batch_size=3):
    '''Add the cross sections for a batch in the file
    

    Parameters
    ----------
    input_lines : list of byte array
        the skeleton input file 2HDMC_physicalbasis.in
        
    run : Run
        collection of datasets to save output
        
    batch_size : int
        number of items to process


    '''
    crossSec_ds_name = "crossSections"
    # read a batch
    all_input = run.datasets['uniqueinputs']
    columns = run.infos['uniqueinputs']['columns']
    CS_ds_exists = crossSec_ds_name in run.datasets
    # there are 3 versions gg to h and bb to h for each of h in (h,H,A)
    CS_column_format = ["lo $\\sigma(g g \\rightarrow {})$ (pb)",
                        "nlo $\\sigma(g g \\rightarrow {})$ (pb)",
                        "nnlo $\\sigma(g g \\rightarrow {})$ (pb)",
                        "lo $\\sigma(b b \\rightarrow {})$ (pb)",
                        "nlo $\\sigma(b b \\rightarrow {})$ (pb)",
                        "nnlo $\\sigma(b b \\rightarrow {})$ (pb)"]
    #higgs_types = {'h': 11, 'H' : 12, 'A': 21}
    higgs_types = {'H' : 12, 'A': 21}
    if CS_ds_exists:
        point_reached = len(run.datasets[crossSec_ds_name])
        CS_columns = run.infos[crossSec_ds_name]['columns']
    else:
        point_reached = 0
        # this dic mapt the higgs typ to the value needed by the input file
        CS_columns = []
        for symbol in higgs_types:
            CS_columns += [col.format(symbol) for col in CS_column_format]
        info = {'part': 'CrossSections',
                'dtype': float,
                'columns': CS_columns}
    total_length = len(all_input)
    if total_length == point_reached:
        last_batch = True
        return last_batch
    if total_length <= batch_size + point_reached -1:
        last_batch = True
        points = all_input[point_reached:]
    else:
        last_batch = False
        points = all_input[point_reached: point_reached+batch_size]
    # make array for this batch
    crossSecs = np.full((len(points), len(CS_columns)), np.nan)
    # set up to run script
    program = "./bin/sushi.2HDMC"
    temp_name = "./scan_tmp.out"
    block_name = b"2HDMC"
    # python2 and 3 have some divergance in the way they treat strings, so acount for all possibles
    param_numbers = {'$m_H$': 22 , '$m_A$': 23, '$m_C$' :24 , '$sin(\\beta - \\alpha)$': 25,
                     b'$m_H$': 22 , b'$m_A$': 23, b'$m_C$' :24 , b'$sin(\\beta - \\alpha)$': 25,
                     '$tan(\\beta)$': 3, '$m_{12}^2$': 4,
                     b'$tan(\\beta)$': 3, b'$m_{12}^2$': 4}
    line_numbers = []; charicter_numbers = []
    for name in columns:
        line_num, char_num = parameter_position(input_lines, block_name, param_numbers[name])
        line_numbers.append(line_num)
        charicter_numbers.append(char_num)
    # set yukawa type
    yukawa_block_name = b"2HDMC"
    yukawa_param_num = 2
    yukawa_line_num, yukawa_char_num = parameter_position(input_lines, yukawa_block_name, yukawa_param_num)
    input_lines = replace_parameter(input_lines, yukawa_line_num, yukawa_char_num, run.infos['uniqueinputs']['yukawa'])
    higgs_block_name = b"SUSHI"
    higgs_param_num = 2
    higgs_line_num, higgs_char_num = parameter_position(input_lines, higgs_block_name, higgs_param_num)
    results_block_names = [b"SUSHIggh", b"SUSHIbbh", b"XSGGH", b"XSBBH"]
    results_parameter_numbers = [[1], [1], [1, 2], [1, 2]]
    # run script
    #check_cols = ["$m_H$", "$m_A$", "$sin(\\beta - \\alpha)$", "$tan(\\beta)$"]
    #check_indices = [columns.index(c) for c in check_cols]
    #check_params = [250, 500, 1, 5]
    #print(' '.join(check_cols))
    for row, params in enumerate(points):
        #print(str(params[check_indices]))
        # set the parameters
        input_lines = replace_parameters(input_lines, line_numbers, charicter_numbers, params)
        for h_i, symbol in enumerate(higgs_types):
            # set the higgs type
            input_lines = replace_parameter(input_lines, higgs_line_num, higgs_char_num, higgs_types[symbol])
            # run the input
            output_lines = RunSusHi.run_SusHi(input_lines, locations)
            # get the return
            out = fetch_result(output_lines, results_block_names, results_parameter_numbers)
            #aout = np.array([x for line in out for x in line])
            #print(aout)
            #if np.nan in aout or None in aout:
            #    print("Problem; input lines;")
            #    for line in input_lines:
            #        print(line)
            #    print("Output Lines;")
            #    for line in output_lines:
            #        print(line)
            #    raise RuntimeError
            higgs_start = h_i * len(CS_column_format)
            crossSecs[row, higgs_start + 0] = out[2][0]
            crossSecs[row, higgs_start + 1] = out[2][1]
            crossSecs[row, higgs_start + 2] = out[0][0]
            crossSecs[row, higgs_start + 3] = out[3][0]
            crossSecs[row, higgs_start + 4] = out[3][1]
            crossSecs[row, higgs_start + 5] = out[1][0]
        #if np.allclose(params[check_indices], check_params):
        #    print("Got it !!!")
        #    with open("/home/henry/Programs/2HDM/tmp/check_results{}.txt".format(row), 'wb') as cfile:
        #        cfile.write("{} ~~~ \n".format(row).encode())
        #        cfile.writelines(input_lines)
        #        cfile.write(b"\n ~~~ \n")
        #        cfile.writelines(output_lines)
        #        cfile.write(b"\n ~~~ \n")
        #        cfile.write(str(list(zip( CS_columns, crossSecs[row]))).encode())
    # if there are already cross sections append to them
    if CS_ds_exists:
        current = run.datasets[crossSec_ds_name]
        new = np.vstack((current, crossSecs))
        run.datasets[crossSec_ds_name] = new
    else:
        run.add_ds(crossSec_ds_name, info, crossSecs)
    run.write()
    return last_batch


# define a worker object, one will go in each thread
def worker(input_lines, run_dir, locations, run_condition, batch_size):
    run = Data.Run(run_dir)
    batch_n = 0
    last_batch = False  # the ru condition may never be true
    if run_condition == 'continue':
        while os.path.exists(run_condition):
            print("Starting batch {}".format(batch_n))
            last_batch = batch_crossSections(input_lines, run, locations, batch_size)
            if last_batch:
                break
            batch_n += 1
    else:
        while time.time() < run_condition:
            print("Starting batch {}".format(batch_n))
            last_batch = batch_crossSections(input_lines, run, locations, batch_size)
            if last_batch:
                break
            batch_n += 1
    if last_batch:
        print("Finished {}".format(run_dir))
    else:
        print("{} incomplete".format(run_dir))
    #return last_batch, batch_n


def add_crossSections(input_name, run_dir, locations, multi_thread=True, one_free=True):
    '''Add the cross sections for a batch in the file
    works while the file continue exists 
    

    Parameters
    ----------
    input_name : str
        name of the skeleton input file 2HDMC_physicalbasis.in
        
    run_dir : str
        name of the directory containing the data
    '''
    last_batch = False
    # decide on a stop condition
    if os.path.exists('continue'):
        run_condition = 'continue'
    else:
        if InputTools.yesNo_question("Would you like to do a time based run? "):
            run_time = InputTools.get_time("How long should it run?")
            stop_time = time.time() + run_time
            run_condition = stop_time
        elif InputTools.yesNo_question("Would you like to create a continue file?"
                                       +" (be sure you can delete it while the"
                                       +" program is running!) "):
            open('continue', 'w').close()
            run_condition = 'continue'
        else:
            return 
    profile_start_time = time.time()
    batch_size = 20
    # work out how many threads
    if multi_thread:
        n_threads = multiprocessing.cpu_count()
        if n_threads > 1 and one_free:
            n_threads -= 1
        print("Running on {} threads".format(n_threads))
        # work out how many data components
        segment_names = [os.path.join(run_dir, name) for name in next(os.walk(run_dir))[1]]
        if len(segment_names) == 0:
            run = Data.Run(run_dir)
            while run_dir.endswith('/'):
                run_dir = run_dir[:-1]
            run_dir += "_split"
            if not os.path.exists(run_dir):
                print("splitting run in dir " + run_dir)
                run.split(n_threads, run_dir)
            segment_names = [os.path.join(run_dir, name) for name in next(os.walk(run_dir))[1]]
    else:
        segment_names = [run_dir]
    segment_names = np.array(segment_names)
    print("Segment names")
    print(segment_names)
    # check that mh = 125
    input_lines = RunSusHi.get_skeleton(input_name)
    block_name = b"2HDMC"
    param_number = 21
    line_number, charicter_number = parameter_position(input_lines, block_name, param_number)
    input_lines = replace_parameter(input_lines, line_number, charicter_number, 125.)
    # possibly excessive, but doubel check that each worked gets a difernet lines object
    clones_input_lines = [list(input_lines) for _ in segment_names]
    # make a run for each thread
    if multi_thread:
        #pool = multiprocessing.Pool(n_threads)
        job_list = []
        # now each segment makes a worker
        #args = []
        for lines, run_d in zip(clones_input_lines, segment_names):
            #args.append((lines, run_d, locations, run_condition, batch_size))
            args = (lines, run_d, locations, run_condition, batch_size)
            job = multiprocessing.Process(target=worker, args=args)
            job.start()
            job_list.append(job)
        for job in job_list:
            job.join()
        #results  =pool.starmap(worker, args)
        #pool.close()
        #print(str(len(results)) + " pools used")
        #finished, num_batches = zip(*results)
        #batches_finished = sum(num_batches)
    else:
        #finished, batches_finished = worker(input_lines, run_dir, locations, run_condition, batch_size)
        #finished = [finished]
        worker(input_lines, run_dir, locations, run_condition, batch_size)
    profile_run_time = time.time() - profile_start_time
    print("All processes ended in {} minutes.".format(profile_run_time/60))
    #items_ran = batch_size * batches_finished
    #profile_message = "Ran {} items in {} seconds\n".format(items_ran, profile_run_time)
    #if items_ran != 0:
    #    profile_message += "{} seconds per item".format(profile_run_time/items_ran)
    #print(profile_message)
    #with open("speed_PipeSusHi.txt", 'w') as f:
    #    f.write(profile_message)
    #if np.all(finished):
    #    print("Finished last batch.")
    #else:
    #    print("stopped before last batch")
    #    incomplete = ~np.array(finished, dtype=bool)
    #    print("Incomplete sections; ")
    #    print(segment_names[incomplete])


def main():
    """ add cross sections """
    locations = RunSusHi.setup()
    run_dir = InputTools.get_dir_name("Name the data file (existing): ")
    if run_dir == '':
        run_dir = "/home/henry/Programs/docker/2HDM_live/test_sushi/dayrun1/"
    if not os.path.exists(run_dir):
        print("Error, this directory dosn't exist")
        return
    input_name = InputTools.get_file_name("Give the SusHi input file to start from; ", '.in')
    if input_name == '':
        input_name = "example/2HDMC_physicalbasis.in"
    add_crossSections(input_name, run_dir, locations)


def redo(run_dir):
    locations = RunSusHi.setup()
    suffix = "_rep"
    input_name = "../PipeSusHi/example/2HDMC_physicalbasis.in"
    new_dir = run_dir.rstrip(os.sep) + suffix
    try:
        shutil.copytree(run_dir, new_dir)
    except FileExistsError:
        return
    crossSec_ds_name = "crossSections"
    try:
        os.remove(os.path.join(new_dir, crossSec_ds_name +'.csv'))
    except OSError:
        pass
    try:
        os.remove(os.path.join(new_dir, crossSec_ds_name +'.info'))
    except OSError:
        pass
    add_crossSections(input_name, new_dir, locations)
    split_path = new_dir.rstrip(os.sep) + "_split"
    Data.merge_superdir(split_path)

def do_all(meta_dir):
    locations = RunSusHi.setup()
    suffix = "_rep"
    input_name = "../PipeSusHi/example/2HDMC_physicalbasis.in"
    for name in os.listdir(meta_dir):
        path = os.path.join(meta_dir, name)
        if '.' in name:
            continue
        if not os.path.exists(os.path.join(path, 'uniqueinputs.csv')):
            continue
        add_crossSections(input_name, path, locations)
        split_path = path.rstrip(os.sep) + "_split"
        Data.merge_superdir(split_path)


def redo_all(meta_dir):
    for name in os.listdir(meta_dir):
        path = os.path.join(meta_dir, name)
        if '.' in name or "unique" not in name:
            continue
        if not os.path.exists(os.path.join(path, 'uniqueinputs.csv')):
            continue
        if name.endswith("_rep"):
            continue
        print(name)
        if os.path.exists("stop"):
            break
        redo(path)
        
if __name__ == '__main__':
    #redo(InputTools.get_dir_name("Name the run to repeat; "))
    #redo_all(InputTools.get_dir_name("Name the directory containing runs to repeat; "))
    #do_all(InputTools.get_dir_name("Name the meta dir containing runs; "))
    main()
    

