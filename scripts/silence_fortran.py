import os
#from ipdb import set_trace as st
print_statement = "write(*,*)"  # changing lines that start with this
continuation = "&"  # or lines that follow and start with this
comment = "! silence "  # the word silence makes the substiutions identifiable
# in the current directory find fortran file names
fortran_names = [f for f in os.listdir('.') if f[-4:].lower() == '.f90']
for f_name in fortran_names:
    # read from file
    with open(f_name, 'r') as f_file:
        lines = f_file.readlines()
    # process the file
    i = 0
    commenting = False
    previous_line = ""
    while i < len(lines):
        strip_line = lines[i].strip()
        # check if we should/shouldn't be commenting
        if strip_line.startswith(print_statement):
            commenting = True
        elif not (strip_line.startswith(continuation) or
                  previous_line.endswith(continuation)):
            commenting = False
        # now add comments charcters if required
        if commenting:
            lines[i] = comment + lines[i]
        previous_line = strip_line
        i += 1
    # write back to file
    with open(f_name, 'w') as f_file:
        f_file.writelines(lines)




