import MonteCarlo
import InputTools
import os
import Data
import numpy as np

def both(run):
    if isinstance(run, str):
        run = Data.Run(run)
    # remove anything related to valid or unique
    # with the exception of unique inputs, becuase the order of unique inputs must remain the smae
    for name in ['validinputs', 'uniquechecks', 'validchecks']:
        try:
            del run.datasets[name]
        except KeyError:
            pass
        try:
            del run.infos[name]
        except KeyError:
            pass
    # yank unique inputs
    uniqueinputs_old = run.datasets.pop('uniqueinputs')
    uniqueinputs_old_info = run.infos.pop('uniqueinputs')
    # the checks may need to be done in batches else the ram will die in a fire
    batch_length = 1000
    inputs = run.datasets['inputs']
    input_info = run.infos['inputs']
    n_inputs = len(inputs)
    n_batches = int(np.ceil(n_inputs/batch_length))
    new_checks = []
    for i in range(n_batches):
        print('.', end='', flush=True)
        batch = inputs[i*batch_length:(i+1)*batch_length]
        checks = MonteCarlo.check_bounds(batch, input_info)
        new_checks.append(checks)
    new_checks = np.vstack(new_checks)
    # just for amusment sake, compare to exisiting checks
    old_checks = run.datasets.pop('checks')
    similarity = np.sum(old_checks == new_checks)/(old_checks.shape[0]*old_checks.shape[1])
    print("{}% matching old checks".format(100*similarity))
    run.datasets['checks'] = new_checks
    run.add_unique()
    # that that the order of unique did not change
    uniqueinputs = run.datasets['uniqueinputs']
    close = np.allclose(uniqueinputs, uniqueinputs_old)
    if not close:
        print("ordering of uniqueinpus has changed, fixing")
        order = [np.argmin(np.abs(np.sum(uniqueinputs - old, axis=1)))
                 for old in uniqueinputs_old]
        if len(order) == len(set(order)): # no repeats, use it
            print("success")
            run.datasets['uniqueinputs'] = uniqueinputs[order]
            run.datasets['uniquechecks'] = run.datasets['uniquechecks'][order]
            np.testing.assert_allclose(run.datasets['uniqueinputs'], uniqueinputs_old)
        else: # something is screwy
            print("Failed to order unique inputs for run {}".format(run.dir_name))
            run.add_ds('old_uniqueinputs', uniqueinputs_old_info, uniqueinputs_old)
    run.write()

    
def all_dir(dir_name):
    for name in os.listdir(dir_name):
        path = os.path.join(dir_name, name)
        try:
            run = Data.Run(path)
        except:
            print("{} does not seem to be a run".format(path))
            continue
        print("working on {}".format(path))
        both(run)


if __name__ == '__main__':
    all_dir(InputTools.get_dir_name("Give the parent dir to recheck runs in; "))

