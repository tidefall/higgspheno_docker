""" jumble of tools designed to make plotting easier; may requre python3 """
import numpy as np
import matplotlib
from matplotlib import pyplot as plt
import os
#from ipdb set_trace as st
import InputTools
import Data
from sklearn import preprocessing

def fetch_col(run=None, location=None):
    if run is None:
        run_dir = InputTools.get_dir_name("What is the directory of the run? ")
        run = Data.Run(run_dir)
    if location is None:
        print("Avalible cols:")
        stripped_cols = run.strip_columns()
        InputTools.print_strlist(stripped_cols.keys())
        col_name = InputTools.list_complete("Chose a column; ",
                                            stripped_cols.keys())
        col_name = col_name.strip()
        location = stripped_cols[col_name]
    elif isinstance(location, str):
        # then the location is the stripped name
        col_name = location
        stripped_cols = run.strip_columns()
        try:
            location = stripped_cols[col_name]
        except KeyError:
            common_substitutions = [(' )', ')'), ('NNLO', 'nnlo')]
            keys_subbed = {key: key for key in stripped_cols.keys()}
            col_name_subbed = col_name
            for i, sub_list in enumerate(common_substitutions):
                replacement = "<{}>".format(i)
                for sub in sorted(sub_list, key=len):
                    keys_subbed = {key.replace(sub, replacement): value
                                   for key, value in keys_subbed.items()}
                    col_name_subbed = col_name_subbed.replace(sub, replacement)
            subbed_name = keys_subbed[col_name_subbed]
            print("Guessing {} to be equivalent to {}".format(col_name, subbed_name))
            col_name = subbed_name
            location = stripped_cols[col_name]
    pretty_name = run.pretty_columns[col_name]
    return pretty_name, run.datasets[location[0]][:, location[1]]


def pretty_scatter(xs, ys,
                   x_label, y_label,
                   title, colours=None, cbar_label=None,
                   semilogy=False, loglog=False,
                   size=1.5):
    if colours is None:
        colours = np.zeros_like(xs)
    if len(xs) != len(ys) or len(xs) != len(colours):
        #trim
        min_len = min((len(xs), len(ys), len(colours)))
        print("Trimming to {}".format(min_len))
        xs = xs[:min_len]
        ys = ys[:min_len]
        colours = colours[:min_len]
    #plt.rc('text', usetex=True)
    fig, ax = plt.subplots()
    #fig.set_size_inches((15, 15))
    c_map = "viridis"
    if semilogy:
        plt.semilogy()
    if loglog:
        plt.loglog()
    plt.scatter(xs, ys, c=colours, cmap=c_map, s=size)
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.title(title)
    if cbar_label is not None:
        plt.colorbar(label=cbar_label)
    return fig, ax


def make_plots(dir_name = "./plottest0/", run=None):
    title = "TypeI"
    if run is None:
        run = Data.Run(dir_name)
    min_len = min([len(d) for d in run.datasets.values()])

    ntanb, tanb = fetch_col(run, 'tan(beta)')
    tanb = tanb[:min_len]
    nmH, mH = fetch_col(run, 'mH')
    mH = mH[:min_len]
    nmA, mA = fetch_col(run, 'mA')
    mA = mA[:min_len]
    nbrHbb, brHbb = fetch_col(run, 'BR(H-> b b)')
    brHbb = brHbb[:min_len]
    nbrHtautau, brHtautau = fetch_col(run, 'BR(H-> tau tau)')
    brHtautau = brHtautau[:min_len]
    nbrAbb, brAbb = fetch_col(run, 'BR(A-> b b)')
    brAbb = brAbb[:min_len]
    nbrAtautau, brAtautau = fetch_col(run, 'BR(A-> tau tau)')
    brAtautau = brAtautau[:min_len]
    nbrAZH, brAZH = fetch_col(run, 'BR(A-> Z H)')
    brAZH = brAZH[:min_len]
    nbrHZA, brHZA = fetch_col(run, 'BR(H-> Z A)')
    brHZA = brHZA[:min_len]
    nCSppH, CSppH = fetch_col(run, 'NNLO sigma(p p -> H) (pb)')
    CSppH = CSppH[:min_len]
    nCSbbH, CSbbH = fetch_col(run, 'NNLO sigma(b b -> H) (pb)')
    CSbbH = CSbbH[:min_len]
    nCSggH, CSggH = fetch_col(run, 'NNLO sigma(g g -> H) (pb)')
    CSggH = CSggH[:min_len]
    nCSppA, CSppA = fetch_col(run, 'NNLO sigma(p p -> A) (pb)')
    CSppA = CSppA[:min_len]
    nCSbbA, CSbbA = fetch_col(run, 'NNLO sigma(b b -> A) (pb)')
    CSbbA = CSbbA[:min_len]
    nCSggA, CSggA = fetch_col(run, 'NNLO sigma(g g -> A) (pb)')
    CSggA = CSggA[:min_len]

    pretty_scatter(mH, brHbb, nmH, nbrHbb, title, CSppA, nCSppA)
    plt.show()
    pretty_scatter(mA, CSbbA*brAZH*brHbb, 
                   nmA, 'X'.join((nCSbbA,nbrAZH,nbrHbb)),
                   title, mH, nmH, semilogy=True)
    plt.show()
    pretty_scatter(mA, CSggA, nmA, nCSggA, title, tanb, ntanb)
    plt.show()
    pretty_scatter(mH, CSppH*brHZA*brAbb, 
                   nmH, 'X'.join((nCSppH,nbrHZA,nbrAbb)),
                   title, mA, nmA, semilogy=True)
    plt.show()
    pretty_scatter(CSbbA*brAZH*brHbb, CSggA*brAZH*brHbb,
                   'X'.join((nCSbbA,nbrAZH,nbrHbb)), 'X'.join((nCSggA,nbrAZH,nbrHbb)),
                   title, tanb, ntanb)
    plt.show()
    pretty_scatter(mA, CSppA*brAZH*brHtautau, 
                   nmA, 'X'.join((nCSppA,nbrAZH,nbrHtautau)),
                   title, mH, nmH, semilogy=True)
    plt.show()
    pretty_scatter(mH, CSppH*brHZA*brAtautau, 
                   nmH, 'X'.join((nCSppH,nbrHZA,nbrAtautau)),
                   title, mA, nmA, semilogy=True)
    plt.show()


def devation(run, ds_name):
    """ Calculate the devation of each point from the mean.
    Results are stored in the dataset file
    

    Parameters
    ----------
    run : Run
        data to work on
        
    ds_name : str
        name of dataset

    """
    dset = run.datasets[ds_name]
    dset_info = run.infos[ds_name]
    scalar = preprocessing.RobustScaler().fit(dset)
    devations = scalar.transform(dset)
    mid_point = np.average(devations, axis=0)
    displacements = np.linalg.norm(devations-mid_point, axis=1)
    name = "devations_" + ds_name
    info = {'dtype': float,
            'columns': ['devation'],
            'part': name}
    run.add_ds(name, info, displacements)
    run.write()


def plot_allowed(run, ds_name="inputs"):
    """ Plot both allowed and forbidden points in 2d.
    To display the plot call "plt.show()"

    Parameters
    ----------
    run : Run
        data to work on
        

    """
    plt.rc('text', usetex=True)
    all_points = run.datasets[ds_name]
    info = run.infos[ds_name]
    print(info)
    mask = np.all(run.datasets["checks"], axis=1)
    axis = info['columns']
    axis = axis.astype(str)
    x_index = int(input("Index the x axis: "))
    y_index = int(input("Index the y axis: "))
    # make axes
    fig, ax = plt.subplots()
    fig.set_size_inches((15,15))
    ax.set_title("Plot of allowed points")
    if InputTools.yesNo_question("Make log-y? (y/n) "):
        plt.semilogy()
    ax.set_xlabel(axis[x_index])
    ax.set_ylabel(axis[y_index])
    ax.scatter(all_points[mask, x_index], all_points[mask, y_index], label="allowed")
    ax.scatter(all_points[~mask, x_index], all_points[~mask, y_index], label="forbidden")
    ax.legend()
    return fig, ax


def multidim_plots(run, showing='devation', fig=None, ax_arr=None, s=1., alpha=0.8,
                   check_num=None, invert_check=False, skip_params=['$m_C$', '$sin(\\beta - \\alpha)$']):
    """ Plot all dimensions as a grid.
    To display the plot call "plt.show()"

    Parameters
    ----------
    run : Run
        data to work on
        
        
    tanb_strat : bool
         (Default value = True)
         Do we stratify the data with tan(beta)

    """
    plt.rc('text', usetex=True)
    clean_showing = showing.lower().strip()
    if clean_showing == 'devation':
        positions_name = 'validinputs'
        colour_name = 'devations_' + positions_name
        if colour_name not in run.datasets:
            devation(run, positions_name)
    elif clean_showing == 'tanbstrat':
        positions_name = 'validinputs'
    elif clean_showing == 'checks':
        positions_name = 'inputs'
    elif clean_showing == 'predictions':
        positions_name = 'validinputs'
    positions = run.datasets[positions_name]
    info = run.infos[positions_name]
    print(info)
    axis = [(name, i) for i, name in enumerate(info['columns']) if name not in skip_params]
    if 'mins' in info:
        draw_lines = True
        mins = info['mins']
        maxes = info['maxes']
    else:
        draw_lines = False
    if fig is None:
        # make axes
        fig, ax_arr = plt.subplots(len(axis), len(axis))
        fig.set_size_inches((15,15))
        fig.suptitle("Points for Type{}".format(info['yukawa']))
        # decide on axes limits
        new_plot = True
        lim_colour = 'red'
        buffers = [(np.max(col) - np.min(col))/10 for col in positions.T]
        column_xlims = [(np.min(col)-b, np.max(col)+b) for b, col 
                        in zip(buffers, positions.T)]
        row_ylims = [(np.min(col)-b, np.max(col)+b) for b, col 
                     in zip(buffers, positions.T)]
    else:
        draw_lines = False
        new_plot = False
    # decide on coloring
    if clean_showing == 'tanbstrat':
        colours = ['green', 'blue', 'red']
        tb_index = axis[next(a for a in axis if a[0] == "$tan(\\beta)$")][1]
        color_values = [colours[int(tb>5) + int(tb>10)]
                        for tb in positions[:, tb_index]]
        # add a ledgend for this
        ax = ax_arr[tb_index, tb_index]
        ax.scatter([], [], c=colours[0],
                   label="$tan(\\beta) < 5$")
        ax.scatter([], [], c=colours[1],
                   label="$5 < tan(\\beta) < 10$")
        ax.scatter([], [], c=colours[2],
                   label="$10 < tan(\\beta)$")
        ax.legend()
    elif clean_showing == 'checks':
        checks = run.datasets['checks']
        if check_num is None:
            # only intrested in checks that sometimes fail
            use_cols = ~np.all(checks, axis=0)
            my_cm = matplotlib.cm.get_cmap('gist_rainbow')
            colour_choices = [my_cm(x) for x in np.linspace(0., 1., sum(use_cols))]
        else:
            use_cols = np.array([i==check_num for i in range(checks.shape[1])])
            RED = (1., 0., 0.)
            GREEN = (0., 1., 0.)
            colour_choices = [RED] if invert_check else [GREEN]
        checks = checks[:, use_cols]
        if invert_check:
            checks = ~checks
        if not np.any(checks):
            print("No matching points!!")
            return fig, ax_arr
        color_values = np.sum([[cc]*np.sum(checks[:, i]) for i, cc in enumerate(colour_choices)],
                              axis=0)
        # now rearange the points to match the colours
        positions = np.vstack([positions[check] for check in checks.T])
        # add a ledgend for this
        ax = ax_arr[0, -1]
        names = np.array(run.infos['checks']['columns'])[use_cols]
        if invert_check:
            names = ['not ' + n for n in names]
        for check, colour in zip(names, colour_choices):
            ax.scatter([], [], c=colour,
                       label=str(check))
        ax.legend()
    elif clean_showing == 'predictions':
        nbrHbb, brHbb = fetch_col(run, 'BR(H-> b b)')
        nbrAZH, brAZH = fetch_col(run, 'BR(A-> Z H)')
        nCSppA, CSppA = fetch_col(run, 'NNLO sigma(p p -> A) (pb)')
        color_values = brHbb * brAZH * CSppA
    else:
        color_values = run.datasets[colour_name]
    c_map = "viridis"
    # first plot the points
    for row_n, row in enumerate(ax_arr):
        for col_n, ax in enumerate(row):
            if(row_n > col_n):
                # teh axis don't containn all the columns of the data
                i = axis[row_n][1]
                j = axis[col_n][1]
                if draw_lines and i < len(mins):
                    ax.axhline(mins[i], color=lim_colour, linewidth=0.5)
                    ax.axhline(maxes[i], color=lim_colour, linewidth=0.5)
                if draw_lines and j < len(mins):
                    ax.axvline(mins[j], color=lim_colour, linewidth=0.5)
                    ax.axvline(maxes[j], color=lim_colour, linewidth=0.5)
                ax.scatter(positions[:, j], positions[:, i],
                            c=color_values, cmap=c_map,
                            s=s,
                            alpha=alpha)
    if new_plot:
        # now we have axis scales
        # add the labels and apply the scales
        for row_n, row in enumerate(ax_arr):
            for col_n, ax in enumerate(row):
                # teh axis don't containn all the columns of the data
                i = axis[row_n][1]
                j = axis[col_n][1]
                ax.set_xlim(column_xlims[j])
                ax.set_ylim(row_ylims[i])

                if row_n != len(ax_arr)-1:
                    ax.set_xticklabels(['']*10)
                    ax.set_xticks([])
                if col_n != 0:
                    ax.set_yticklabels(['']*10)
                    ax.set_yticks([])
                
                if i == j: # and False:
                    xmin, xmax = ax.get_xlim()
                    xmid = float(xmax-xmin)/2 + xmin
                    ymin, ymax = ax.get_ylim()
                    ymid = float(ymax-ymin)/2 + ymin
                    label = axis[row_n][0]
                    size = 100./np.sqrt(len(label))
                    ax.text(xmid, ymid, label, fontsize=size, ha='center', va='center')
        plt.subplots_adjust(wspace=0, hspace=0)
    return fig, ax_arr


def main():
    print("Get one column of one dataset with fetch_col")
    print("Make a pretty scatter with pretty_scatter")

if __name__ == "__main__":
    main()
