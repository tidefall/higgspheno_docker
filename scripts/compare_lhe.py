#! /usr/bin/python2
# make fstrings work in older python versions
# -*- coding: future_fstrings -*-
""" compare the pyslha and charicter substitution techniques """
#from ipdb set_trace as st
import pyslha2
import SusHiScan, RunSusHi
import Data

def make_pair(model_type, higgs, input_params, input_slha, locations):
    higgs_types = {'h': 11, 'H' : 12, 'A': 21}
    columns = ['$m_H$', '$m_A$', '$m_C$', '$sin(\\beta - \\alpha)$', '$tan(\\beta)$', '$m_{12}^2$']
    if isinstance(higgs, str):
        h_i = higgs_types[higgs]
    else:
        h_i = higgs
    # Original ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # set up to run script
    block_name = b"2HDMC"
    # python2 and 3 have some divergance in the way they treat strings, so acount for all possibles
    param_numbers = {'$m_H$': 22 , '$m_A$': 23, '$m_C$' :24 , '$sin(\\beta - \\alpha)$': 25,
                     b'$m_H$': 22 , b'$m_A$': 23, b'$m_C$' :24 , b'$sin(\\beta - \\alpha)$': 25,
                     '$tan(\\beta)$': 3, '$m_{12}^2$': 4,
                     b'$tan(\\beta)$': 3, b'$m_{12}^2$': 4}
    line_numbers = []; charicter_numbers = []
    for name in columns:
        line_num, char_num = SusHiScan.parameter_position(input_slha, block_name, param_numbers[name])
        line_numbers.append(line_num)
        charicter_numbers.append(char_num)
    higgs_block_name = b"SUSHI"
    higgs_param_num = 2
    higgs_line_num, higgs_char_num = SusHiScan.parameter_position(input_slha, higgs_block_name, higgs_param_num)
    CS_column_format = ["lo $\\sigma(g g \\rightarrow {})$ (pb)",
                        "nlo $\\sigma(g g \\rightarrow {})$ (pb)",
                        "nnlo $\\sigma(g g \\rightarrow {})$ (pb)",
                        "lo $\\sigma(b b \\rightarrow {})$ (pb)",
                        "nlo $\\sigma(b b \\rightarrow {})$ (pb)",
                        "nnlo $\\sigma(b b \\rightarrow {})$ (pb)"]
    results_block_names = [b"SUSHIggh", b"SUSHIbbh", b"XSGGH", b"XSBBH"]
    results_parameter_numbers = [[1], [1], [1, 2], [1, 2]]
    # run script
    original_slha = SusHiScan.replace_parameters(input_slha, line_numbers, charicter_numbers, input_params)
    original_slha = SusHiScan.replace_parameter(original_slha, higgs_line_num, higgs_char_num, h_i)
    # run the input
    original_out_lines = RunSusHi.run_SusHi(input_slha, locations)
    # get the return
    original_out = SusHiScan.fetch_result(original_out_lines, results_block_names, results_parameter_numbers)
    # Modified ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    blocks, decays = pyslha2.readSLHA('\n'.join([l.decode() for l in input_slha]), ignorenomass=True)
    THDMC=blocks['2HDMC']  
    THDMC.entries[2]= model_type
    # tanb m12_2
    THDMC.entries[3, 4]= input_params[4:]
    # mh
    THDMC.entries[21]= 125.
    # mH mA mC sba
    THDMC.entries[22, 23, 24, 25]= input_params[:4]
    # l6 l7
    THDMC.entries[26, 27]= 0, 0
    SUSHI1=blocks['SUSHI']
    # higgs type
    SUSHI1.entries[2]=h_i
    # COM energy
    SUSHI1.entries[4]=13000.0
    # order ggh, order bbh
    SUSHI1.entries[5]=2
    SUSHI1.entries[6]=2
    new_slha = pyslha2.writeSLHA(blocks,decays)
    lines = new_slha.splitlines()
    for l in range(len(lines)):
        if lines[l].startswith("BLOCK"):
            lines[l] = lines[l].replace("BLOCK", "Block")
        else:
            lines[l] = "  " + lines[l]
    new_slha = '\n'.join(lines)
    new_out_lines = RunSusHi.run_SusHi(input_slha, locations)
    # get the return
    new_out = SusHiScan.fetch_result(new_out_lines, results_block_names, results_parameter_numbers)
    return original_slha, original_out_lines, original_out, new_slha, new_out_lines, new_out


base_dir = "/home/henry/Programs/2HDM/"
sushi_dir = base_dir + "PipeSusHi/"
model_type = 1
higgs = 'A'
locations = [sushi_dir + "bin/sushi",
             base_dir + "legacy_junk.txt"]
run = Data.Run(base_dir + "scan_typeI")
input_params = run.datasets["inputs"][0]
input_slha_name = sushi_dir + "example/2HDMC_physicalbasis.in"
with open(input_slha_name, 'r') as slha:
    input_slha = slha.readlines()
    input_slha = [line.encode() for line in input_slha]


original_slha, original_out_lines, original_out, new_slha, new_out_lines, new_out = make_pair(model_type, higgs, input_params, input_slha, locations)

