import subprocess
import os
import InputTools
#from ipdb set_trace as st
import h5py
import numpy as np

def check_bounds(inputs, file_tag, program_path="2HDMC/Rachid_ParamScan"):
    """ Example function. 
    Shows how a call to the cpp program Bounds should go.
    

    Parameters
    ----------
    inputs : numpy array of float
        input paramerters with columns;
           mh_in    
           mH_in    
           mA_in    
           tanb_in  
           mC_in    
           sba_in   
           l6_in    
           l7_in    
           m12_2_in 
           yt_in   
           
    file_tag : str
        name of the file to write to

    program_path : str
        full path to the program bounds
        
    """
    program = np.full((len(inputs), 1), program_path)
    inputs = inputs.astype(str)
    out_file = np.full((len(inputs), 1), file_tag)
    commands = np.hstack((program, inputs, out_file))
    for i, line in enumerate(commands):
        print(line)
        subprocess.call(line)
        if i %1000 ==0 : print(i)


def run():
    #outName=input("Name the file to write to? ")
    out_name="Rachid_ParamScan.csv"
    #program_location = input("Where is Rachid_ParamScan? ")
    program_location = "./Rachid_ParamScan"

    n_pts=100
    mh = np.full((n_pts, 1), 125.)
    mH = np.random.uniform(200., 800., n_pts).reshape((-1, 1))
    mA = mH + 1000.-mH*np.random.uniform(0., 1., n_pts) 
    mA = np.random.uniform(250., 850., n_pts).reshape((-1, 1))
    tanb = np.random.uniform(0.5, 2.5, n_pts).reshape((-1, 1))
    mC = mA
    sba = np.full((n_pts, 1), 1.)
    l6 = np.full((n_pts, 1), 0.)
    l7 = np.full((n_pts, 1), 0.)
    m12_2 = mA*mA*np.sin(np.arctan(tanb))*np.cos(np.arctan(tanb))
    yt = np.full((n_pts, 1), 1)

    inputs = np.hstack((mh, mH, mA, tanb, mC, sba, l6, l7, m12_2, yt))

    check_bounds(inputs, out_name, program_location)


class RachidData:
    def __init__(self, data_file="Rachid_ParamScan.csv"):
        self.columns = ["mh_in",
                        "mH_in",
                        "mA_in",
                        "mC_in",
                        "sba_in",
                        "tanb_in",
                        "m12_2_in",
                        "tot_hbobs",
                        "csqtot",
                        "pval",
                        "BitAllowedStability",
                        "BitAllowedUnitarity",
                        "BitAllowedPerturbativity",
                        "BrhWW",
                        "BrhZZ",
                        "Brhtautau",
                        "Brhss",
                        "Brhbb",
                        "Brhuu",
                        "Brhcc",
                        "Brhgg",
                        "Brhgaga",
                        "BrHWW",
                        "BrHZZ",
                        "BrHtautau",
                        "BrHss",
                        "BrHbb",
                        "BrHuu",
                        "BrHcc",
                        "BrHtt",
                        "BrHHpHm",
                        "BrHhh",
                        "BrHAA",
                        "BrHgg",
                        "BrHgaga",
                        "BrHZA",
                        "BrAtautau",
                        "BrAss",
                        "BrAbb",
                        "BrAuu",
                        "BrAcc",
                        "BrAtt",
                        "BrAgg",
                        "BrAgaga",
                        "BrAZh",
                        "BrAZH",
                        "BrHptb",
                        "BrHpcs",
                        "BrHpcb",
                        "BrHpWh",
                        "BrHpWA",
                        "BrHpWH",
                        "BrHpmunu",
                        "BrHptanu",
                        "S",
                        "T",
                        "U"]
        self.file_contents = np.genfromtxt(data_file)
        
    def get_column(self, column_name=None):
        if column_name is None:
            column_name = InputTools.list_complete("Which column? ", self.columns)
        idx = self.columns.index(column_name.strip())
        return self.file_contents[:, idx]


if __name__ == '__main__':
    if os.path.exists('continue'):
        while os.path.exists('continue'):
            run()
    else:
        print("Running once")
        run()
