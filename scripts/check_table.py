"""From a run make a table of the number of successes per tanb catigory """
import numpy as np
#import debug 
from ipdb import set_trace as st
import excluded_regions, Data, InputTools
import os

AtoZH='A->ZH'
HtoZA='H->ZA'

def make_bins(runs, bin_var_name):
    bin_vars = []
    short_var_name = runs[0].strip_column(bin_var_name)
    for run in runs:
        try:
            bin_vars.append(getattr(run.inputs, short_var_name))
        except AttributeError:
            bin_vars.append(getattr(run.validinputs, short_var_name))
    catigories = sorted(set(np.hstack(bin_vars)))
    catigorial = len(catigories) < 10
    if catigorial:
        all_indices = [{str(cat): bin_var == cat for cat in catigories}
                   for bin_var in bin_vars]
        bin_names = [str(cat) for cat in sorted(catigories)]
    else:
        n_bins = 6
        small = catigories[0]
        large = catigories[-1]
        bin_len = (large-small)/n_bins
        all_indices = [{} for _ in runs]
        bin_names = []
        catigories = []
        for i in range(n_bins):
            low = small + i*bin_len
            high = small + (i+1)*bin_len
            catigories.append((low, high))
            name = f"${low:.1f}<$ {bin_var_name} $\leq {high:.1f}$"
            bin_names.append(name)
            for run_n, bin_var in enumerate(bin_vars):
                all_indices[run_n][name] = np.logical_and(low < bin_var, bin_var < high)
    return bin_names, all_indices

def checks_by_input(run, bin_names, indices):
    """ bin names given as a list to enforce an order"""
    lines = []
    check_names = [name for name in run.infos['checks']['columns']
                   if 'valid' not in name]
    all_checks = {bin_name: np.full(np.sum(indices[bin_name]), True)
                  for bin_name in bin_names}
    formated_check_names = []
    for name in check_names:
        form_name = ' '.join([f"\\rotatebox{{90}}{{{part.capitalize()}}}" for part in name.split()])
        formated_check_names.append(form_name)
        check_values = getattr(run.checks, run.strip_column(name))
        lines.append([])
        for bin_name in bin_names:
            here = check_values[indices[bin_name]]
            all_checks[bin_name] = np.logical_and(all_checks[bin_name],
                                                  here)
            lines[-1].append(np.sum(here)/len(here))
    lines = np.array(lines)
    formated_check_names.append('\\rotatebox{90}{All theory}')
    all_percents = np.fromiter((np.sum(all_checks[bin_name])/len(all_checks[bin_name])
                               for bin_name in bin_names), dtype=float)
    lines = np.vstack((lines, all_percents))
    return formated_check_names, lines.transpose()


def obsexp_by_input(run, bin_names, indices, theory_lines=None, interpolate_kind='other'):
    """ bin names given as a list to enforce an order"""
    predicted, observed, expected = excluded_regions.run_to_interpolation(run, interpolate_kind)
    comparison_names = ["\\rotatebox{90}{of passing theory;} \\rotatebox{90}{Prediction $<$ observed} \\rotatebox{90}{95\\% CL.}",
                        "\\rotatebox{90}{of passing theory;} \\rotatebox{90}{Prediction $>$ expected} \\rotatebox{90}{95\\% CL.}",
                        "\\rotatebox{90}{of passing theory;} \\rotatebox{90}{Pass} \\rotatebox{90}{experiment}"]
    lines = []
    for bin_name in bin_names:
        obs_gt_pred, exp_lt_pred, both = 0, 0, 0
        mH = run.validinputs.mH[indices[bin_name]]
        mA = run.validinputs.mA[indices[bin_name]]
        n_points = len(mH)
        if n_points == 0:
            lines.append([0, 0, 0])
            continue
        tanbeta = run.validinputs.tanbeta[indices[bin_name]].astype(int)
        for tb in set(tanbeta):
            mH_here = mH[tanbeta == tb]
            mA_here = mA[tanbeta == tb]
            preds = np.fromiter((predicted[tb, [h], [a]][0] for h, a
                                 in zip(mH_here, mA_here)), dtype=float)
            obs = np.fromiter((observed[tb, [h], [a]][0] for h, a
                               in zip(mH_here, mA_here)), dtype=float)
            exp = np.fromiter((expected[tb, [h], [a]][0] for h, a
                               in zip(mH_here, mA_here)), dtype=float)
            obs_passes = obs > preds
            exp_passes = exp < preds
            obs_gt_pred += np.sum(obs_passes)
            exp_lt_pred += np.sum(exp_passes)
            both += np.sum(np.logical_and(obs_passes, exp_passes))
        lines.append([obs_gt_pred/n_points, exp_lt_pred/n_points, both/n_points])
    lines = np.array(lines)
    if theory_lines is not None:  # then we can calculate overall premitted and replace path both
        comparison_names[-1] = "\\rotatebox{90}{Pass theory} \\rotatebox{90}{+ experiment}"
        all_theory = theory_lines[:, -1]
        lines[:, -1] *= all_theory
    return comparison_names, lines


def form_grid(bin_var_name, fraction_run, cross_run=None):
    all_runs = [fraction_run]
    if cross_run is not None:
        all_runs.append(cross_run)
    bin_names, all_indices = make_bins(all_runs, bin_var_name)
    check_names, lines = checks_by_input(fraction_run, bin_names, all_indices[0])
    if cross_run is not None:
        obsexp_names, obsexp_lines = obsexp_by_input(cross_run, bin_names, all_indices[1], lines)
        check_names += obsexp_names
        lines = np.hstack((lines, obsexp_lines))
    grid = [[bin_var_name] + check_names]
    for y, bin_name in enumerate(bin_names):
        grid.append([bin_name])
        for x, check_name in enumerate(check_names):
            if x == len(check_names) -1:
                grid[y+1].append(f"{lines[y][x]:.05g}")
            else:
                grid[y+1].append(f"{lines[y][x]:.03f}")
    return np.array(grid)
        

latex_begining = \
"\\documentclass{article}\n\
\\usepackage{graphicx}\n\
\\usepackage{placeins}\n\
\\usepackage{geometry}\n\
\\newgeometry{vmargin={25mm}, hmargin={30mm,30mm}}\n\
\\usepackage{listings}\n\
\\lstset{basicstyle=\\ttfamily}\n\
\\let\\vec\\mathbf\n\
\\def\\AZH{\\(A \\rightarrow ZH\\)}\n\
\\def\\HZA{\\(H \\rightarrow ZA\\)}\n\
\\def\\UFO{\\lstinline{2HDM_UFO}}\n\
\\def\\SM{\\lstinline{SM}}\n\
\\def\\tII{\\lstinline{2HDMtII_NLO}}\n\
\\begin{document}\n\
\\title{Permitted phase space}\n\
    \\author{Henry Day-Hall}\n\
    \\maketitle\n\
"
latex_end = "\\end{document}"

def latex_table(fraction_run, cross_run=None, file_name=None):
    lines = latex_begining
    info = fraction_run.infos['inputs']
    bin_columns = info['columns']
    lines += "Scan has parameters;\n"
    rename_dict = {'mins':'Minimum values of $m_H$ and $m_A$',
                   'maxes':'Maximum values of $m_H$ and $m_A$',
                   'step': '$m_H$ and $m_A$ grid step size',
                   'mC': 'The mass of the charged higgs is fixed to',
                   'tan(beta)': 'Values of $tan(\\beta)$ used are',
                   'num_tries': 'Random tests of $m_{12}^2$ at each $m_A$ and $m_H$ value',
                   'm12_2': '$m_{12}^2$ is chosen to be',
                   'min(mA-mH)': '$min(m_A - m_H)$'}
    skip_keys = ['dtype', 'part', 'columns', 'm12_2_step']
    lines += '\\begin{itemize}\n'
    for key in info:
        if key in skip_keys:
            continue
        param = rename_dict.get(key, key.capitalize())
        lines += f"\t\item {param}; {info[key]}.\n"
    lines += '\\end{itemize}\n'
    lines += "\\begin{flushright}\n"
    for bin_var_name in bin_columns:
        if bin_var_name == '$m_C$':
            continue
        grid = form_grid(bin_var_name, fraction_run, cross_run)
        n_columns = grid.shape[1]
        lines += "\\begin{tabular}{|" + "c|"*n_columns + "}\n"
        lines += "\\hline \n"
        for row in grid:
            lines += ' & '.join(row)
            lines += " \\\\\n"
        lines += "\\hline \n"
        lines += "\\end{tabular}\n"
    lines += "\\end{flushright}\n"
    lines += latex_end
    if file_name is not None:
        with open(file_name, 'w') as latex_file:
            latex_file.write(lines)
    return lines

def main():
    frun_name = InputTools.get_dir_name("Fraction run? ")
    if InputTools.yesNo_question("Have cross sections? "):
        crun_name = InputTools.get_dir_name("Cross run? ")
    else:
        crun_name = None
        crun = None
    out_name = InputTools.get_file_name("Output name? ", file_ending='.tex')
    frun = Data.Run(frun_name)
    if crun_name is not None:
        crun = Data.Run(crun_name)
    latex_table(frun, crun, out_name)

if __name__ == '__main__':
    settings_name = "check_table_settings.dat"
    types = np.arange(4, 5)
    for i in types:
        InputTools.pre_selections = InputTools.PreSelections(settings_name)
        print(f"type{i}")
        InputTools.pre_selections.replace_string("type.", f"type{i}")
        main()
    #InputTools.last_selections.write(settings_name)

