""" plot the excluded regions """
# make fstrings work in older python versions
# -*- coding: future_fstrings -*-
import awkward
from ipdb import set_trace as st
import Data, PlotFramework, InputTools
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import os
import scipy.interpolate
import scipy.stats
import scipy.spatial
import sklearn.gaussian_process as gp
import debug
import coloured_hatching_contourf

AtoZH='A->ZH'
HtoZA='H->ZA'
custom='custom'
checks='checks'
interp='interp'
cls='CLs'
obs_cls='Obs_CLs'
exp_cls='Exp_CLs'
annotations='annotations'
colours = {tb: matplotlib.colors.to_rgb(c) for tb, c in 
           zip([1, 2, 3, 5, 10, 20], ['lime', 'purple', 'mediumspringgreen', 'blue', 'orangered', 'crimson'])}
hatching_options = ('/', '\\', '|', '-', 'o', 'O', '.', '+', '*')



def gen_flat_interpolator(xs, ys, zs, fill_value):
    """
    

    Parameters
    ----------
    xs :
        
    ys :
        
    zs :
        
    fill_value :
        
    Returns
    -------

    """
    sorted_xs = np.array(sorted(xs))
    sorted_ys = np.array(sorted(ys))
    x_seperation = np.max(sorted_xs[1:] - sorted_xs[:-1])
    y_seperation = np.max(sorted_ys[1:] - sorted_ys[:-1])
    def interpolate(points):
        """
        

        Parameters
        ----------
        points :
            

        Returns
        -------

        """
        if hasattr(points[0], '__iter__'):
            out = [interpolate(point) for point in points]
            return np.array(out)
        x, y = points.T
        # need to locate closest indixes for x and y seperatly
        closest_xy = np.argmin(np.abs(xs-x) + np.abs(ys-y))
        x_out = xs[closest_xy]
        if np.abs(x_out-x) > x_seperation:
            return fill_value
        y_out = ys[closest_xy]
        if np.abs(y_out-y) > y_seperation:
            return fill_value
        return zs[closest_xy]
    return interpolate


def gen_gaussianProcess_interpolator(points, zs):
    """
    Create an interpolator that uses gaussian process.

    Parameters
    ----------
    points : 2d numpy array of floats
        the points for which data is avalible.
        
    zs : 1d numpy array of floats
        The values to be interpolated.

    Returns
    -------
    interpolate : callable
        A function that can interpolate between the values given

    """
    print("Fitting a gaussian process")
    # get an apropreate scale
    sorted_xs = np.array(sorted(points[:, 0]))
    sorted_ys = np.array(sorted(points[:, 1]))
    scale = np.array([np.max(sorted_xs[1:] - sorted_xs[:-1]), np.max(sorted_ys[1:] - sorted_ys[:-1])])
    kernel = gp.kernels.ConstantKernel(1., (0.1, 1000)) * gp.kernels.RBF(scale*0.2)
    model = gp.GaussianProcessRegressor(kernel=kernel, n_restarts_optimizer=2, alpha=0.01)
    model.fit(points, zs)
    return model.predict


class Interpolated_Table:
    """ """
    def __init__(self, tanb, mH, mA, values, fill_value, kind='other', name=None, masks=None):
        if name is None:
            self.name = ''
        else:
            self.name = name
        assert len(tanb) == len(values), "should be a set of values for each tan beta"
        order = np.argsort(tanb)
        self.tanb = np.array(tanb)[order]
        # values may be provided as a 2d grid for each tanbeta or
        if hasattr(values[0][0], '__iter__'):
            new_values = []
            for i in range(len(tanb)):
                values.append(values[i].flatten())
        else:
            new_values = list(values)
        # mH and mA may be provided per tanb or for all tanb
        if not hasattr(mH[0], '__iter__'):
            assert not hasattr(mA[0], '__iter__')
            # these are the same for all tanbeta
            if len(mH) != len(values[0]):
                # thses are grid coordinates
                assert len(mA) != len(values[0])
                # make them a full length list
                mH, mA = np.meshgrid(mH, mA)
                mH = mH.flatten()
                mA = mA.flatten()
            new_mH = np.tile(mH, (len(tanb), 1))
            new_mA = np.tile(mA, (len(tanb), 1))
        else: # it is possible that all lists differ in length
            # check the length of each list matches the length of the values
            new_mH, new_mA = [], []
            for i, vals in enumerate(values): 
                if len(mH[i]) != len(vals):
                    assert len(mA[i]) != len(vals)
                    mH_here, mA_here = np.meshgrid(mH[i], mA[i])
                    new_mH.append(mH_here.flatten())
                    new_mA.append(mA_here.flatten())
                else:
                    new_mH.append(mH[i])
                    new_mA.append(mA[i])
        # remove nan
        for i, tb in enumerate(self.tanb):
            mask = ~np.isnan(values[i])
            new_mH[i] = new_mH[i][mask]
            new_mA[i] = new_mA[i][mask]
            new_values[i] = new_values[i][mask]
        self.values = awkward.fromiter(new_values)[order]
        self._mH = awkward.fromiter(new_mH)[order]
        self._mA = awkward.fromiter(new_mA)[order]
        self.fill_value = fill_value
        self.kind = kind
        self._epsilon = 0.1
        self.__interpolators = [self.__create_interpolaton(i) for i, tb in enumerate(self.tanb)]
        self.__hulls = [self.__create_convexHull(i) for i, tb in enumerate(self.tanb)]
        #self.masks = masks[order]
        self.max_mH = np.max(self._mH.flatten().tolist())
        self.max_mA = np.max(self._mA.flatten().tolist())
        self.min_mH = np.min(self._mH.flatten().tolist())
        self.min_mA = np.min(self._mA.flatten().tolist())

    def __create_convexHull(self, i):
        xs = self._mH[i]
        ys = self._mA[i]
        points = np.vstack((xs, ys)).T
        convex_hull = scipy.spatial.ConvexHull(points)
        hull = scipy.spatial.Delaunay(convex_hull.points[convex_hull.vertices])
        return hull

    def __create_interpolaton(self, i):
        xs = self._mH[i].astype(int)
        ys = self._mA[i].astype(int)
        zs = self.values[i]
        points = np.vstack((xs, ys)).T
        if self.kind == 'flat':
            interpolator = gen_flat_interpolator(xs, ys, zs, self.fill_value)
        elif self.kind == 'thin-plate':
            interpolator = scipy.interpolate.Rbf(xs, ys, zs, smooth=10, function=self.kind)
        elif self.kind=='rectbivar':
            # this one needs a diferent format
            x_range = np.sort(list(set(xs)))
            y_range = np.sort(list(set(ys)))
            grid_zs = np.full((len(y_range), len(x_range)),
                              self.fill_value, dtype=float)
            y_range_list = y_range.tolist()
            x_range_list = x_range.tolist()
            for x, y, z in zip(xs, ys, zs):
                if np.isnan(z):
                    continue
                grid_zs[y_range_list.index(y), x_range_list.index(x)] = z
            interpolator = scipy.interpolate.RectBivariateSpline(x_range, y_range, grid_zs, s=0)
        elif self.kind == 'rbf':
            interpolator = scipy.interpolate.Rbf(xs, ys, zs, smooth=0, norm='cityblock')
        elif self.kind == 'linear':
            interpolator = scipy.interpolate.LinearNDInterpolator(points, zs, fill_value=self.fill_value)
        elif self.kind == 'gaussian_process':
            interpolator = gen_gaussianProcess_interpolator(points, zs)
        else:
            interpolator = scipy.interpolate.CloughTocher2DInterpolator(points, zs, fill_value=self.fill_value)
        return interpolator

    def __check_mask(self, tanb_idx, mH, mA):
        if self.masks is None:
            return np.full_like(mH, True)
        mask = self.masks[tanb_idx]
        position = "TODO"

        

    def __getitem__(self, position):
        tanb, mH, mA = position
        batch_dimension = 150
        all_rows = []
        for mA_batch_start in range(0, len(mA), batch_dimension):
            mA_row = []
            mA_batch = mA[mA_batch_start: mA_batch_start + batch_dimension]
            for mH_batch_start in range(0, len(mH), batch_dimension):
                mH_batch = mH[mH_batch_start: mH_batch_start + batch_dimension]
                batch = self.__get_batch(tanb, mH_batch, mA_batch)
                mA_row.append(batch)
            all_rows.append(np.hstack(mA_row))
        output = np.vstack(all_rows)
        return output

    def __get_batch(self, tanb, mH, mA):
        mesh_mH, mesh_mA = np.meshgrid(mH, mA)
        points = np.vstack((mesh_mH.flatten(), mesh_mA.flatten())).T
        outputs = np.full(len(points), self.fill_value, dtype=float)
        tanb_idx = np.argmin(np.abs(self.tanb - tanb))
        if np.abs(self.tanb[tanb_idx] - tanb) > self._epsilon:
            raise ValueError("Provided value of tanb ({}) not in interpolated table (contains {})".format(tanb, self.tanb.tolist()))
        hull = self.__hulls[tanb_idx]
        inside = hull.find_simplex(points) >= 0
        if not np.any(inside):
            return outputs.reshape((len(mA), len(mH)))
        if self.kind in ['thin-plate', 'rbf']:
            outputs = self.__interpolators[tanb_idx](mesh_mH, mesh_mA)
            inside = inside.reshape((len(mA), len(mH)))
            outputs[~inside] = self.fill_value
        elif self.kind == 'rectbivar':
            foo = self.__interpolators[tanb_idx]
            outputs = self.__interpolators[tanb_idx](mH, mA)
            inside = inside.reshape((len(mA), len(mH)))
            outputs[~inside] = self.fill_value
        else:
            outputs[inside] = self.__interpolators[tanb_idx](points[inside])
        return outputs.reshape((len(mA), len(mH)))


def sort_hepdata(process, dir_name, file_names, interpolate_kind, skip_tb, multiplier=None, selective=False):
    """
    

    Parameters
    ----------
    process :
        
    dir_name :
        
    file_names :
        
    interpolate_kind :
        
    multiplier :
         (Default value = None)

    Returns
    -------

    """
    assert process in [AtoZH, HtoZA]
    tanb = [float(name.split('.', 1)[0].split('tanb')[1]) for name in file_names]
    # go through the file names removeing tb values we don't want 
    skip_tb = np.array(skip_tb)
    tanb = []
    keep_names = []
    for name in file_names:
        tb = float(name.split('.', 1)[0].split('tanb', 1)[1])
        if np.min(np.abs(skip_tb - tb), initial=1.) < 0.1:
            continue
        tanb.append(tb)
        keep_names.append(name)
    file_names = keep_names
    obs_tbl, exp_tbl = read_hepdata_file(dir_name, file_names[0])
    original_mH = obs_tbl[:, 0]
    original_mA = obs_tbl[:, 1]
    np.testing.assert_allclose(original_mH, exp_tbl[:, 0])
    np.testing.assert_allclose(original_mA, exp_tbl[:, 1])
    obs = [obs_tbl[:, 2].tolist()]
    exp = [exp_tbl[:, 2].tolist()]
    for name in file_names[1:]:
        obs_tbl, exp_tbl = read_hepdata_file(dir_name, name)
        np.testing.assert_allclose(original_mH, obs_tbl[:, 0])
        np.testing.assert_allclose(original_mA, obs_tbl[:, 1])
        np.testing.assert_allclose(original_mH, exp_tbl[:, 0])
        np.testing.assert_allclose(original_mA, exp_tbl[:, 1])
        obs.append(obs_tbl[:, 2].tolist())
        exp.append(exp_tbl[:, 2].tolist())
    if process == AtoZH:
        mH = original_mH
        mA = original_mA
    elif process == HtoZA:
        mH = original_mA
        mA = original_mH
    if multiplier == '14TeV':
        print("Boosting the expected")
        # multiply the bounds by a *factor*
        # for energy change
        #factor = 1.114  # cross section at 14/ cross section at 13
        # Background only boost - our signal can be calculated directly at 14TeV
        factor = 1.114  # at a higher energy is there is more background so we cannot observe smaller cross sections
        # for lumniocity change
        factor *= 36.1/300  # at higher luminocity there is more staatistics so we can observed smaller cross sections
    else:
        # for energy change
        factor = 1.
    obs = np.array(obs)
    exp = np.array(exp)*factor
    if not selective:
        observables = Interpolated_Table(tanb, mH, mA, obs, 1, kind=interpolate_kind)
        expected = Interpolated_Table(tanb, mH, mA, exp, 1, kind=interpolate_kind)
    elif 'obs' in selective.lower():
        observables = Interpolated_Table(tanb, mH, mA, obs, 1, kind=interpolate_kind)
        expected = None
    elif 'exp' in selective.lower():
        observables = None
        expected = Interpolated_Table(tanb, mH, mA, exp, 1, kind=interpolate_kind)
    return observables, expected


def read_hepdata_file(dir_name, file_name):
    """
    

    Parameters
    ----------
    dir_name :
        
    file_name :
        

    Returns
    -------

    """
    tables = []
    in_table=False
    delim = ','
    with open(os.path.join(dir_name, file_name), 'r') as data_file:
        for line_n, line in enumerate(data_file):
            is_numeric = line[0].isdigit()
            if in_table:
                if is_numeric:
                    processed_line = [float(x) for x in line.split(delim)]
                    tables[-1].append(processed_line)
                else:
                    tables[-1] = np.array(tables[-1])
                    in_table = False
                    #print(f"In file {file_name} table {len(tables)}")
                    #print(f"   ends on line {line_n}")
                    #print(f"   '{line}'")
            else:
                if is_numeric:
                    in_table = True
                    processed_line = [float(x) for x in line.split(delim)]
                    tables.append([processed_line])
                    #print(f"In file {file_name} table {len(tables)}")
                    #print(f"   starts on line {line_n}")
                    #print(f"   '{line}'")
    return np.array(tables)


def sort_rundata(process, run, interpolate_kind, skip_tb, theory_checks=False):
    """
    

    Parameters
    ----------
    process :
        
    run :
        
    interpolate_kind :
        
    theory_checks :
         (Default value = False)

    Returns
    -------

    """
    if isinstance(run, str):
        run = Data.Run(run)
    # get predicted values from the run 
    tanb = run.uniqueinputs.tanbeta
    tanb_records = list(set(tanb))
    skip_tb = np.array(skip_tb)
    tanb_records = [tb for tb in tanb_records if np.min(np.abs(skip_tb - tb), initial=1.) > 0.1]
    assert len(tanb_records) < 5
    mH = run.uniqueinputs.mH
    mA = run.uniqueinputs.mA
    if process == AtoZH:
        brXbb = run.brs_hH.BRHTObb
        brYZX = run.brs_hA.BRATOZH
        nbrXbb = run.n_brs_hH.BRHTObb
        nbrYZX = run.n_brs_hA.BRATOZH
        try:
            CSppY = run.crossSections.nnlosigmappTOApb
            nCSppY = run.n_crossSections.nnlosigmappTOApb
        except (KeyError, AttributeError):
            run.make_pp()
            run.write()
            CSppY = run.crossSections.nnlosigmappTOApb
            nCSppY = run.n_crossSections.nnlosigmappTOApb
        all_predictions = CSppY*brXbb*brYZX
        name = ' '.join([nCSppY, nbrYZX, nbrXbb])
    elif process == HtoZA:
        brXbb = run.brs_hA.BRATObb
        brYZX = run.brs_hH.BRHTOZA
        nbrXbb = run.n_brs_hA.BRATObb
        nbrYZX = run.n_brs_hH.BRHTOZA
        try:
            CSppY = run.crossSections.nnlosigmappTOHpb
            nCSppY = run.n_crossSections.nnlosigmappTOHpb
        except (KeyError, AttributeError):
            run.make_pp()
            run.write()
            CSppY = run.crossSections.nnlosigmappTOHpb
            nCSppY = run.n_crossSections.nnlosigmappTOHpb
        all_predictions = CSppY*brXbb*brYZX
        name = 'x'.join([nCSppY, nbrYZX, nbrXbb])
    elif process == custom:
        options = run.datasets.keys()
        all_chosen = []
        name = ''
        while True:
            print("What do you want to multiply by?")
            choice = InputTools.list_complete("(tab complete, empty to finish): ", options).strip()
            if choice == '':
                break
            else:
                options2 = getattr(run, choice).content.keys()
                choice2 = InputTools.list_complete("(tab complete): ", options2).strip()
            power = InputTools.get_literal("To what exponent? ", int)
            try:
                content = getattr(getattr(run, choice), choice2)
                label = getattr(getattr(run, 'n_' + choice), choice2)
            except IndexError:
                print("Invalid choice, try again")
                continue
            name += label
            if power != 1:
                name += "$^{{{}}}$".format(power)
            name += " x "
            all_chosen.append(content**power)
        all_predictions = np.ones_like(all_chosen[0])
        for component in all_chosen:
            all_predictions *= component
        name = name[:-3]  # remove last multiplication
    else:
        raise ValueError("Don't recognise process " + str(process))
    if theory_checks:  # remove values that don't fit theory
        print("Filtering on theory")
        stab = run.checks.stability
        nStab = run.n_checks.stability
        uni = run.checks.unitarity
        nUni = run.n_checks.unitarity
        per = run.checks.perturbativity
        nPer = run.n_checks.perturbativity
        valid = np.logical_and(stab, np.logical_and(uni, per))
        tanb = tanb[valid]
        mH = mH[valid]
        mA = mA[valid]
        all_predictions = all_predictions[valid]
    # make the predicted values and sort them the same way for each tanb
    full_table = np.vstack((tanb, mH, mA, all_predictions))
    # sort this, first by tanb then mH then mA
    # ensure consistent ordering for all tanb
    full_table = full_table[:, np.lexsort(full_table[[0, 1, 2]])]
    tanb, mH, mA, all_predictions = full_table
    # this assumes that all tanb share smae number of mH/mA
    mH_records = []
    mA_records = []
    predictions = []
    for tb in tanb_records:
        indices = np.isclose(tanb, tb)
        predictions.append(all_predictions[indices])
        mH_records.append(mH[indices])
        mA_records.append(mA[indices])
    predictions = awkward.fromiter(predictions)
    mH_records = awkward.fromiter(mH_records)
    mA_records = awkward.fromiter(mA_records)
    #intr1 =  Interpolated_Table(tanb_records, mH_records, mA_records, predictions, -1, kind=interpolate_kind, name=name)
    #intr2 =  Interpolated_Table(tanb_records, mH_records, mA_records, predictions, -1, kind='flat', name=name)
    #st()
    #o1 = intr1[5., [350., 500.], [240., 250.]]
    #o2 = intr2[5., [350., 500.], [240., 250.]]
    return Interpolated_Table(tanb_records, mH_records, mA_records, predictions, 0, kind=interpolate_kind, name=name)


def visulaise_accuracy(run, interpolate_kind='other'):
    if isinstance(run, str):
        run = Data.Run(run)
    settings = run.infos['uniqueinputs']
    yukawa_type = settings['yukawa']
    process = AtoZH if 'min(mA-mH)' in settings else HtoZA
    predicted = sort_rundata(process, run, interpolate_kind)
    tb_index = -1
    tanbeta = predicted.tanb[tb_index]
    mH = predicted._mH[-1]
    mA = predicted._mA[-1]
    zs = predicted.values[-1]
    n_points = 700
    mH_range = np.linspace(predicted.min_mH, predicted.max_mH, n_points)
    mA_range = np.linspace(predicted.min_mA, predicted.max_mA, n_points)
    prd_grid = predicted[tanbeta, mH_range, mA_range]
    plt.contourf(mH_range, mA_range, prd_grid, levels=10)
    plt.scatter(mH, mA, c=zs, cmap='jet', s=3.2, marker='X', label="Data points")
    plt.xlim(230, 400)
    plt.ylim(130, 300)
    plt.title("Won't wobble")
    plt.legend()
    plt.show()


def run_to_interpolation(run, interpolate_kind='other', apply_skips=True, energy_level=None, selective=False):
    """
    bin names given as a list to enforce an order

    Parameters
    ----------
    run :
        
    interpolate_kind :
         (Default value = 'other')

    Returns
    -------

    """
    if isinstance(run, str):
        run = Data.Run(run)
    settings = run.infos['uniqueinputs']
    yukawa_type = settings['yukawa']
    process = AtoZH if 'min(mA-mH)' in settings else HtoZA
    if energy_level is None:
        energy_level = '14TeV' if '14TeV' in run.dir_name else '13TeV'
    download_dir = "/home/henry/Programs/2HDM/bbll_paper_Data/"
    # now get the hepdata table
    if yukawa_type == 1:
        type_name = 'type-I'
    elif yukawa_type == 2:
        type_name = 'type-II'
    elif yukawa_type == 3:
        type_name ='flipped'
    elif yukawa_type == 4:
        type_name ='leptonspecific'
    # make these into interpolated tables
    all_files = [name for name in os.listdir(download_dir)
                 if 'Cross-sectionlimits'+type_name in name
                 and name.endswith('.csv')]
    if apply_skips:
        skip_tb = run.infos['uniqueinputs']['skip_tanbetas']
    else:
        skip_tb = []
    predicted = sort_rundata(process, run, interpolate_kind, skip_tb)
    observed, expected = sort_hepdata(process, download_dir, all_files, interpolate_kind, skip_tb, multiplier=energy_level, selective=selective)
    return predicted, observed, expected


def make_CL_plots(run, colour_range=(0, 1), interpolate_kind='flat', energy_level=None, selective=False):
    """
    

    Parameters
    ----------
    run :
        
    colour_range :
         (Default value = (0)
    1) :
        
    interpolate_kind :
         (Default value = 'flat')

    Returns
    -------

    """
    if energy_level is None:
        energy_level = '14TeV' if '14TeV' in run.dir_name else '13TeV'
    predicted, observed, expected = run_to_interpolation(run, interpolate_kind, energy_level=energy_level, selective=selective)
    if isinstance(run, str):
        run = Data.Run(run)
    settings = run.infos['uniqueinputs']
    yukawa_type = settings['yukawa']
    process = AtoZH if 'min(mA-mH)' in settings else HtoZA
    #axis_rotate = process==HtoZA and True
    axis_rotate = False
    # now get the hepdata table
    if yukawa_type == 1:
        type_symbol = 'I'
    elif yukawa_type == 2:
        type_symbol = 'II'
    elif yukawa_type == 4:
        type_symbol = 'Lepton Specific'
    elif yukawa_type == 3:
        type_symbol = 'Flipped'
    # start making the plot
    title_txt = "95% CL. exclusion for Type-{} 2HDM".format(type_symbol)
    colour_map = matplotlib.cm.get_cmap('gist_rainbow')
    #colours = [colour_map(x) for x in np.linspace(*colour_range, len(predicted.tanb))]
    if energy_level == '14TeV':
        title_txt += " with boost"
    plt.title(title_txt)
    if expected is not None:
        e_handles, e_labels = plot_contour(predicted, expected, 'expected', energy_level, colours, axis_rotate)
    else:
        e_handles, e_labels = [], []
    if observed is not None:
        o_handles, o_labels = plot_contour(predicted, observed, 'observed', energy_level, colours, axis_rotate)
    else:
        o_handles, o_labels = [], []

    if process == HtoZA and axis_rotate:
        plt.ylabel("$m_H$")
        plt.xlabel("$m_A$")
    else:
        plt.xlabel("$m_H$")
        plt.ylabel("$m_A$")
    plt.xlim(150, 800)
    plt.ylim(150, 800)
    handles = o_handles + e_handles
    labels = o_labels + e_labels
    return handles, labels
        

def plot_contour(predicted, comparitor, name, energy_level, colours, axis_rotate):
    """
    

    Parameters
    ----------
    predicted :
        
    comparitor :
        
    name :
        
    energy_level :
        
    colours :
        
    axis_rotate :
        
    add_legend :
        

    Returns
    -------

    """
    n_points = 100
    hatches = ['//', '\\\\', '||', None, '..']
    mH_range = np.linspace(predicted.min_mH, predicted.max_mH, n_points)
    mA_range = np.linspace(predicted.min_mA, predicted.max_mA, n_points)
    if axis_rotate:
        xs, ys = mA_range, mH_range
    else:
        xs, ys = mH_range, mA_range
    def make_label(tb):
        comp_label = "$tan(\\beta)={}$ {}".format(int(tb), name)
        if energy_level == '13TeV':
            comp_label += ", 13TeV, 36.1 fb$^{-1}$"
        else:
            comp_label += ", 14TeV, 300 fb$^{-1}$"
        return comp_label
    add_legend = True
    handles = []
    labels = []
    if name == 'expected':
        def contour_base(xs, ys, comp_grid, colour, hatching, tb):
            #colour = np.clip(np.array(colour)*2+0.4, 0, 1)
            #alpha = 0.7
            labels.append(make_label(tb))
            #handles.append(matplotlib.patches.Patch(color=colour, hatch=hatching, alpha=alpha))
            #handles.append(matplotlib.patches.Patch(color=colour, alpha=alpha))
            #return plt.contourf(xs, ys, comp_grid,
            #                    antialiased=False, colors=np.array([colour, colour]),
            #                    alpha=alpha, levels=[0., 100.], extend='neither')
            if energy_level == '14TeV':
                linestyle = 'dotted'
            else:
                linestyle = 'dashed'
            handles.append(matplotlib.lines.Line2D([], [], color=colour, linestyle=linestyle))
            return plt.contour(xs, ys, comp_grid, levels=[0.],
                               antialiased=False, colors=np.array([colour]),
                               linestyles=linestyle)
    elif name == 'observed':
        def contour_base(xs, ys, comp_grid, colour, hatching, tb):
            #colour = np.clip(np.array(colour)*1.4 + 0.1, 0, 1)
            labels.append(make_label(tb))
            handles.append(matplotlib.lines.Line2D([], [], color=colour))
            return plt.contour(xs, ys, comp_grid, levels=[0.],
                               antialiased=False, colors=np.array([colour]),
                               linestyles='solid')
    else:
        raise NotImplementedError
    comp_grids = []
    for n, tb in enumerate(predicted.tanb):
        prd_grid = predicted[tb, mH_range, mA_range]
        if name == 'expected':
            # high in the lower left corner, low else where
            # so permitted in the lower left corner
            comp_grid = prd_grid - comparitor[tb, mH_range, mA_range]
        elif name == 'observed':
            # low in the lower left corner, high else where
            # so excluded in the lower left corner
            comp_grid = comparitor[tb, mH_range, mA_range] - prd_grid
        else:
            raise NotImplementedError
        if axis_rotate:
            new_comp_grid = np.zeros_like(comp_grid)
            for i in range(len(comp_grid)):
                for j in range(len(comp_grid[0])):
                    new_comp_grid[i,j] = comp_grid[j,i]
            comp_grid = new_comp_grid
        comp_grids.append(comp_grid)
    # plot the one with greatist area first
    plot_order = np.argsort([np.sum(comp_grid) for comp_grid in comp_grids])[::-1]
    for n in plot_order:
        if np.max(comp_grids[n]) > 0:
            tb = predicted.tanb[n]
            contour_base(xs, ys, comp_grids[n], colours[int(tb)], hatches[n], tb)
    return handles, labels


def make_custom_plots(run, interpolate_kind='flat'):
    """
    

    Parameters
    ----------
    run :
        
    interpolate_kind :
         (Default value = 'flat')

    Returns
    -------

    """
    if isinstance(run, str):
        run = Data.Run(run)
    settings = run.infos['uniqueinputs']
    yukawa_type = settings['yukawa']
    predicted = sort_rundata(custom, run, interpolate_kind)
    str_tanb = [str(tb) for tb in predicted.tanb]
    print("Possibe tanb; {}".format(', '.join(str_tanb)))
    tanb_idx = str_tanb.index(InputTools.list_complete("Which to plot? ", str_tanb).strip())
    # get soem info for titles
    if yukawa_type == 1:
        type_symbol = 'I'
    elif yukawa_type == 2:
        type_symbol = 'II'
    elif yukawa_type == 4:
        type_symbol = 'Lepton Specific'
    elif yukawa_type == 3:
        type_symbol = 'Flipped'
    # start making the plot
    plt.title("Plot of {} for Type-{} 2HDM, tanb={}".format(predicted.name, type_symbol, str_tanb[tanb_idx]))
    colourmap_name =  ['viridis', 'plasma', 'cividis'][np.random.randint(3)]
    colourmap_name = 'viridis'
    white = (1, 1, 1, 0)
    alpha = .6
    n_points = 100
    x_range = np.linspace(predicted.min_mH, predicted.max_mH, n_points)
    y_range = np.linspace(predicted.min_mA, predicted.max_mA, n_points)
    prd_grid = predicted[predicted.tanb[tanb_idx], x_range, y_range]
    ceiling = np.max(prd_grid)
    floor = min(0., np.min(prd_grid))
    levels = np.linspace(floor, ceiling, 100)
    axis_rotate = InputTools.yesNo_question("Mirror on diagonal? ")
    if axis_rotate:
        x_range, y_range = y_range, x_range
        new_prd_grid = np.zeros_like(prd_grid)
        for i in range(len(prd_grid)):
            for j in range(len(prd_grid[0])):
                new_prd_grid[i,j] = prd_grid[j,i]
        prd_grid = new_prd_grid
    contours = plt.contourf(x_range, y_range, prd_grid, levels,
                            antialiased=True, cmap=colourmap_name)
    for cont in contours.collections:
        cont.set_edgecolor("face")
    if axis_rotate:
        plt.ylabel("$m_H$")
        plt.xlabel("$m_A$")
    else:
        plt.xlabel("$m_H$")
        plt.ylabel("$m_A$")
    cbar = plt.colorbar()
    cbar.set_label(predicted.name)


def same_point_indices(*args):
    """ from all the lists of coordinates in args 
    compile lists of indices of points that have the same 
    coordinates """
    key_coords = np.array([str(line) for line in zip(*args)])
    coords = []
    sames = []
    for key in set(key_coords):
        locs = np.where(key_coords == key)[0]
        sames.append(locs)
        coords.append([a[locs[0]] for a in args])
    coords = np.array(coords)
    return coords, sames


def make_grid(xs, ys, values, default=0):
    xs_range = sorted(set(xs))
    ys_range = sorted(set(ys))
    grid = np.full((len(ys_range), len(xs_range)), default)
    for x, y, v in zip(xs, ys, values):
        grid[ys_range.index(y), xs_range.index(x)] = v
    return xs_range, ys_range, grid


def make_exclusion(run, colour_range=(0, 1)):
    """
    

    Parameters
    ----------
    run :
        
    colour_range :
         (Default value = (0)
    1) :
        

    Returns
    -------

    """
    if isinstance(run, str):
        run = Data.Run(run)
    skip_tbs = run.infos['uniqueinputs']['skip_tanbetas']
    mH = run.uniqueinputs.mH.astype(int)
    mA = run.uniqueinputs.mA.astype(int)
    tanb = run.uniqueinputs.tanbeta.astype(int)
    stab = run.uniquechecks.stability
    uni = run.uniquechecks.unitarity
    per = run.uniquechecks.perturbativity
    all_theory = np.logical_and(stab, np.logical_and(uni, per))
    #plt.scatter(mH[all_theory], mA[all_theory], c=tanb[all_theory])
    nall_theory = "Theory forbidden"
    flav = run.uniquechecks.flavourconstraints
    nFlav = run.n_uniquechecks.flavourconstraints
    higgsBounds = run.uniquechecks.HiggsBounds
    nHiggsBounds = run.n_uniquechecks.HiggsBounds
    names = [nall_theory, nFlav, nHiggsBounds]
    masks = [~all_theory, ~flav, ~higgsBounds]
    any_forbidden = np.sum(masks, axis=0).astype(bool)
    set_tanb = sorted(set(tanb))
    tanb_masks = [tanb==tb for tb in set_tanb]
    # set up to hatch the contours
    #hatches = ['//', '\\\\', '||', '..']
    # points to add to legend
    handles = []
    labels = []
    def make_label(tb, mask_name, state=None):
        comp_label = "$tan(\\beta)={}$ {}".format(int(tb), mask_name)
        if state is not None:
            comp_label += " ALL " + state
        return comp_label
    def contour_solid(xs, ys, bool_grid, colour, tb, mask_name):
        alpha = 0.3
        labels.append(make_label(tb, mask_name))
        handles.append(matplotlib.patches.Patch(color=colour, alpha=alpha))
        return plt.contourf(xs, ys, bool_grid,
                            antialiased=False, colors=np.array([colour, colour]),
                            alpha=alpha, levels=[0.5, 100.], extend='neither')
    n_hatch_ops = len(hatching_options)
    def contour_hatch(xs, ys, grid, hatching, hatch_color, tb, mask_name):
        labels.append(make_label(tb, mask_name))
        # make it translucent
        hatch_color = (*hatch_color[:3], 0.5)
        patch = coloured_hatching_contourf.contourf_hatch(xs, ys, grid,
                                           hatches=hatching, hatch_color=hatch_color,
                                           levels=[0.5, 1.5], extend='neither',
                                           antialiased=True)
        handles.append(patch)
    # plot the one with greatist area first
    tb_order = np.argsort([np.sum(any_forbidden[tb_mask]) for tb_mask in tanb_masks])
    for tanb_idx in tb_order:
        tb = set_tanb[tanb_idx]
        tanb_mask = tanb_masks[tanb_idx]
        if tb in skip_tbs:
            continue
        # draw a solid colour
        mask_name = 'forbidden'
        points = any_forbidden[tanb_mask]
        mH_here = mH[tanb_mask]
        mA_here = mA[tanb_mask]
        xs, ys, grid = make_grid(mH_here, mA_here, points)
        contour_solid(xs, ys, grid, colours[int(tb)], tb, mask_name)
        # again apply the largest mask first
        mask_order = np.argsort([np.sum(m[tanb_mask]) for m in masks])
        for mask_idx in mask_order:
            mask_name = names[mask_idx]
            points = masks[mask_idx][tanb_mask]
            if not np.any(points):
                handles.append(matplotlib.patches.Patch(alpha=0))
                labels.append(make_label(tb, mask_name, "ALLOWED"))
            #elif np.all(points):
            #    labels.append(make_label(tb, mask_name, "FORBIDDEN"))
            #    handles.append(matplotlib.patches.Patch(alpha=0))
            else:
                mH_here = mH[tanb_masks[tanb_idx]]
                mA_here = mA[tanb_masks[tanb_idx]]
                xs, ys, grid = make_grid(mH_here, mA_here, points)
                #hatch_index = tanb_idx + len(set_tanb) * mask_idx
                hatch_index = mask_idx
                hatches = hatching_options[hatch_index%n_hatch_ops]*(2+hatch_index//n_hatch_ops)
                contour_hatch(xs, ys, grid, hatches, colours[int(tb)], tb, mask_name)
    return handles, labels


def theory_checks_3D(run, tan_beta, ax=None, show=True):
    from mpl_toolkits.mplot3d import Axes3D
    if ax == None:
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
    if isinstance(run, str):
        run = Data.Run(run)
    tanb_mask = np.abs(run.inputs.tanbeta - tan_beta) < 0.1
    mH = run.inputs.mH[tanb_mask]
    mA = run.inputs.mA[tanb_mask]
    m122 = run.inputs.m122[tanb_mask]
    stab = run.checks.stability[tanb_mask]
    uni = run.checks.unitarity[tanb_mask]
    per = run.checks.perturbativity[tanb_mask]
    all_theory = np.logical_and(stab, np.logical_and(uni, per))
    ax.scatter(mH[all_theory], mA[all_theory], m122[all_theory], marker='o')
    ax.set_xlabel(run.n_inputs.mH)
    ax.set_ylabel(run.n_inputs.mA)
    ax.set_zlabel(run.n_inputs.m122)
    ax.set_title("Type{} $tan(\\beta)={}$, points passing theory checks".format(run.infos['inputs']['yukawa'], tan_beta))
    if show:
        plt.show()


def sort_legend_inputs(labels1, labels2, handles1, handles2):
    # this assumes a lot of summetry and breaks easly
    process1 = AtoZH
    process2 = HtoZA
    new_labels = []
    new_handles = []
    forbidden_string = ' ALL FORBIDDEN'
    allowed_string = ' ALL ALLOWED'
    for i, label in enumerate(labels1):
        if label in labels2 or label + allowed_string in labels2:
            # remove it from 2
            idx_in_2 = next(i for i, label2 in enumerate(labels2) if label2.startswith(label))
            del labels2[idx_in_2]
            del handles2[idx_in_2]
            # add the identical label
            new_labels.append(label)
            new_handles.append(handles1[i])
        elif allowed_string in label:  # just ignore the allowed side
            without_suffix = label.split(' ALL ', 1)[0]
            new_labels.append(without_suffix)
            new_handles.append(handles2[i])
        # giving up on the forbidden regions being ommited
        #elif allowed_string in label:
        #    without_suffix = label.split(' ALL ', 1)[0]
        #    if without_suffix in labels2:
        #        # we can just add the half where it is partially present
        #        idx_in_2 = labels2.index(without_suffix)
        #        label = labels2.pop(idx_in_2)
        #        handle = handles2.pop(idx_in_2)
        #        new_labels.append(label)
        #        new_handles.append(handle)
        #    else:
        #        # both halves need prefixing with the process
        #        new_labels.append(process1 + ' ' +label)
        #        new_handles.append(handles1[i])
        #        idx_in_2 = labels2.index(without_suffix+forbidden_string)
        #        label = process2 + ' ' + labels2.pop(idx_in_2)
        #        handle = handles2.pop(idx_in_2)
        #        new_labels.append(label)
        #        new_handles.append(handle)
        #elif forbidden_string in label:
        #    without_suffix = label.replace(forbidden_string, '')
        #    # both halves need prefixing with the process
        #    try:
        #        idx_in_2 = labels2.index(without_suffix+allowed_string)
        #    except ValueError:
        #        idx_in_2 = labels2.index(without_suffix)
        #    label = process2 + ' ' + labels2.pop(idx_in_2)
        #    handle = handles2.pop(idx_in_2)
        #    new_labels.append(label)
        #    new_handles.append(handle)
        #    new_labels.append(process1 + ' ' +label)
        #    new_handles.append(handles1[i])
        else:  # just add it not prefixed with the process
            new_labels.append(label)
            new_handles.append(handles1[i])
    # add all the remaning process 2 labels, prefiex with the process
    for i, label in enumerate(labels2):
        new_labels.append(label)
        new_handles.append(handles2[i])
    # check if all labels start with smae string and if so move to title
    title_append = ''
    common_text = new_labels[0]
    for label in new_labels:
        i = 0
        try:
            while label[i] == common_text[i]:
                i += 1
            common_text = common_text[:i]
        except IndexError:
            # got to the end of either common text or label
            pass
    if common_text:
        title_append = ', ' + common_text
        new_labels = [label[i:] for label in new_labels]
    return new_labels, new_handles, title_append
        

import matplotlib.text as mtext
import matplotlib.transforms as mtransforms


class RotationAwareAnnotation(mtext.Annotation):
    def __init__(self, s, location, gradient, ax=None, **kwargs):
        self.ax = ax or plt.gca()
        self.location = location
        self.gradient = gradient
        self.calc_angle_data()
        kwargs.update(rotation_mode=kwargs.get("rotation_mode", "anchor"))
        mtext.Annotation.__init__(self, s, location, **kwargs)
        self.set_transform(mtransforms.IdentityTransform())
        if 'clip_on' in kwargs:
            self.set_clip_path(self.ax.patch)
        self.ax._add_text(self)

    def calc_angle_data(self):
        ang = np.arctan2(self.gradient[1], self.gradient[0])
        self.angle_data = np.rad2deg(ang)

    def _get_rotation(self):
        return self.ax.transData.transform_angles(np.array((self.angle_data,)), 
                                                  self.location.reshape((1, 2)))[0]

    def _set_rotation(self, rotation):
        pass

    _rotation = property(_get_rotation, _set_rotation)

def decay_text():
    ax = plt.gca()
    # centre corridor text
    location = np.array([250, 250])
    gradient = np.ones(2)
    text = "Neither decay accessible"
    RotationAwareAnnotation(text, location, gradient, fontsize=12)
    # A->HZ
    text = "$A\\rightarrow H Z$"
    plt.text(250, 600, text, fontsize=12, bbox=dict(facecolor='white', alpha=0.6, edgecolor=(0.5, 0.5, 0.5, 0.8), boxstyle='round'))
    # H->AZ
    text = "$H\\rightarrow A Z$"
    plt.text(500, 250, text, fontsize=12, bbox=dict(facecolor='white', alpha=0.6, edgecolor=(0.5, 0.5, 0.5, 0.8), boxstyle='round'))


def make_single_tb_plots():
    for type_n in range(1, 5):
        if type_n == 4:
            tanb = [1., 2., 3.]
        else:
            tanb = [1., 5., 10., 20.]
        for show_value in tanb:
            InputTools.pre_selections = InputTools.PreSelections("highE_settings.dat")
            str_type = "type{}".format(int(type_n))
            str_img_suffix = "tb{}.png".format(int(show_value))
            InputTools.pre_selections.replace_string('type.', str_type)
            InputTools.pre_selections.replace_string('tb.+png', str_img_suffix)
            skip_values = [tb for tb in tanb if tb != show_value]
            skip_message = "Please list new tanbetas to skip (e.g. '[1., 20.]'); "
            InputTools.pre_selections.replace_all_answers(skip_message, skip_values)
            try:
                main()
            except Exception:
                print("tanb = {} type={type_n} failed".format(tanb))
                plt.clf()


def main(show=False):
    """ """
    interpolate_kind = InputTools.list_complete("How to interpolate? ", ['gaussian_process', 'flat', 'thin-plate', 'other', 'rbf', 'linear']).strip()
    handles1, handles2, labels1, labels2 = [], [], [], []
    run_dir = ''
    while InputTools.yesNo_question("Add layer? "):
        #energy_level = InputTools.list_complete("What energy? ", ["13TeV", "14TeV"]).strip()
        new_run_dir = InputTools.get_dir_name("Where is the run directory; ")
        if new_run_dir != run_dir:
            run_dir = new_run_dir
            run = Data.Run(run_dir)
            # check if pp has been calculated
            try:
                next(col for col in run.infos['crossSections']['columns'] if 'p p' in col)
            except StopIteration:
                run.make_pp()
                run.write()
            if not hasattr(run.crossSections, 'nnlosigmappTOApb'):
                st()
        process = InputTools.list_complete("Which process? ", [cls, custom, checks, interp, annotations]).strip()
        if 'min(mA-mH)' in run.infos['uniqueinputs']:
            # its A-> HZ
            handles = handles1
            labels = labels1
        else: # its H->AZ
            handles = handles2
            labels = labels2
        skip_tb_name = 'skip_tanbetas'
        update_skips = False  # don't do it unless some situation call for it
        if skip_tb_name in run.infos['uniqueinputs']:
            current_skips = run.infos['uniqueinputs'][skip_tb_name]
        else:
            update_skips = True
            current_skips = []
            new_skips = []
        replace_skips = InputTools.yesNo_question("Current {} is {}, change? ".format(skip_tb_name, current_skips), consistant_length=8)
        if replace_skips:
            new_skips = InputTools.get_literal("Please list new tanbetas to skip (e.g. '[1., 20.]'); ")
            update_skips = update_skips or new_skips != current_skips
        if update_skips:
            run.infos['uniqueinputs'][skip_tb_name] = new_skips
            run.write()
        if process == custom:
            make_custom_plots(run, interpolate_kind=interpolate_kind)
        elif process == cls:
            energy_level = InputTools.list_complete("What is the energy level? (default 13TeV) ", ['13TeV', '14TeV']).strip()
            if energy_level == '':
                energy_level = '13TeV'
            selective = InputTools.list_complete("Use only obs or only exp? (default; use both) ", ['obs', 'exp']).strip()
            if selective == '':
                selective = False

            these_handles, these_labels = make_CL_plots(run, interpolate_kind=interpolate_kind, energy_level=energy_level, selective=selective)
            for i, label in enumerate(these_labels):
                if label not in labels:
                    handles.append(these_handles[i])
                    labels.append(label)
        elif process == checks:
            these_handles, these_labels = make_exclusion(run)
            for i, label in enumerate(these_labels):
                if label not in labels:
                    handles.append(these_handles[i])
                    labels.append(label)
        elif process == interp:
            visulaise_accuracy(run, interpolate_kind)
        elif process == annotations:
            decay_text()
        else:
            print('Not a valid process')
    fig = plt.gcf()
    fig.set_size_inches(7, 7)
    if labels1 and not labels2:
        all_labels = labels1
        all_handles = handles1
    elif labels2 and not labels1:
        all_labels = labels2
        all_handles = handles2
    else:
        all_labels, all_handles, title_append = sort_legend_inputs(labels1, labels2, handles1, handles2)
        ax = plt.gca()
        title = ax.get_title() + title_append
        plt.title(title)
    legend = plt.legend(handles=all_handles, labels=all_labels, loc='upper right')
    save_name = InputTools.get_file_name("What to save the figure as? ").strip()
    if save_name:
        try:
            fig.savefig(save_name)
        except Exception as e:
            print("Couldn't save {} becuase {}".format(save_name), e)
    if legend:
        legend.set_draggable(True)
    if show:
        plt.show()
    else:
        plt.clf()
    InputTools.last_selections.write("previous_settings.dat")


if __name__ == '__main__':
    #InputTools.pre_selections = InputTools.PreSelections("highE_settings.dat")
    #main(True)
    make_single_tb_plots()
else:
    print("Im imported")
