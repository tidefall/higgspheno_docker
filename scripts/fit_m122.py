""" module to fit m122 for each tanb and type, should result in two surrfaces, the upper and the lower premitted m122 for each mH, mA """
import numpy as np
from matplotlib import pyplot as plt
import itertools
import InputTools
from ipdb import set_trace as st
import scipy.interpolate
import scipy.optimize
import os
import ast

AtoHZ = 'AtoHZ'
HtoAZ = 'HtoAZ'

# don't bother, we don't really need to know
def mAmH_decision(run, tanbeta):
    print("Gathing data")
    stab = run.checks.stability
    uni = run.checks.unitarity
    per = run.checks.perturbativity
    mask = np.logical_and(stab, np.logical_and(uni, per))
    sign = 2*mask.astype(int) - 1
    mH = run.inputs.mH
    mA = run.inputs.mA
    points = np.vstack((mH, mA)).T
    def cost_functon(mA_intercept, grad):
        # make 2 points on the decision boundary
        decision1 = np.array([0, mA_intercept])
        decision2 = np.array([1, mA_intercept + grad])



class Curves:
    _inputs_order = ['m122', 'mH', 'mA', 'mC', 'tanbeta']
    _checks_order = ['stability', 'unitarity', 'perturbativity']
    _sep = ' '
    _recorded_file = "recorded.dat"
    def __init__(self, save_path=None, order=2):
        self.type = None
        self.tanbeta = None
        self.save_path = save_path
        self.recorded_path = os.path.join(save_path, self._recorded_file)
        if not os.path.exists(save_path):
            if InputTools.yesNo_question("Create CoreCurves at {}? ".format(save_path)):
                os.mkdir(save_path)
            else:
                raise FileNotFoundError
        try:
            with open(self.recorded_path, 'r') as rfile:
                self.recorded = rfile.readlines()
        except FileNotFoundError:
            self.recorded = []
        self.__order = order

    def _file_name(self, process, yukawa, tanbeta=None, coefficents=False):
        base_name = os.path.join(self.save_path, "type{}_{}".format(yukawa, process))
        if tanbeta is not None:
            base_name += "_tanb{}".format(int(tanbeta))
        if coefficents:
            base_name += "_coeff"
        base_name += ".dat"
        return base_name

    def _file_parameters(self, file_name):
        base_name = file_name.split('.', 1)[0]
        is_coeff = base_name.endswith('_coeff')
        if is_coeff:
            base_name = base_name[:-len('_coeff')]
        if 'tanb' in base_name:
            base_name, tanbeta = base_name.split('_tanb', 1)
            tanbeta = int(tanbeta)
        else:
            tanbeta = None
        yukawa, process = base_name.split('_', 1)
        yukawa = int(yukawa.lstrip('type'))  # safe becuase we know the type is an integer
        return is_coeff, process, yukawa, tanbeta

    def _write_coeffients(self, coefficient_order, coefficients, process, yukawa, tanbeta=None):
        file_name = self._file_name(process, yukawa, tanbeta, coefficents=True)
        coefficient_order = list(coefficient_order)  # sometimes come in as a numpy array
        header = "Coefficient format = {}\n".format(coefficient_order)
        # each check has 3 curves fitted, one through the center of the valid region, one at the lower limit one at the upper limit
        assert len(coefficients) == len(self._checks_order)*3
        np.savetxt(file_name, coefficients, delimiter=self._sep, header=header)

    def _write_points(self, points, process, yukawa, tanbeta=None):
        file_name = self._file_name(process, yukawa, tanbeta, coefficents=False)
        header = "Points\n"
        continuous_tanbeta = points.shape[1] == len(self._inputs_order + self._checks_order)
        if continuous_tanbeta:
            column_order = self._inputs_order + self._checks_order
        else:
            column_order = self._inputs_order[:-1] + self._checks_order
        header += "Columns = {}".format(column_order)
        np.savetxt(file_name, points, delimiter=self._sep, header=header)

    def _read_coefficients(self, check, position, process, yukawa, tanbeta=None):
        file_name = self._file_name(process, yukawa, tanbeta, coefficents=True)
        with open(file_name, 'r') as data_file:
            # check the format line
            format_line = data_file.readline()
            found_format = ast.literal_eval(format_line.split('=', 1)[1].strip())
            variables = self._inputs_order[1:] if self.tanbeta is not None else self._inputs_order[1:-1]
            expected_format = self.variable_terms(variables).tolist()
            could_match = [sorted(here) == sorted(found) for here, found in zip(expected_format, found_format)]
            if len(expected_format) != len(found_format) or not np.all(could_match):
                print("expected polynomial format is {}, but format line is {} indicating format {}".format(expected_format, format_line, found_format))
                st()
        coefficients = np.loadtxt(file_name, delimiter=self._sep)
        check_n = self._checks_order.index(check)
        position_n = ["upper", "centre", "lower"].index(position)
        row = check_n*3 + position_n
        return found_format, coefficients[row]
    
    def _read_points(self, process, yukawa, tanbeta=None):
        file_name = self._file_name(process, yukawa, tanbeta, coefficents=False)
        if os.path.exists(file_name):
            points = np.loadtxt(file_name, delimiter=self._sep)
        else:  # this one should still work even if the file has yet to be created
            n_cols = len(self._inputs_order + self._checks_order)
            n_cols -= '_tanb' in file_name
            points = np.empty((0, n_cols), dtype=float)
        return points

    def _already_recorded(self, run):
        string = run.dir_name.split(os.path.sep)[-1] + os.linesep
        return string in self.recorded
        with open(self.recorded_path, 'w') as rfile:
            rfile.writelines(self.recorded)

    def _add_recorded(self, run):
        string = run.dir_name.split(os.path.sep)[-1] + os.linesep
        self.recorded.append(string)

    def add_run(self, run, discreet_tanbeta=True, recalculate_coefficients=True):
        if self._already_recorded(run):
            raise RuntimeError("Already recorded run {}".format(run.dir_name))
        yukawa = run.infos['inputs']['yukawa']
        process = AtoHZ if 'min(mA-mH)' in run.infos['inputs'] else HtoAZ
        inputs_in_run = [run.strip_column(name) for name in run.infos['inputs']['columns']]
        inputs_to_use = [inputs_in_run.index(name) for name in self._inputs_order]
        checks_in_run = [run.strip_column(name) for name in run.infos['checks']['columns']]
        checks_to_use = [checks_in_run.index(name) for name in self._checks_order]
        if discreet_tanbeta:
            tanbetas = run.datasets['inputs'][:, inputs_to_use[-1]]
            points = np.hstack((run.datasets['inputs'][:, inputs_to_use[:-1]],
                                run.datasets['checks'][:, checks_to_use]))
            for tanb in set(tanbetas):
                existing_points = self._read_points(process, yukawa, tanb)
                points_here = np.vstack((existing_points, points[tanbetas == tanb]))
                self._write_points(points_here, process, yukawa, tanb)
                if recalculate_coefficients:
                    coefficient_format, coefficients = self.calculate_coefficients(points_here)
                    self._write_coeffients(coefficient_format, coefficients, process, yukawa, tanb)
        else:
            points = np.hstack((run.datasets['inputs'][:, inputs_to_use],
                                run.datasets['checks'][:, checks_to_use]))
            existing_points = self._read_points(process, yukawa)
            points = np.vstack((existing_points, points))
            self._write_points(points, process, yukawa)
            if recalculate_coefficients:
                coefficient_format, coefficients = self.calculate_coefficients(points)
                self._write_coeffients(coefficient_format, coefficients, process, yukawa)
        self._add_recorded(run)

    def calculate_coefficients(self, points=None):
        if points is not None:  # just do the points
            coeffs = []
            for check in self._checks_order:
                coefficient_format, coeffs_here = self._fit_equations(points, check)
                coeffs.append(coeffs_here)
            coeffs = np.vstack(coeffs)
            return coefficient_format, coeffs
        else:  # go through all the files and recaluclate their coefficients
            all_files = os.listdir(self.save_path)
            for name in all_files:
                is_coeff, process, yukawa, tanbeta = self._file_parameters(name)
                if is_coeff:
                    continue  # prevent duplicates
                points = self._read_points(process, yukawa, tanbeta)
                coeffs = []
                for check in self._checks_order:
                    coefficient_format, coeffs_here = self._fit_equations(points, check)
                    coeffs.append(coeffs_here)
                coeffs = np.vstack(coeffs)
                self._write_coeffients(coefficient_format, coeffs, process, yukawa, tanbeta)

    def _fit_equations(self, points, check):
        n_inputs = points.shape[1] - len(self._checks_order)
        check_num = self._checks_order.index(check)
        # skipping the first becuase it's m122
        coefficients_format = self.variable_terms(self._inputs_order[1:n_inputs])
        centre_mask = points[:, n_inputs + check_num] > 0.5
        input_variables = points.T[1:n_inputs]  # should you be clipping of m122 like this??
        centre_terms = self.variable_terms(input_variables[:, centre_mask]) # correct here
        heights = points[:, 0]
        centre, r, rank, s = np.linalg.lstsq(centre_terms, heights[centre_mask], rcond=None)
        centre_heights = self._height(input_variables, centre)
        # split the dataset in two, so that each half has at most one bondary to find
        upper_mask = heights > centre_heights
        if not np.all(centre_mask[upper_mask]):
            st()
            # now get the upper bounds
            upper_terms = self.variable_terms(input_variables[:, upper_mask])
            cost = self._gen_boundary_cost(upper_terms, heights[upper_mask], ~centre_mask[upper_mask])
            upper = scipy.optimize.minimize(cost, centre).x
            #upper = centre * 1.001
            
        else:
            upper = np.full_like(centre, np.nan)
        lower_mask = ~upper_mask
        if not np.all(centre_mask[lower_mask]):
            # now get the lower bounds
            lower_terms = self.variable_terms(input_variables[:, lower_mask])
            cost = self._gen_boundary_cost(lower_terms, heights[lower_mask], centre_mask[lower_mask])
            lower = scipy.optimize.minimize(cost, centre). x
            #lower = centre * 0.999
        else:
            lower = np.full_like(centre, np.nan)
        coefficients = np.vstack((upper, centre, lower))
        return coefficients_format, coefficients

    #TODO not working....
    def _gen_boundary_cost(self, terms, heights, put_above, reg=1e6):
        def cost(coefficients):
            cutoff = 0.1 * (np.max(heights) - np.min(heights))
            sign = 1 - 2*put_above
            #directed_vectors = sign*(np.sum(coefficients * terms, axis=1) - heights)
            directed_vectors = sign*(np.sum(coefficients * terms, axis=1) - terms[:, 1])
            #np.clip(directed_vectors, -cutoff, None)
            #directed_vectors += reg*np.sum(coefficients)
            #directed_vectors = reg*np.sum(np.abs(coefficients))
            return np.sum(directed_vectors)
        return cost

    def variable_terms(self, variables):
        if isinstance(variables[0], str):  # then make the visual representation
            def make_term(things):
                if len(things) == 0:
                    return ''
                term_list = []
                set_things = set(things)
                for p in set_things:
                    counts = sum([q == p for q in things])
                    if counts > 1:
                        term_list.append('{}^{{{}}}'.format(p, counts))
                    else:
                        term_list.append(str(p))
                return '.'.join(term_list)
        else:  #otherwise you wanted numerics
            def make_term(things):
                if len(things) == 0:
                    return np.ones(len(variables[0]))
                term = np.copy(things[0])
                for p in things:
                    term *= p
                return term
        terms = []
        for this_order in range(self.__order+1):
            combinations = itertools.combinations_with_replacement(variables, this_order)
            terms += [make_term(c) for c in combinations]
        terms = np.array(terms).T
        return terms

    def m122(self, process, yukawa, mH, mA, mC, tanbeta):
        variable_tb = hasattr(tanbeta, '__iter__')
        # if th remainign values are floats, make them lists
        if not hasattr(mH, '__iter__'):
            mH = [mH]
        if not hasattr(mA, '__iter__'):
            mA = [mA]
        if not hasattr(mC, '__iter__'):
            mC = [mC]
        names = ['mH', 'mA', 'mC']
        unordered = [mH, mA, mC]
        if variable_tb:
            names.append('tanbeta')
            unordered.append(tanbeta)
        unordered = np.array(unordered)
        inputs = unordered[[names.index(n) for n in self._inputs_order[1:len(names)+1]]]
        upper_bounds = []
        lower_bounds = []
        for check in self._checks_order:
            _, upper = self._read_coefficients(check, 'upper', process, yukawa, tanbeta)
            if np.nan not in upper:
                upper_bounds.append(self._height(inputs, upper))
            _, lower = self._read_coefficients(check, 'lower', process, yukawa, tanbeta)
            if np.nan not in lower:
                lower_bounds.append(self._height(inputs, lower))
        upper_bounds = np.vstack(upper_bounds).T
        lower_bounds = np.vstack(lower_bounds).T
        results = []
        pass_all = []
        # now it is easier to process each point indervidually
        for up, lo in zip(upper_bounds, lower_bounds):
            min_up, max_lo = np.min(up), np.max(lo)
            if min_up > max_lo:
                pass_all.append(True)
                results.append(0.5*(min_up + max_lo))
            else:
                pass_all.append(False)
                # we want to be inside the most checks possible
                stack = [(l, +1) for l in lo] + [(u, -1) for u in up]
                stack = np.array(sorted(stack, key=lambda a: a[0]))
                scores = np.cumsum(stack[:, 1])
                best = min(np.argmax(scores), len(stack)-2)
                results.append(0.5*(stack[best, 0] + stack[best+1, 0]))
        return np.array(results), np.array(pass_all)

    def _height(self, inputs, coefficients):
        variables = self.variable_terms(inputs)
        values = np.sum((coefficients * variables), axis=-1)
        return values

    @property
    def order(self):
        return self.__order
    
    @order.setter
    def order(self, val):
        self.__order = val
        self.calculate_coefficients()

    def plot_fit(self, process, yukawa, tanbeta=None, check=None, scatter=True, ax=None):
        if check is None:
            from mpl_toolkits.mplot3d import Axes3D
            fig = plt.figure()
            ax = fig.add_subplot(111, projection='3d')
            for check in self._checks_order:
                self.plot_fit(process, yukawa, tanbeta, check, False, ax)
            return
        exact = tanbeta is not None
        if tanbeta is None:
            column_order = []
        points = self._read_points(process, yukawa, tanbeta)
        mH = points[:, self._inputs_order.index('mH')]
        mA = points[:, self._inputs_order.index('mA')]
        mC = points[:, self._inputs_order.index('mC')]
        m122 = points[:, self._inputs_order.index('m122')]
        check_idx = len(self._inputs_order) + self._checks_order.index(check) - exact
        check_mask = points[:, check_idx] > 0.5
        if exact:
            eq_str = '='
        else:
            tanbeta = points[:, self._inputs_order.index('tanbeta')]
            eq_str = '\\approx'
            # pick a tanbeta slice
            print("tanbeta goes from {} to {}".format(min(tanbeta), max(tanbeta)))
            tanb_min = InputTools.get_literal("Minimum tanbeta to plot? ")
            tanb_max = InputTools.get_literal("Maximum tanbeta to plot? ")
            assert tanb_max > tanb_min
            tanb_mask = np.logical_and(tanbeta > tanb_min, tanbeta < tanb_max)
            tanbeta = 0.5*(tanb_min + tanb_max)
            mH = mH[tanb_mask]
            mA = mA[tanb_mask]
            mC = mC[tanb_mask]
            m122 = m122[tanb_mask]
            check_mask = check_mask[tanb_mask]
        # make the scatter
        if ax is None:
            from mpl_toolkits.mplot3d import Axes3D
            fig = plt.figure()
            ax = fig.add_subplot(111, projection='3d')
        if scatter:
            short_mask = np.where(check_mask)[0]
            np.random.shuffle(short_mask)
            short_mask = short_mask[:500]
            scatter = ax.scatter(mH[short_mask], mA[short_mask], m122[short_mask], c=mC[short_mask], alpha=0.2, marker='o')
            #scatter = ax.scatter(mH[short_mask], mA[short_mask], m122[short_mask], c='yellow', alpha=0.2, marker='o')
            #short_mask = np.where(~check_mask)[0]
            #np.random.shuffle(short_mask)
            #short_mask = short_mask[:500]
            #scatter = ax.scatter(mH[short_mask], mA[short_mask], m122[short_mask], c='green', alpha=0.2, marker='o')
            ##scatter = ax.scatter(mH, mA, m122, c=mC, alpha=0.2, marker='o')
            colour_bar = plt.colorbar(scatter)
            colour_bar.set_label("$m_C$")
        # make the wireframe
        w_steps = 100
        w_mH = np.linspace(np.min(mH), np.max(mH), w_steps)
        w_mA = np.linspace(np.min(mA), np.max(mA), w_steps)
        ww_mH, ww_mA = np.meshgrid(w_mH, w_mA)
        # must interpolate to get mC
        interpolator_limit = 5000
        mask = np.zeros_like(mH, dtype=bool)
        mask[:interpolator_limit] = True
        np.random.shuffle(mask)
        mC_intepolator = scipy.interpolate.interp2d(mH[mask], mA[mask], mC[mask], kind='cubic')
        ww_mC = mC_intepolator(w_mH, w_mA)
        ww_tanb = np.full_like(ww_mH, tanbeta).flatten()
        # need to be carefull when feeding stuff into this
        if exact:
            grid = np.vstack((ww_mH.flatten(), ww_mA.flatten(), ww_mC.flatten()))
        else:
            grid = np.vstack((ww_mH.flatten(), ww_mA.flatten(), ww_mC.flatten(), ww_tanb))
        #variables = self.variable_terms(grid)
        _, upper = self._read_coefficients(check, 'upper', process, yukawa, tanbeta)
        _, lower = self._read_coefficients(check, 'lower', process, yukawa, tanbeta)
        _, centre = self._read_coefficients(check, 'centre', process, yukawa, tanbeta)
        st()
        centre_values = self._height(grid, centre)
        centre_values = centre_values.reshape(ww_mA.shape)
        ax.plot_wireframe(ww_mH, ww_mA, centre_values, alpha=0.4, color='black')
        if np.nan not in lower:
            lower_values = self._height(grid, lower)

            lower_values = lower_values.reshape(ww_mA.shape)
            #ax.plot_wireframe(ww_mH, ww_mA, ww_mH, alpha=0.4, color='blue')
            ax.plot_wireframe(ww_mH, ww_mA, lower_values, alpha=0.4, color='blue')
        if np.nan not in upper:
            upper_values = self._height(grid, upper)
            plt.scatter(ww_mH.flatten(), ww_mA.flatten(), np.zeros_like(upper_values), color='red', alpha=0.4)
            #upper_values = upper_values.reshape(ww_mA.shape)
            #ax.plot_wireframe(ww_mH, ww_mA, upper_values, alpha=0.4, color='red')
        # check the limits makes sense
        zmin, zmax = ax.get_zlim()
        zmax = np.clip(zmax, 0, np.max(m122)*2)
        zmin = np.clip(zmin, 0, zmax)
        ax.set_zlim(zmin, zmax)
        # label up
        ax.set_xlabel("$m_H$")
        ax.set_ylabel("$m_A$")
        ax.set_zlabel("$m_{12}^2$")
        ax.set_title("Type{} $tan(\\beta){}{}$, points passing theory/HB checks".format(yukawa, eq_str, tanbeta))
        plt.show()

