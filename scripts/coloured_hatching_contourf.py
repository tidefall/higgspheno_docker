import matplotlib.pyplot as plt
import numpy as np
import matplotlib


def contourf_hatch(*args, data=None, ax=None, hatch_color=None, **kwargs):
    if ax is None:
        ax = plt.gca()
    if hatch_color is None:  # call without modification
        print("You did not provide a hatch_color, so no hatch-colouring will be done")
        ax.contourf(*args, data=data, **kwargs)
    elif 'hatches' not in kwargs:  # call without modification
        print("You did not provide a hatches argument, so no a normal contourf plot will be made")
        ax.contourf(*args, data=data, **kwargs)
    else:  # everything is avalible to hatch the contours
        invisible_kws = {name: kwargs[name] for name in kwargs}
        invisible_kws['alpha'] = 0.
        invisible_kws.pop('hatches', None)
        contours = plt.contourf(*args, data=None, **invisible_kws)
        hatches = kwargs['hatches']
        if 'linestyles' in kwargs:
            # draw a line
            linestyle = kwargs['linestyles']
            linewidth = kwargs.get('linewidths', 1.5)
            line_kwargs = {k:kwargs[k] for k in kwargs}
            line_kwargs['colors'] = [hatch_color]
            line_kwargs['levels'] = [0.5]
            plt.contour(*args, data=data, **line_kwargs)
        else:
            linestyle = None
            linewidth = 0
        for collection in contours.collections:
            for path in collection.get_paths():
                path_patch = matplotlib.patches.PathPatch(path,
                                                          edgecolor=hatch_color, hatch=hatches,
                                                          facecolor=(1.,1.,1.,0.), lw=linewidth,
                                                          ls=linestyle)
                ax.add_patch(path_patch)
        return path_patch
            


if __name__ == '__main__':
    x = [1,2,3,4]
    y = [1,2,3,4]
    m = [[15,14,13,12],[14,12,10,8],[13,10,7,4],[12,8,4,0]]
    contourf_hatch(x, y, m, hatch_color=(0.1, 0.1, 0.8, 0.6), hatches='//', levels=[0., 12.])
    plt.show()
