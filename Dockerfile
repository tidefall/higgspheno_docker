#To build;
# docker build -t dock2hdm .
# to run;
# mkdir output
# docker run -v $(pwd)/output:/tohost -ti dock2hdm

# base image
FROM ubuntu:16.04

# some meta data
LABEL version=1.0
LABEL description="A docker container for 2HDMC"
LABEL author="Henry Day-Hall"

# install packages
# isajet requires the latex ....
RUN apt-get update && \
    apt-get -y install python3 python-dev wget \
                       vim-tiny nano make \
                       libgsl-dev g++ gfortran \
                       git python-ipdb tmux \
                       gedit csh \
                       texlive-latex-base
RUN echo "alias vim=vim.tiny" >> ~/.bashrc

RUN apt -y install python3-pip
RUN  pip3 install --upgrade pip

RUN pip3 install numpy matplotlib pyslha h5py lhapdf sklearn future_fstrings ipdb

# fetch the packages
RUN  wget -O HiggsSignals.tar.gz https://higgsbounds.hepforge.org/downloads?f=HiggsSignals-2.2.3beta.tar.gz
RUN  wget -O 2HDMC.tar.gz https://2hdmc.hepforge.org/downloads/?f=2HDMC-1.7.0.tar.gz
RUN  wget -O SusHi.tar.gz https://sushi.hepforge.org/downloads/?f=SusHi-1.7.0.tar.gz
RUN  wget -O LHAPDF.tar.gz https://lhapdf.hepforge.org/downloads/?f=LHAPDF-6.2.3.tar.gz
RUN  wget -O HiggsBounds.tar.gz https://higgsbounds.hepforge.org/downloads?f=HiggsBounds-5.3.2beta.tar.gz
#RUN wget -O Isajet.tar http://www.nhn.ou.edu/~isajet/isajet.tar
#RUN wget -O Softsusy.tag.gz http://www.hepforge.org/archive/softsusy/softsusy-4.1.9.tar.gz
#RUN wget -O Spheno.tar.gz https://spheno.hepforge.org/downloads/?f=SPheno-4.0.4.tar.gz
#RUN wget -O NMSSMTools.tgz --no-check-certificate https://www.lupm.univ-montp2.fr/users/nmssm/NMSSMTools_5.5.2.tgz
#RUN wget -O SuSpect2.gz https://coulomb.umontpellier.fr/perso/jean-loic.kneur/Suspect/suspect252.gz
# import the alterations
RUN echo "please reclone"
RUN git clone https://tidefall@bitbucket.org/tidefall/higgspheno_docker.git && \
    mv higgspheno_docker/alterations ./                                     && \
    mv higgspheno_docker/scripts ./                                         && \
#    mv higgspheno_docker/sample_data ./                                     && \
    yes | rm -r higgspheno_docker
## start making packages
RUN tar -xvf HiggsBounds.tar.gz                                             && \
    mkdir downloaded_packages                                               && \
    mv HiggsBounds.tar.gz downloaded_packages                               && \
    cd /HiggsBounds-5.3.2beta                                               && \
    # kill every print command
    python /scripts/silence_fortran.py                                      && \
    /bin/bash ./configure                                                   && \
    make                                                                    && \
    make libHB
RUN cd /                                                                    && \
    tar -xvf HiggsSignals.tar.gz                                            && \
    mv HiggsSignals.tar.gz downloaded_packages                              && \
    cd /HiggsSignals-2.2.3beta                                              && \
    # kill every print command
    python /scripts/silence_fortran.py                                      && \
    sed -i -e 's|HiggsBounds-5.3.1beta|HiggsBounds-5.3.2beta|' ./configure    && \
    /bin/bash ./configure                                                   && \
    make                                                                    && \
    make libHS
RUN cd /                                                                    && \
    tar -xvf 2HDMC.tar.gz                                                   && \
    mv 2HDMC.tar.gz downloaded_packages                                     && \
    cd /2HDMC-1.7.0                                                         && \
    cp /HiggsBounds-5.3.2beta/libHB.a ./lib/                                && \
    cp /HiggsSignals-2.2.3beta/libHS.a ./lib/                               && \
    sed -i -e 's/#CFLAGS+=-DHiggsBounds/CFLAGS+=-DHiggsBounds/' ./Makefile    && \
    sed -i -e 's/#LDFLAGS+=-L$(LIBDIR)/LDFLAGS+=-L$(LIBDIR)/' ./Makefile      && \
    sed -i -e 's/#SOURCES+=HBHS.cpp/SOURCES+=HBHS.cpp/' ./Makefile            && \
    # add some corrections (courtesy of Souad) 
    cp /alterations/Constraints.cpp /alterations/Constraints.h src/         && \
    cp /alterations/HBHS.cpp /alterations/HBHS.h src/                       && \
    # kill every print command
#    sed -i -e 's|^\s*printf(|if(false) printf(|' src/*                           && \
    make 
    # note, if the above step fails with an error related to constexpr
    # add the flag -fpermissive to the compile flags in the makefile
RUN cd /                                                                    && \
    tar -xvf LHAPDF.tar.gz                                                  && \
    mv LHAPDF.tar.gz downloaded_packages                                    && \
    cd /LHAPDF-6.2.3                                                        && \
    /bin/bash ./configure                                                   && \
    make                                                                    && \
    make check                                                              && \
    make install
# also need pdf sets to do anything with this
RUN LHAPDF_SETS_DIR=/usr/local/share/LHAPDF                                 && \
    cd $LHAPDF_SETS_DIR                                                     && \
    wget --no-check-certificate https://www.hep.ucl.ac.uk/mmht/LHAPDF6/MMHT2014lo68cl.tar.gz  && \
    tar -xvf MMHT2014lo68cl.tar.gz                                          && \
    wget --no-check-certificate https://www.hep.ucl.ac.uk/mmht/LHAPDF6/MMHT2014nlo68cl.tar.gz  && \
    tar -xvf MMHT2014nlo68cl.tar.gz                                         && \
    wget --no-check-certificate https://www.hep.ucl.ac.uk/mmht/LHAPDF6/MMHT2014nnlo68cl.tar.gz  && \
    tar -xvf MMHT2014nnlo68cl.tar.gz
# going to make 2 versions of sushi, pipe and print
RUN cd /                                                                    && \
    tar -xvf SusHi.tar.gz                                                   && \
    mv SusHi.tar.gz downloaded_packages                                     && \
    cd /SusHi-1.7.0                                                         && \
    /bin/bash ./configure                                                   && \
    sed -i -e 's|^LHAPATH =.*$|LHAPATH = /usr/local/lib|' ./Makefile             && \
    sed -i -e 's|^2HDMCPATH =.*$|2HDMCPATH = /2HDMC-1.7.0|' ./Makefile           && \
    sed -i -e 's|^2HDMCVERSION =.*$|2HDMCVERSION = 1.7.0|' ./Makefile            && \
    sed -i -e 's|^HBPATH =.*$|HBPATH = /HiggsBounds-5.3.2beta|' ./Makefile       && \
    sed -i -e 's|^HBVERSION =.*$|HBVERSION = 5.3.2|' ./Makefile                  && \
    sed -i -e 's|^HSPATH =.*$|HSPATH = /HiggsSignals-2.2.3beta|' ./Makefile      && \
    sed -i -e 's|^HSVERSION =.*$|HSVERSION = 2.2.3|' ./Makefile                  && \
    # this one is a bit mysterious, but otherwise HB4/HS1 libraries get used
    sed -i -e 's|PREDEFLIBS := \${PREDEFLIBS} \${2HDMCPATH}/lib/lib2HDMC.a|PREDEFLIBS := ${PREDEFLIBS} ${2HDMCPATH}/lib/lib2HDMC.a ${2HDMCPATH}/lib/libHB.a ${2HDMCPATH}/lib/libHS.a|' ./Makefile && \
    # try to kill every print command
#    python /scripts/silence_fortran.py                                         && \
    # that dosn't really work on SusHi, so make this change
#    cp /alterations/sushi.F src/sushi.F                                        && \
    # make a copy of this
    cp -r /SusHi-1.7.0 /PipeSusHi                                              && \
    make predef=2HDMC
# now make the second version with the alterations
RUN cd /PipeSusHi                                                              && \
    cp /alterations/inputoutput.f /alterations/readslha.f src/sushi/           && \
    make predef=2HDMC
# ommiting superiso for now
#RUN cd /                                                                    && \
#    tar -xvf isajet.tar                                                     && \
#    mv isajet.tar downloaded_packages                                       && \
#    cd /isajet                                                              && \
## idk why this is needed, but the makefile likes to create a symbolic link here
#    sed '56i	rm isajet.ps' Makefile                                      && \
#    make                                                                
#RUN cd /                                                                    && \
#    tar -xvf Softsusy.tar.gz                                                && \
#    mv Softsusy.tar.gz downloaded_packages                                  && \
#    cd /softsusy-4.1.9                                                      && \
#    /bin/bash ./configure                                                   && \
#    make                                                                    && \
#    make check                                                              && \
## this seems to fail? not sure if it matters
#    make install
#RUN cd /                                                                    && \
#    tar -xvf Spheno.tar.gz                                                && \
#    mv Spheno.tar.gz downloaded_packages                                  && \
#    cd /SPheno-4.0.4                                                      &&\
#    sed -i -e 's|^# F90 = gfortran.*$|F90 = gfortran|' ./Makefile         && \
#    sed -i -e 's|^.*F90 = ifort.*$|# F90 = ifort|' ./Makefile             && \
#    make                                                                
#RUN cd /                                                                    && \
#    tar -xvf NMSSMTools.tgz                                                && \
#    mv NMSSMTools.tgz downloaded_packages                                  && \
#    cd /NMSSMTools_5.5.2                                                      && \
#    make init                                                                  && \
#    make                                                                 
#RUN cd /                                                                    && \
#    tar -xvf SuSpect2.gz -C SuSpect2
    

# now set up Rachid's script
RUN cp /scripts/InputTools.py /scripts/Rachid_ParamScan.py /2HDMC-1.7.0/    && \
    cp /scripts/Rachid_ParamScan.cpp /2HDMC-1.7.0/src/                      && \
    cd /2HDMC-1.7.0                                                         && \
    make Rachid_ParamScan

# set up my scripts
RUN cd /                                                                    && \ 
    cp /scripts/Bounds.cpp /scripts/FromFile.cpp /2HDMC-1.7.0/src/        && \
    cd /2HDMC-1.7.0                                                         && \
    make Bounds FromFile

# leave some instructions
RUN echo "echo" >> ~/.bashrc                                                                                                      && \
    echo "echo" >> ~/.bashrc                                                                                                      && \
    echo "echo 'To run the script please go to the directory 2HDMC-1.7.0'" >> ~/.bashrc                                           && \
    echo "echo 'and run > python Rachid_ParamScan.py'" >> ~/.bashrc                                                               && \
    echo "echo" >> ~/.bashrc                                                                                                      && \
    echo "echo 'Alternatively go to the directory scripts'" >> ~/.bashrc                                                      && \
    echo "echo 'and run > python Scan.py'" >> ~/.bashrc                                                                     && \
    echo "echo 'and follow instructions on the prompt' " >> ~/.bashrc                                                             && \
    echo "echo" >> ~/.bashrc                                                                                                      && \
    echo "echo 'To extract the results to your computer, move them into the ./tohost/ folder' " >> ~/.bashrc                      && \
    echo "echo ' assuming you launched docker with ' " >> ~/.bashrc                                                               && \
    echo "echo '> run -v $(pwd)/output:/tohost -ti dock2hdm' " >> ~/.bashrc                                                       && \
    echo "echo" >> ~/.bashrc                                                                                                      && \
    echo "echo" >> ~/.bashrc
